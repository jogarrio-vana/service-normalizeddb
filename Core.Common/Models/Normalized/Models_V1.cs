﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

/// <summary>
/// Version V1.1
/// </summary>
namespace Core.Common.Models.Normalized.V1
{


  public enum PhoneCallType
  {
    INCOMING_TYPE = 1,
    OUTGOING_TYPE,
    MISSED_TYPE,
    VOICEMAIL_TYPE,
    REJECTED_TYPE,
    BLOCKED_TYPE,
    ANSWERED_EXTERNALLY_TYPE
  }

  public enum PhoneCallNumberType
  {
    CONTACT = 2,
    UNKNOWN = 0
  }
  
  public class BankLedger
  {
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Key]
    public int TrxId { get; set; }

    // OPERATION
    public DateTime? Date { get; set; }
    public string Debit { get; set; }
    public string Credit { get; set; }

    // BANK
    [JsonProperty(PropertyName = "Banco")]
    public string Bank { get; set; }
    [JsonProperty(PropertyName = "Cuenta")]
    public string BankAccount { get; set; }
    [JsonProperty(PropertyName = "Reference")]
    public string BankRef { get; set; }

    // INVOICE
    [JsonProperty(PropertyName = "Monto Bruto")]
    public string InvoiceSubtotal { get; set; }
    [JsonProperty(PropertyName = "IVA")]
    public string InvoiceVAT { get; set; }

    // DOCUMENTS
    public string InvoiceUrl { get; set; }
    public string BankUrl { get; set; }

    // DATASOURCES
    public string loan_request_id { get; set; }
    public string user_id { get; set; }
    [JsonProperty(PropertyName = "CAE")]
    public string invoice_cae { get; set; }
    public string loan_id { get; set; }
    public string bank_transaction_id { get; set; }
    public string payment_id { get; set; }

    // TRANSIENT
    [JsonProperty(PropertyName = "created_at")]
    public string source_created_at;
    public DateTime helper_2;
    //public string helper_3 { get; set; }
    public string source_date;
    public string source_date_string;
  }


  public class User
  {
    public int Id { get; set; }
    public string Firebase_Id { get; set; }

    public string workProfile___workAddress___street { get; set; }
    public string workProfile___workAddress___state { get; set; }
    public string workProfile___workAddress___country { get; set; }
    public string workProfile___workAddress___city { get; set; }
    public string workProfile___profession { get; set; }
    public string workProfile___paymentFrequency { get; set; }
    public string workProfile___nextPaymentDate { get; set; }
    public string workProfile___incomeSource { get; set; }
    public string workProfile___incomeAmount { get; set; }
    public string workProfile___employmentStatus { get; set; }

    public string status___skipBankAccountVerification { get; set; }
    public string status___loanTotalPaymentApprovals { get; set; }
    public string status___loanTotalApprovals { get; set; }
    public string status___loanTotalApplications { get; set; }
    public string status___loanAmountLevel { get; set; }
    public string status___idAlreadyVerified { get; set; }
    public string status___displayedTutorial { get; set; }
    public string status___displayedPhoneVerification { get; set; }
    public string status___displayedLogin { get; set; }
    public string status___activeLoan { get; set; }

    public string profile___taxId { get; set; }
    public string profile___referralSource { get; set; }
    public string profile___profession { get; set; }
    public string profile___lastName { get; set; }
    public string profile___industry { get; set; }
    public string profile___idVerification___selfie { get; set; }
    public string profile___idVerification___front { get; set; }
    public string profile___idVerification___back { get; set; }
    public bool   profile___hasKids { get; set; }
    public string profile___gender { get; set; }
    public string profile___firstName { get; set; }
    public string profile___email { get; set; }
    public string profile___dpi { get; set; }
    public string profile___civilStatus { get; set; }
    public string profile___birthdate { get; set; }
    public string profile___bankAccount { get; set; }
    public string profile___address___street { get; set; }
    public string profile___address___state { get; set; }
    public string profile___address___country { get; set; }
    public string profile___address___city { get; set; }
    public string profile___address { get; set; }

    public string phoneProfile___ownPhone { get; set; }
    public string phoneProfile___newPhone { get; set; }
    public string phoneProfile___months { get; set; }

    public string info___signedUp { get; set; }
    public string info___installReferrer { get; set; }
    public string info___activeDeviceId { get; set; }

    public bool   fb_user_profile___verified { get; set; }
    public string fb_user_profile___timezone { get; set; }
    public string fb_user_profile___name { get; set; }
    public string fb_user_profile___locale { get; set; }
    public string fb_user_profile___last_name { get; set; }
    public string fb_user_profile___gender { get; set; }
    public string fb_user_profile___first_name { get; set; }
    public string fb_user_profile___email { get; set; }
    public string fb_user_profile___birthday { get; set; }

    public string bankAccount___type { get; set; }
    public string bankAccount___number { get; set; }
    public string bankAccount___name { get; set; }
    public string bankAccount___bank { get; set; }

    public bool   verified { get; set; }
    public string timezone { get; set; }
    public string signedUp { get; set; }
    public string phoneVerification { get; set; }
    public long phone { get; set; }
    public string name { get; set; }
    public string locale { get; set; }
    public string latestLogTimestamp { get; set; }
    public string last_name { get; set; }
    public string first_name { get; set; }
    public string endTutorial { get; set; }
    public string email { get; set; }

    public string Devices { get; set; }
    public string LastLocations { get; set; }

    public ICollection<Loan> Loans { get; set; }
    public ICollection<Deposit> Deposits { get; set; }

    //public virtual Balance Balance { get; set; }
    //public int? BalanceId { get; set; }

    public DateTime CreatedOn { get; set; }
  }

  public class AWSLoan
  {
    public int Id { get; set; }

    public DateTime due_datetime { get; set; }      // **********
    public DateTime created_datetime { get; set; }
    public DateTime created_at { get; set; }
    public DateTime updated_at { get; set; }
    public DateTime? extension_datetime { get; set; }
    public DateTime? payment_datetime { get; set; }

    public string user_id { get; set; }             // **********
    public int additional_payment { get; set; }
    public string loan_request_id { get; set; }     // **********
    public string status { get; set; }
    public DateTime? released_datetime { get; set; }
    public string user_snapshot_id { get; set; }
    public string loan_id { get; set; }
    public int loan_number { get; set; }            // **********

    public string sub_category { get; set; }
    public float amount_requested { get; set; }     // **********
    public string loan_purpose { get; set; }
    public bool outstanding_loans { get; set; }
    public int loan_term { get; set; }
    public float interest_amount { get; set; }
    public string category { get; set; }
    public float interest_rate { get; set; }
  }


  public class Loan
  {
    public int Id { get; set; }

    public string Firebase_Id { get; set; }
    public string Firebase_UserId { get; set; }
    public string Firebase_createdDateTime { get; set; }
    public string Firebase_dueDateTime { get; set; }

    public string subCategory { get; set; }
    public string statusNotificationUnavailable { get; set; }
    public string status { get; set; }
    public string signature { get; set; }
    public DateTime repaymentDateTime { get; set; }

    public string reasonForLoan { get; set; }
    public string promissoryNote { get; set; }
    public string outstandingLoans { get; set; }
    public string loanTerm { get; set; }
    public string loanPurpose { get; set; }
    public string lastNotifiedStatus { get; set; }
    public int loanNumber{ get; set; }

    public float interestRate { get; set; }
    public float interestAmount { get; set; }

    public string estimatedReleaseText { get; set; }
    public string estimatedReleaseStatus { get; set; }
    public string estimatedReleaseHour { get; set; }
    public string estimatedReleaseDate { get; set; }

    public string endReleasedLoanFirstTime { get; set; }
    public string endRejectedFirstTime { get; set; }
    public string endPaymentRejectedFirstTime { get; set; }
    public string endPaymentApprovedFirstTime { get; set; }
    public string endConfirmAccountInfo { get; set; }
    public string endApprovedLoanFirstTime { get; set; }

    public DateTime dueDateTime { get; set; }
    //public string dueDate { get; set; }

    public string displayedBankConfirmation { get; set; }
    public DateTime createdDateTime { get; set; }
    //public string created { get; set; }
    public string category { get; set; }
    public string appVersion { get; set; }

    public string amountRequested { get; set; }
    public string amountReleased { get; set; }
    public string amountAsked { get; set; }
    public string amountApproved { get; set; }

    //public string userId { get; set; }
    //public string user { get; set; }
    public string Signatures { get; set; }

    public User User { get; set; }
    public int UserId { get; set; }

    public DateTime CreatedOn { get; set; }
  }

  public class Deposit
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    public string Airtable_Id { get; set; }
    public DateTime Airtable_CreatedTime { get; set; }

    public string Firebase_Loan_Id { get; set; }
    public string Firebase_User_Id { get; set; }

    public string DepositID { get; set; }
    public string First_Name { get; set; }
    public string Last_Name { get; set; }
    public string Phone_Number { get; set; }
    public float Deposit_Amount { get; set; }
    public string DPI { get; set; }
    public string Nit { get; set; }
    public string Bank { get; set; }
    public int Loan_Counter { get; set; }
    public string Lote { get; set; }
    public string Money_Source { get; set; }
    public string Check_Id { get; set; }
    public string _doc { get; set; }
    public string Deposit_Image { get; set; }
    public string Deposit_Status { get; set; }
    public DateTime Deposit_Date { get; set; }
    public DateTime Due_Date { get; set; }
    public float Paid_Amount { get; set; }
    public DateTime Payment_Date { get; set; }
    public DateTime Extension_Pay_Date { get; set; }
    public string Payment_Confirmation_Image { get; set; }
    public string Pay_Early_Check { get; set; }
    public string Notas { get; set; }
    
    public User User { get; set; }
    public int UserId { get; set; }
  }

  public class LoanRequest
  {
    public string user_id { get; set; }

    public string amount_requested { get; set; }
    public string app_version { get; set; }
    public string category { get; set; }
    public string created_at { get; set; }
    public string created_datetime { get; set; }
    public string interest_rate { get; set; }
    public string loan_purpose { get; set; }
    public string loan_request_id { get; set; }
    public string loan_term { get; set; }
    public string outstanding_loans { get; set; }
    public string status { get; set; }
    public string sub_category { get; set; }

    public string device___IMEI { get; set; }
    public string device___instance { get; set; }
    public string device___last_dumps___calls { get; set; }
    public string device___last_dumps___contacts { get; set; }
    public string device___last_dumps___installed_apps { get; set; }
    public string device___last_dumps___sms_inbox { get; set; }
    public string device___last_dumps___sms_sent { get; set; }
    public string device___messaging_token { get; set; }
    public string device___SSAID { get; set; }
    public string device___user_id { get; set; }

    public string files___calls { get; set; }
    public string files___contacts { get; set; }
    public string files___installed_apps { get; set; }
    public string files___sms_inbox { get; set; }
    public string files___sms_sent { get; set; }

    public object user___devices { get; set; }
    public object user___loans { get; set; }

    public string user___bank_account___bank { get; set; }
    public string user___bank_account___name { get; set; }
    public string user___bank_account___number { get; set; }
    public string user___bank_account___type { get; set; }

    public string user___fb_user_profile___email { get; set; }
    public string user___fb_user_profile___first_name { get; set; }
    public string user___fb_user_profile___last_name { get; set; }
    public string user___fb_user_profile___locale { get; set; }
    public string user___fb_user_profile___name { get; set; }
    public string user___fb_user_profile___timezone { get; set; }
    public string user___fb_user_profile___verified { get; set; }

    public string user___info___active_device_id { get; set; }
    public string user___info___install_referrer { get; set; }
    public string user___info___signed_up { get; set; }

    public string user___phone { get; set; }
    public string user___phone_profile___months { get; set; }
    public string user___phone_profile___new_phone { get; set; }
    public string user___phone_profile___own_phone { get; set; }
    public string user___phone_verification { get; set; }

    public string user___profile___birthdate { get; set; }
    public string user___profile___civil_status { get; set; }
    public string user___profile___dpi { get; set; }
    public string user___profile___email { get; set; }
    public string user___profile___first_name { get; set; }
    public string user___profile___gender { get; set; }
    public string user___profile___has_kids { get; set; }
    public string user___profile___last_name { get; set; }
    public string user___profile___referral_source { get; set; }
    public string user___profile___tax_id { get; set; }

    public string user___profile___address___city { get; set; }
    public string user___profile___address___country { get; set; }
    public string user___profile___address___state { get; set; }
    public string user___profile___address___street { get; set; }

    public string user___profile___id_verification___back { get; set; }
    public string user___profile___id_verification___front { get; set; }
    public string user___profile___id_verification___selfie { get; set; }

    public string user___status___active_loan { get; set; }
    public string user___status___displayed_login { get; set; }
    public string user___status___displayed_phone_verification { get; set; }
    public string user___status___displayed_tutorial { get; set; }

    public string user___work_profile___employment_status { get; set; }
    public string user___work_profile___income_amount { get; set; }
    public string user___work_profile___income_source { get; set; }
    public string user___work_profile___next_payment_date { get; set; }
    public string user___work_profile___payment_frequency { get; set; }
    public string user___work_profile___profession { get; set; }

    public string user___work_profile___work_address___city { get; set; }
    public string user___work_profile___work_address___country { get; set; }
    public string user___work_profile___work_address___state { get; set; }
    public string user___work_profile___work_address___street { get; set; }

  }

  public class Balance
  {
    public int Id { get; set; }
    
    public float capital_subtotal { get; set; } // aggregate
    public float interest_subtotal { get; set; } // aggregate
    public float penalty_subtotal { get; set; } // aggregate

    public float total { get; set; }

    public ICollection<Transaction> Transactions { get; set; }

    public virtual User User { get; set; }
  }

  public class Transaction
  {
    public int Id { get; set; }

    public float credit { get; set; }  // payment: amount
    public float debit { get; set; } // load: amount_request + interest_amount

    //public float capital_subtotal { get; set; } // optional
    //public float interest_subtotal { get; set; } // optional
    //public float penalty_subtotal { get; set; } // optional

    // DEBIT:
    private string bank_name { get; set; } // [WRITE-BACK] 
    private string bank_paymentType { get; set; } // [WRITE-BACK] 
    private string bank_paymentBatch { get; set; } // [WRITE-BACK] 
    private string bank_document { get; set; } // [WRITE-BACK] 

    // CREDIT:
    public string face_cae { get; set; } // [WRITE-BACK] bank_transaction: reference
    public string face_numeroDte { get; set; } // bank_transaction: reference
    
    public string reference { get; set; } // bank_transaction: reference
    public string type { get; set; } // payment: type [payout, interest]
    public string status { get; set; } // payment: status [added,processing,rejected]
    public string image_url { get; set; } // payment: image.remote_url.S
    
    public string ledger_id { get; set; } // loan_ledger: ledger_id
    public string payment_id { get; set; } // loan_ledger: payment_id
    public string bank_transaction_id { get; set; } // loan_ledger: bank_transaction_id
    public string loan_id { get; set; } // payment: loan_id
    public string loan_request_id { get; set; } // payment: loan_request_id
    public string user_id { get; set; } // payment: user_id



    public string created_at { get; set; } // payment: created_at
    public string updated_at { get; set; } // payment: updated_at

    public Balance Balance { get; set; }
    public int BalanceId { get; set; }
  }

  public class LoanResult
  {
    public int Id { get; set; }
    public string Firebase_Id { get; set; }

    public string processingResult___user_id { get; set; }
    public string processingResult___results___sagemaker_endpoint { get; set; }
    public string processingResult___results___result___predictions___score { get; set; }
    public string processingResult___results___result___predictions___predicted_label { get; set; }
    public string processingResult___results___result { get; set; }
    public string processingResult___loan_request_id { get; set; }

    public string humanResult___resultDateTime { get; set; }
    public string humanResult___humanResult { get; set; }

    public string humanResult { get; set; }
    public string modelResult { get; set; }
    public string modelDumpUrl { get; set; }
    public string modelConfidence { get; set; }
    public string statusCode { get; set; }
    public string resultDateTime { get; set; }
    public string body { get; set; }
  }

  public class Device
  {
    public int Id { get; set; }
    public string Firebase_Id { get; set; }

    public string lastDumps___smsSent { get; set; }
    public string lastDumps___smsInbox { get; set; }
    public string lastDumps___installedApps { get; set; }
    public string lastDumps___contacts { get; set; }
    public string lastDumps___calls { get; set; }

    public string data___smsLogs___timestamp { get; set; }
    public string data___smsLogs___files { get; set; }
    public string data___sms_sent___timestamp { get; set; }
    public string data___sms_sent___files { get; set; }
    public string data___sms_inbox___timestamp { get; set; }
    public string data___sms_inbox___files { get; set; }
    public string data___contactList___timestamp { get; set; }
    public string data___contactList___files { get; set; }
    public string data___callList___timestamp { get; set; }
    public string data___callList___files { get; set; }

    public string SSAID { get; set; }
    public string messagingToken { get; set; }
    public string Instance { get; set; }
    public string IMEI { get; set; }

    public string user { get; set; }
    public string userId { get; set; }
  }

  public class PhoneCall
  {
    public int Id { get; set; }
    public string S3_Id { get; set; }

    public PhoneCallType type { get; set; }
    public string normalized_number { get; set; } // +50247551411
    public string name { get; set; } // Iracema
    public DateTime date { get; set; } // 1542477763849
    public int duration { get; set; } // 114
    public PhoneCallNumberType numbertype { get; set; }

    //public string Vertice_Number { get; set; }
    //public string OutEdge_Number { get; set; }
    //public string InEdge_Number { get; set; }
  }

  public class PhoneContact
  {
    public long id { get; set; }
    public string S3_Id { get; set; }

    public long Number { get { return this.id; } }

    public string display_name { get; set; } // mi amor
    public int times_contacted { get; set; } // 13
    public string account_name { get; set; } // amor, mi
    public DateTime contact_last_updated_timestamp { get; set; }
    public int has_phone_number { get; set; } // 1539905910374
    public string data4 { get; set; } // +50212341234
  }

  public class PhoneSMS
  {
    public int Id { get; set; }
    public string S3_Id { get; set; }

    //public long to { get; set; }
    //public long from { get; set; }

    public string address { get; set; } // +50242174672, 7720, +502 4621 0358
    public DateTime date { get; set; } // 1539905910374
    public string body { get; set; } // Dios abre caminos donde no los hay. Cuando todas las puertas se te cierren, solo espera y confia.
    public bool read { get; set; }
    public PhoneCallType type { get; set; }
    
    //public string Vertice_Number { get; set; }
    //public string OutEdge_Number { get; set; }
    //public string InEdge_Number { get; set; }
  }

  public class PhoneApp
  {
    public int Id { get; set; }
    public string S3_Id { get; set; }

    public string className { get; set; } // com.sec.android.app.music.common.MusicApplication
    public bool enabled { get; set; }
  }
  
}
