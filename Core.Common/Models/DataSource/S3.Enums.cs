﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Common.Models.DataSource.S3
{

  public enum PhoneCallType
  {
    INCOMING_TYPE = 1,
    OUTGOING_TYPE,
    MISSED_TYPE,
    VOICEMAIL_TYPE,
    REJECTED_TYPE,
    BLOCKED_TYPE,
    ANSWERED_EXTERNALLY_TYPE
  }

  public enum PhoneCallNumberType
  {
    CONTACT = 2,
    UNKNOWN = 0
  }
}
