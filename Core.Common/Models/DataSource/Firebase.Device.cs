﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Common.Models.DataSource.Firebase
{
  public class Device
  {
    public string id { get; set; }

    public string SSAID { get; set; }
    public string Instance { get; set; }
    public string IMEI { get; set; }
    public string messagingToken { get; set; }
    public string data___callList___files { get; set; }
    public string data___sms_sent___files { get; set; }
    public string data___sms_inbox___files { get; set; }
    public string data___contactList___files { get; set; }
    public string lastDumps___calls { get; set; }
    public string lastDumps___smsInbox { get; set; }
    public string lastDumps___contacts { get; set; }
    public string lastDumps___smsSent { get; set; }
    public string lastDumps___installedApps { get; set; }
    public string data___callList___timestamp { get; set; }
    public string data___contactList___timestamp { get; set; }
    public string data___sms_inbox___timestamp { get; set; }
    public string data___sms_sent___timestamp { get; set; }
    public string data___smsLogs___timestamp { get; set; }
    public string data___smsLogs___files { get; set; }

    public string user { get; set; }
    public string userId { get; set; }
  }

}
