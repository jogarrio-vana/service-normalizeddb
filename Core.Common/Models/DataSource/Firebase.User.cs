﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Common.Models.DataSource.Firebase
{
  public class User
  {
    public string id { get; set; }

    public string fb_user_profile___locale { get; set; }
    public string fb_user_profile___first_name { get; set; }
    public string fb_user_profile___timezone { get; set; }
    public string fb_user_profile___name { get; set; }
    public string fb_user_profile___last_name { get; set; }
    public string fb_user_profile___verified { get; set; }
    public string phone { get; set; }
    public string phoneVerification { get; set; }
    public string status___displayedTutorial { get; set; }
    public string status___displayedLogin { get; set; }
    public string status___displayedPhoneVerification { get; set; }
    public string info___activeDeviceId { get; set; }
    public string info___signedUp { get; set; }
    public string info___installReferrer { get; set; }
    public string fb_user_profile___email { get; set; }
    public string profile___idVerification___front { get; set; }
    public string profile___idVerification___back { get; set; }
    public string profile___idVerification___selfie { get; set; }
    public string profile___civilStatus { get; set; }
    public string profile___email { get; set; }
    public string profile___firstName { get; set; }
    public string profile___dpi { get; set; }
    public string profile___lastName { get; set; }
    public string status___loanTotalApplications { get; set; }
    public string signedUp { get; set; }
    public string bankAccount___bank { get; set; }
    public string bankAccount___name { get; set; }
    public string bankAccount___number { get; set; }
    public string bankAccount___type { get; set; }
    public string status___activeLoan { get; set; }
    public string profile___address___city { get; set; }
    public string profile___gender { get; set; }
    public string profile___hasKids { get; set; }
    public string profile___address___street { get; set; }
    public string profile___address___country { get; set; }
    public string profile___address___state { get; set; }
    public string phoneProfile___ownPhone { get; set; }
    public string workProfile___workAddress___city { get; set; }
    public string workProfile___workAddress___street { get; set; }
    public string profile___referralSource { get; set; }
    public string workProfile___workAddress___state { get; set; }
    public string workProfile___paymentFrequency { get; set; }
    public string profile___taxId { get; set; }
    public string profile___birthdate { get; set; }
    public string workProfile___incomeSource { get; set; }
    public string phoneProfile___months { get; set; }
    public string phoneProfile___newPhone { get; set; }
    public string workProfile___profession { get; set; }
    public string workProfile___workAddress___country { get; set; }
    public string workProfile___nextPaymentDate { get; set; }
    public string workProfile___incomeAmount { get; set; }
    public string workProfile___employmentStatus { get; set; }
    public string status___loanAmountLevel { get; set; }
    public string status___loanTotalApprovals { get; set; }
    public string latestLogTimestamp { get; set; }
    public string endTutorial { get; set; }
    public string status___loanTotalPaymentApprovals { get; set; }
    public string fb_user_profile___gender { get; set; }
    public string profile___industry { get; set; }
    public string profile___profession { get; set; }
    public string profile___address { get; set; }
    public string profile___bankAccount { get; set; }
    public string status___skipBankAccountVerification { get; set; }
    public string fb_user_profile___birthday { get; set; }
    public string status___idAlreadyVerified { get; set; }
    public string verified { get; set; }
    public string email { get; set; }
    public string last_name { get; set; }
    public string locale { get; set; }
    public string name { get; set; }
    public string first_name { get; set; }
    public string timezone { get; set; }

    public string Devices { get; set; }
    public string LastLocations { get; set; }
    public string Loans { get; set; }
  }
  
}
