﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Common.Models.DataSource.S3
{  
  public class PhoneCall
  {
    public int id { get; set; }
    public string S3_Id { get; set; }

    public PhoneCallType type { get; set; }
    public string normalized_number { get; set; } // +50247551411
    public string name { get; set; } // Iracema
    public DateTime date { get; set; } // 1542477763849
    public int duration { get; set; } // 114
    public PhoneCallNumberType numbertype { get; set; }

    //public string Vertice_Number { get; set; }
    //public string OutEdge_Number { get; set; }
    //public string InEdge_Number { get; set; }
  }
  
}
