﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Common.Models.DataSource.Firebase
{
  public class LoanResult
  {
    public string id { get; set; }

    public string processingResult___results___result { get; set; }
    public string processingResult___user_id { get; set; }
    public string processingResult___results___sagemaker_endpoint { get; set; }
    public string humanResult___resultDateTime { get; set; }
    public string processingResult___loan_request_id { get; set; }
    public string humanResult___humanResult { get; set; }
    public string modelDumpUrl { get; set; }
    public string modelResult { get; set; }
    public string modelConfidence { get; set; }
    public string resultDateTime { get; set; }
    public string humanResult { get; set; }
    public string processingResult___results___result___predictions___predicted_label { get; set; }
    public string processingResult___results___result___predictions___score { get; set; }
    public string statusCode { get; set; }
    public string body { get; set; }

  }

}
