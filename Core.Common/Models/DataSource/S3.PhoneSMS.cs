﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Common.Models.DataSource.S3
{
  public class PhoneSMS
  {
    public int Id { get; set; }
    public string S3_Id { get; set; }

    //public long to { get; set; }
    //public long from { get; set; }

    public string address { get; set; } // +50242174672, 7720, +502 4621 0358
    public DateTime date { get; set; } // 1539905910374
    public string body { get; set; } // Dios abre caminos donde no los hay. Cuando todas las puertas se te cierren, solo espera y confia.
    public bool read { get; set; }
    public PhoneCallType type { get; set; }

    //public string Vertice_Number { get; set; }
    //public string OutEdge_Number { get; set; }
    //public string InEdge_Number { get; set; }
  }

}
