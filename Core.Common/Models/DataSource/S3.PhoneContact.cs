﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Common.Models.DataSource.S3
{
  public class PhoneContact
  {
    public long id { get; set; }
    public string S3_Id { get; set; }

    public long Number { get { return this.id; } }

    public string display_name { get; set; } // mi amor
    public int times_contacted { get; set; } // 13
    public string account_name { get; set; } // amor, mi
    public DateTime contact_last_updated_timestamp { get; set; }
    public int has_phone_number { get; set; } // 1539905910374
    public string data4 { get; set; } // +50212341234
  }
}
