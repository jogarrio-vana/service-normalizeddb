﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Common.Models.DataSource.Firebase
{
  public class Loan
  {
    public string id { get; set; }

    public string userId { get; set; }
    public string status { get; set; }
    public string loanTerm { get; set; }
    public string interestRate { get; set; }
    public string amountRequested { get; set; }
    public string loanPurpose { get; set; }
    public string subCategory { get; set; }
    public string category { get; set; }
    public string dueDateTime { get; set; }
    public string outstandingLoans { get; set; }
    public string createdDateTime { get; set; }
    public string lastNotifiedStatus { get; set; }
    public string appVersion { get; set; }
    public string amountAsked { get; set; }
    public string dueDate { get; set; }
    public string amountReleased { get; set; }
    public string amountApproved { get; set; }
    public string estimatedReleaseDate { get; set; }
    public string estimatedReleaseHour { get; set; }
    public string estimatedReleaseText { get; set; }
    public string displayedBankConfirmation { get; set; }
    public string promissoryNote { get; set; }
    public string signature { get; set; }
    public string interestAmount { get; set; }
    public string endApprovedLoanFirstTime { get; set; }
    public string endReleasedLoanFirstTime { get; set; }
    public string endRejectedFirstTime { get; set; }
    public string estimatedReleaseStatus { get; set; }
    public string endPaymentApprovedFirstTime { get; set; }
    public string created { get; set; }
    public string statusNotificationUnavailable { get; set; }
    public string reasonForLoan { get; set; }
    public string repaymentDateTime { get; set; }
    public string endConfirmAccountInfo { get; set; }
    public string endPaymentRejectedFirstTime { get; set; }

    public string Signatures { get; set; }
    public string user { get; set; }  
  }

}
