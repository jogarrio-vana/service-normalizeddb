﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Common.Models.DataSource.S3
{
  public class PhoneApp
  {
    public int Id { get; set; }
    public string S3_Id { get; set; }

    public string className { get; set; } // com.sec.android.app.music.common.MusicApplication
    public bool enabled { get; set; }
  }

}
