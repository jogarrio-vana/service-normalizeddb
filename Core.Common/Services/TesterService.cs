﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Core.Common
{
  public static class TesterService
  {
    public static void RaiseException()
    {
      throw new Exception("This is a test.");
    }
  }
}
