﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Core.Common
{
  public class SlackNotifications
  {
    private static HttpClient httpClient = null;

    public SlackNotifications()
    {
      SlackNotifications.httpClient = new HttpClient();
    }

    static public string EncodeTo64(string toEncode)
    {
      byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
      string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
      return returnValue;
    }

    public async Task Notify(string message, string user_id, string payment_id, object state)
    {
      var message_json = System.Web.HttpUtility.JavaScriptStringEncode(message);
      var state_base64 = EncodeTo64(Newtonsoft.Json.JsonConvert.SerializeObject(state));

      var payload = "";// JsonConvert.SerializeObject(new { text = message });
      payload = $"{{\"text\":\"[:interrobang:] *service-invoice-generate* failed to generate invoice \",\"attachments\":[{{\"text\":\"{message_json}\",\"fields\":[{{\"title\":\"PaymentId\",\"value\":\"{payment_id}\",\"short\":false}},{{\"title\":\"State\",\"value\":\"{state_base64}\",\"short\":false}}],\"color\":\"#F35A00\"}},{{\"text\":\"What would you like to do?\",\"fallback\":\"Woops! I didn't get that...\",\"callback_id\":\"wopr_game\",\"color\":\"#3AA3E3\",\"attachment_type\":\"default\",\"actions\":[{{\"type\":\"button\",\"name\":\"game\",\"text\":\"Goto CloudWatch\",\"url\":\"https://console.aws.amazon.com/cloudwatch/home?region=us-east-1#logStream:group=/aws/lambda/service-invoice-generate-dev;streamFilter=typeLogStreamPrefix\"}},{{\"type\":\"button\",\"name\":\"game\",\"text\":\"I'll take care of it\"}},{{\"name\":\"game\",\"text\":\"Retry request\",\"style\":\"danger\",\"type\":\"button\",\"value\":\"war\",\"confirm\":{{\"title\":\"Are you sure?\",\"text\":\"If you confirm, I will try to resend the request\",\"ok_text\":\"Yes\",\"dismiss_text\":\"No\"}}}}]}}]}}";
      var content = new StringContent(payload, Encoding.UTF8, "application/json");
      var response = await httpClient.PostAsync("https://hooks.slack.com/services/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", content);
      //var responseString = response.Content.ReadAsStringAsync().Result;
    }

  }
}
