﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class GcpAuthentication
{
  public async Task<string> GetAuthTokenAsync(string jsonToken)
  {
    System.Diagnostics.Trace.Assert(!String.IsNullOrEmpty(jsonToken), $"Field '{nameof(jsonToken)}' cannot be null or empty.");
    
    // Authenticate a Google credential with the service account

    //Handler.context.Logger.LogLine("GoogleCredentials.FromJson() => " + json.Length);
    var googleCred = Google.Apis.Auth.OAuth2.GoogleCredential.FromJson(jsonToken); //.FromFile(@"C:\git\vana\service-user\secrets\vanatesting-firebase-adminsdk.json");

    // Add the required scopes to the Google credential
    var scoped = googleCred.CreateScoped(
        new string[]{
            "https://www.googleapis.com/auth/firebase.database",
            "https://www.googleapis.com/auth/userinfo.email"
        }
    );

    // Use the Google credential to generate an access token
    //Handler.context.Logger.LogLine("GoogleCredentials.GetAccessTokenForRequestAsync()");
    return await scoped.UnderlyingCredential.GetAccessTokenForRequestAsync();
  }
}
