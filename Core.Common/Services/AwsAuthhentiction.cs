﻿/*
*	Use this code snippet in your app.
*	If you need more information about configurations or implementing the sample code, visit the AWS docs:
*	https://aws.amazon.com/developers/getting-started/net/
*
*/

using System;
using System.IO;
using System.Threading.Tasks;
using Amazon;
using Amazon.Lambda.Core;
using Amazon.SecretsManager;
using Amazon.SecretsManager.Model;

namespace Core.Common.Services
{


  public class AwsAuthhentiction
  {
    public async Task<T> GetSecretAsync<T>(string secretName, string region, string VersionStage = "AWSCURRENT", ILambdaContext context = null)
    {
      System.Diagnostics.Trace.Assert(!String.IsNullOrEmpty(secretName), $"Field '{nameof(secretName)}' cannot be null or empty.");
      System.Diagnostics.Trace.Assert(!String.IsNullOrEmpty(region), $"Field '{nameof(region)}' cannot be null or empty.");
      System.Diagnostics.Trace.Assert(!String.IsNullOrEmpty(VersionStage), $"Field '{nameof(VersionStage)}' cannot be null or empty.");

      context?.Logger.LogLine(">>>> [DBG] AwsAuthhentiction.GetSecretAsync()");

      IAmazonSecretsManager client = new AmazonSecretsManagerClient(RegionEndpoint.GetBySystemName(region));
      GetSecretValueRequest request = new GetSecretValueRequest();
      request.SecretId = secretName;
      request.VersionStage = VersionStage; // VersionStage defaults to AWSCURRENT if unspecified.

      GetSecretValueResponse response = null;

      // In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
      // See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
      // We rethrow the exception by default.

      try
      {
        context?.Logger.LogLine($">>>> [DBG] {nameof(AwsAuthhentiction)}.client.GetSecretValueAsync() => {request.SecretId},{request.VersionStage}");
        response = await client.GetSecretValueAsync(request);
      }
      catch (DecryptionFailureException e)
      {
        // Secrets Manager can't decrypt the protected secret text using the provided KMS key.
        // Deal with the exception here, and/or rethrow at your discretion.

        context?.Logger.LogLine($">>>> [ERR] {nameof(AwsAuthhentiction)}.DecryptionFailureException() => \n{e.Message}");
        throw e;
      }
      catch (InternalServiceErrorException e)
      {
        // An error occurred on the server side.
        // Deal with the exception here, and/or rethrow at your discretion.
        context?.Logger.LogLine($">>>> [ERR] {nameof(AwsAuthhentiction)}.InternalServiceErrorException() => \n{e.Message}");
        throw e;
      }
      catch (InvalidParameterException e)
      {
        // You provided an invalid value for a parameter.
        // Deal with the exception here, and/or rethrow at your discretion
        context?.Logger.LogLine($">>>> [ERR] {nameof(AwsAuthhentiction)}.InvalidParameterException() => \n{e.Message}");
        throw e;
      }
      catch (InvalidRequestException e)
      {
        // You provided a parameter value that is not valid for the current state of the resource.
        // Deal with the exception here, and/or rethrow at your discretion.
        context?.Logger.LogLine($">>>> [ERR] {nameof(AwsAuthhentiction)}.InvalidRequestException() => \n{e.Message}");
        throw e;
      }
      catch (ResourceNotFoundException e)
      {
        // We can't find the resource that you asked for.
        // Deal with the exception here, and/or rethrow at your discretion.
        context?.Logger.LogLine($">>>> [ERR] {nameof(AwsAuthhentiction)}.ResourceNotFoundException() => \n{e.Message}");
        throw e;
      }
      catch (System.AggregateException ae)
      {
        // More than one of the above exceptions were triggered.
        // Deal with the exception here, and/or rethrow at your discretion.
        
        var ex = ae.Flatten().InnerException;
        while (ex.InnerException != null) { ex = ex.InnerException; }

        context?.Logger.LogLine($">>>> [ERR] {nameof(AwsAuthhentiction)}.DecryptionFailureException() => \n{ae?.Message}");
        context?.Logger.LogLine($">>>> [ERR] {nameof(AwsAuthhentiction)}.DecryptionFailureException() => \n{Newtonsoft.Json.JsonConvert.SerializeObject(ae?.Data)}");
        
        throw ex;
      }
      catch (Exception e)
      {
        // We can't find the resource that you asked for.
        // Deal with the exception here, and/or rethrow at your discretion.
        while (e.InnerException != null) { e = e.InnerException; }
        context?.Logger.LogLine($">>>> [ERR] {nameof(AwsAuthhentiction)}.Exception() => \n{e.Message}");
        throw e;
      }
      client.Dispose();

      // Response was empty
      context?.Logger.LogLine($">>>> [DBG] {nameof(AwsAuthhentiction)}.SecretBinary = " + (response.SecretBinary != null ? response.SecretBinary.Length : response.SecretString.Length) + " bytes");
      if (string.IsNullOrEmpty(response.SecretString) && response.SecretBinary == null)
      {
        return default(T);
      }

      // Decrypts secret using the associated KMS CMK.
      // Depending on whether the secret is a string or binary, one of these fields will be populated.

      if (typeof(T) == typeof(String))
      {
        return (T)Convert.ChangeType(response.SecretString, typeof(T));
      }
      else if (typeof(T) == typeof(MemoryStream) || typeof(T) == typeof(Stream))
      {
        //memoryStream = response.SecretBinary;
        //StreamReader reader = new StreamReader(memoryStream);
        //string decodedBinarySecret = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(reader.ReadToEnd()));
        return (T)Convert.ChangeType(response.SecretBinary, typeof(T));
      }
      else
      {
        throw new InvalidCastException();
      }

      // Your code goes here.
    }

  }

}