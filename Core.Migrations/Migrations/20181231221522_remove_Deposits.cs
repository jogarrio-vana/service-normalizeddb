﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Data.Migrations
{
    public partial class remove_Deposits : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Deposits");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Deposits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Airtable_CreatedTime = table.Column<DateTime>(nullable: false),
                    Airtable_Id = table.Column<string>(nullable: true),
                    Bank = table.Column<string>(nullable: true),
                    Check_Id = table.Column<string>(nullable: true),
                    DPI = table.Column<string>(nullable: true),
                    DepositID = table.Column<string>(nullable: true),
                    Deposit_Amount = table.Column<float>(nullable: false),
                    Deposit_Date = table.Column<DateTime>(nullable: false),
                    Deposit_Image = table.Column<string>(nullable: true),
                    Deposit_Status = table.Column<string>(nullable: true),
                    Due_Date = table.Column<DateTime>(nullable: false),
                    Extension_Pay_Date = table.Column<DateTime>(nullable: false),
                    Firebase_Loan_Id = table.Column<string>(nullable: true),
                    Firebase_User_Id = table.Column<string>(nullable: true),
                    First_Name = table.Column<string>(nullable: true),
                    Last_Name = table.Column<string>(nullable: true),
                    Loan_Counter = table.Column<int>(nullable: false),
                    Lote = table.Column<string>(nullable: true),
                    Money_Source = table.Column<string>(nullable: true),
                    Nit = table.Column<string>(nullable: true),
                    Notas = table.Column<string>(nullable: true),
                    Paid_Amount = table.Column<float>(nullable: false),
                    Pay_Early_Check = table.Column<string>(nullable: true),
                    Payment_Confirmation_Image = table.Column<string>(nullable: true),
                    Payment_Date = table.Column<DateTime>(nullable: false),
                    Phone_Number = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false),
                    _doc = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Deposits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Deposits_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Deposits_UserId",
                table: "Deposits",
                column: "UserId");
        }
    }
}
