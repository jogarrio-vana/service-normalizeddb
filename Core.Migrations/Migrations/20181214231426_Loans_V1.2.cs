﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Data.Migrations
{
    public partial class Loans_V12 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Firebase_createdDateTime",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Firebase_dueDateTime",
                table: "Users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Firebase_createdDateTime",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Firebase_dueDateTime",
                table: "Users");
        }
    }
}
