﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Data.Migrations
{
  public partial class loanstateView1 : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      new StreamsDbContext().Database.ExecuteSqlCommand("DROP VIEW dbo.LoanState");
      new StreamsDbContext().Database.ExecuteSqlCommand(
        @"CREATE VIEW dbo.LoanSummary AS 
          SELECT
	          loan_request_id as LoanId,
	          user_id as UserId,
	          amount_requested as DepositAmount,
	          loan_number as LoanNumber,
	          DATEADD(hour,-6,released_datetime) as DepositDate,
	          DATEADD(hour,-6,due_datetime) as DueDate,
	          (SELECT SUM(CAST(dbo.BankLedger.Credit as float)) FROM dbo.BankLedger WHERE dbo.BankLedger.loan_request_id LIKE CONCAT(dbo.AWSLoans.loan_request_id,'%')) as PaidAmount,
	          DATEADD(hour,-6,payment_datetime) as PaymentDate,
	          DATEADD(hour,-6,extension_datetime) as ExtensionDate,
	          status as Status
          FROM dbo.AWSLoans
          WHERE
	          dbo.AWSLoans.status != 'wrong' 
	          AND dbo.AWSLoans.status != 'rejected'");

    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      new StreamsDbContext().Database.ExecuteSqlCommand("DROP VIEW dbo.LoanSummary");
    }
  }
}
