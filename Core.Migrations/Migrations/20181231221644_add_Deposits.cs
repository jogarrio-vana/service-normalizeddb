﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Data.Migrations
{
    public partial class add_Deposits : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Deposits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Airtable_Id = table.Column<string>(nullable: true),
                    Airtable_CreatedTime = table.Column<DateTime>(nullable: false),
                    Firebase_Loan_Id = table.Column<string>(nullable: true),
                    Firebase_User_Id = table.Column<string>(nullable: true),
                    DepositID = table.Column<string>(nullable: true),
                    First_Name = table.Column<string>(nullable: true),
                    Last_Name = table.Column<string>(nullable: true),
                    Phone_Number = table.Column<string>(nullable: true),
                    Deposit_Amount = table.Column<float>(nullable: false),
                    DPI = table.Column<string>(nullable: true),
                    Nit = table.Column<string>(nullable: true),
                    Bank = table.Column<string>(nullable: true),
                    Loan_Counter = table.Column<int>(nullable: false),
                    Lote = table.Column<string>(nullable: true),
                    Money_Source = table.Column<string>(nullable: true),
                    Check_Id = table.Column<string>(nullable: true),
                    _doc = table.Column<string>(nullable: true),
                    Deposit_Image = table.Column<string>(nullable: true),
                    Deposit_Status = table.Column<string>(nullable: true),
                    Deposit_Date = table.Column<DateTime>(nullable: false),
                    Due_Date = table.Column<DateTime>(nullable: false),
                    Paid_Amount = table.Column<float>(nullable: false),
                    Payment_Date = table.Column<DateTime>(nullable: false),
                    Extension_Pay_Date = table.Column<DateTime>(nullable: false),
                    Payment_Confirmation_Image = table.Column<string>(nullable: true),
                    Pay_Early_Check = table.Column<string>(nullable: true),
                    Notas = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Deposits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Deposits_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Deposits_UserId",
                table: "Deposits",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Deposits");
        }
    }
}
