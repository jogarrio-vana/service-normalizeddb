﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Data.Migrations
{
    public partial class Users_V1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Firebase_Id = table.Column<string>(nullable: true),
                    workProfile___workAddress___street = table.Column<string>(nullable: true),
                    workProfile___workAddress___state = table.Column<string>(nullable: true),
                    workProfile___workAddress___country = table.Column<string>(nullable: true),
                    workProfile___workAddress___city = table.Column<string>(nullable: true),
                    workProfile___profession = table.Column<string>(nullable: true),
                    workProfile___paymentFrequency = table.Column<string>(nullable: true),
                    workProfile___nextPaymentDate = table.Column<string>(nullable: true),
                    workProfile___incomeSource = table.Column<string>(nullable: true),
                    workProfile___incomeAmount = table.Column<string>(nullable: true),
                    workProfile___employmentStatus = table.Column<string>(nullable: true),
                    status___skipBankAccountVerification = table.Column<string>(nullable: true),
                    status___loanTotalPaymentApprovals = table.Column<string>(nullable: true),
                    status___loanTotalApprovals = table.Column<string>(nullable: true),
                    status___loanTotalApplications = table.Column<string>(nullable: true),
                    status___loanAmountLevel = table.Column<string>(nullable: true),
                    status___idAlreadyVerified = table.Column<string>(nullable: true),
                    status___displayedTutorial = table.Column<string>(nullable: true),
                    status___displayedPhoneVerification = table.Column<string>(nullable: true),
                    status___displayedLogin = table.Column<string>(nullable: true),
                    status___activeLoan = table.Column<string>(nullable: true),
                    profile___taxId = table.Column<string>(nullable: true),
                    profile___referralSource = table.Column<string>(nullable: true),
                    profile___profession = table.Column<string>(nullable: true),
                    profile___lastName = table.Column<string>(nullable: true),
                    profile___industry = table.Column<string>(nullable: true),
                    profile___idVerification___selfie = table.Column<string>(nullable: true),
                    profile___idVerification___front = table.Column<string>(nullable: true),
                    profile___idVerification___back = table.Column<string>(nullable: true),
                    profile___hasKids = table.Column<bool>(nullable: false),
                    profile___gender = table.Column<string>(nullable: true),
                    profile___firstName = table.Column<string>(nullable: true),
                    profile___email = table.Column<string>(nullable: true),
                    profile___dpi = table.Column<string>(nullable: true),
                    profile___civilStatus = table.Column<string>(nullable: true),
                    profile___birthdate = table.Column<string>(nullable: true),
                    profile___bankAccount = table.Column<string>(nullable: true),
                    profile___address___street = table.Column<string>(nullable: true),
                    profile___address___state = table.Column<string>(nullable: true),
                    profile___address___country = table.Column<string>(nullable: true),
                    profile___address___city = table.Column<string>(nullable: true),
                    profile___address = table.Column<string>(nullable: true),
                    phoneProfile___ownPhone = table.Column<string>(nullable: true),
                    phoneProfile___newPhone = table.Column<string>(nullable: true),
                    phoneProfile___months = table.Column<string>(nullable: true),
                    info___signedUp = table.Column<string>(nullable: true),
                    info___installReferrer = table.Column<string>(nullable: true),
                    info___activeDeviceId = table.Column<string>(nullable: true),
                    fb_user_profile___verified = table.Column<bool>(nullable: false),
                    fb_user_profile___timezone = table.Column<string>(nullable: true),
                    fb_user_profile___name = table.Column<string>(nullable: true),
                    fb_user_profile___locale = table.Column<string>(nullable: true),
                    fb_user_profile___last_name = table.Column<string>(nullable: true),
                    fb_user_profile___gender = table.Column<string>(nullable: true),
                    fb_user_profile___first_name = table.Column<string>(nullable: true),
                    fb_user_profile___email = table.Column<string>(nullable: true),
                    fb_user_profile___birthday = table.Column<string>(nullable: true),
                    bankAccount___type = table.Column<string>(nullable: true),
                    bankAccount___number = table.Column<string>(nullable: true),
                    bankAccount___name = table.Column<string>(nullable: true),
                    bankAccount___bank = table.Column<string>(nullable: true),
                    verified = table.Column<bool>(nullable: false),
                    timezone = table.Column<string>(nullable: true),
                    signedUp = table.Column<string>(nullable: true),
                    phoneVerification = table.Column<string>(nullable: true),
                    phone = table.Column<long>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    locale = table.Column<string>(nullable: true),
                    latestLogTimestamp = table.Column<string>(nullable: true),
                    last_name = table.Column<string>(nullable: true),
                    first_name = table.Column<string>(nullable: true),
                    endTutorial = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    Devices = table.Column<string>(nullable: true),
                    LastLocations = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Loan",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Firebase_Id = table.Column<string>(nullable: true),
                    subCategory = table.Column<string>(nullable: true),
                    statusNotificationUnavailable = table.Column<string>(nullable: true),
                    status = table.Column<string>(nullable: true),
                    signature = table.Column<string>(nullable: true),
                    repaymentDateTime = table.Column<DateTime>(nullable: false),
                    reasonForLoan = table.Column<string>(nullable: true),
                    promissoryNote = table.Column<string>(nullable: true),
                    outstandingLoans = table.Column<string>(nullable: true),
                    loanTerm = table.Column<string>(nullable: true),
                    loanPurpose = table.Column<string>(nullable: true),
                    lastNotifiedStatus = table.Column<string>(nullable: true),
                    interestRate = table.Column<float>(nullable: false),
                    interestAmount = table.Column<float>(nullable: false),
                    estimatedReleaseText = table.Column<string>(nullable: true),
                    estimatedReleaseStatus = table.Column<string>(nullable: true),
                    estimatedReleaseHour = table.Column<string>(nullable: true),
                    estimatedReleaseDate = table.Column<string>(nullable: true),
                    endReleasedLoanFirstTime = table.Column<string>(nullable: true),
                    endRejectedFirstTime = table.Column<string>(nullable: true),
                    endPaymentRejectedFirstTime = table.Column<string>(nullable: true),
                    endPaymentApprovedFirstTime = table.Column<string>(nullable: true),
                    endConfirmAccountInfo = table.Column<string>(nullable: true),
                    endApprovedLoanFirstTime = table.Column<string>(nullable: true),
                    dueDateTime = table.Column<DateTime>(nullable: false),
                    displayedBankConfirmation = table.Column<string>(nullable: true),
                    createdDateTime = table.Column<DateTime>(nullable: false),
                    category = table.Column<string>(nullable: true),
                    appVersion = table.Column<string>(nullable: true),
                    amountRequested = table.Column<string>(nullable: true),
                    amountReleased = table.Column<string>(nullable: true),
                    amountAsked = table.Column<string>(nullable: true),
                    amountApproved = table.Column<string>(nullable: true),
                    Signatures = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Loan", x => x.id);
                    table.ForeignKey(
                        name: "FK_Loan_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Loan_UserId",
                table: "Loan",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Loan");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
