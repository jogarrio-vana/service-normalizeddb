﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Core.Data.Migrations
{
    public partial class bankledgersadddatetime3 : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropColumn(
          name: "helper_1",
          table: "BankLedger");

      migrationBuilder.DropColumn(
          name: "helper_3",
          table: "BankLedger");

    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.AddColumn<DateTime>(
          name: "helper_1",
          table: "BankLedger",
          nullable: false,
          defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

      migrationBuilder.AddColumn<DateTime>(
          name: "helper_3",
          table: "BankLedger",
          nullable: false,
          defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
    }
  }
}
