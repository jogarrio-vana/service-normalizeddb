﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Data.Migrations
{
    public partial class awsloans : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AWSLoans",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    due_datetime = table.Column<DateTime>(nullable: false),
                    created_datetime = table.Column<DateTime>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: false),
                    updated_at = table.Column<DateTime>(nullable: false),
                    user_id = table.Column<string>(nullable: true),
                    additional_payment = table.Column<int>(nullable: false),
                    loan_request_id = table.Column<string>(nullable: true),
                    status = table.Column<string>(nullable: true),
                    released_datetime = table.Column<DateTime>(nullable: true),
                    user_snapshot_id = table.Column<string>(nullable: true),
                    loan_id = table.Column<string>(nullable: true),
                    loan_number = table.Column<int>(nullable: false),
                    sub_category = table.Column<string>(nullable: true),
                    amount_requested = table.Column<float>(nullable: false),
                    loan_purpose = table.Column<string>(nullable: true),
                    outstanding_loans = table.Column<bool>(nullable: false),
                    loan_term = table.Column<int>(nullable: false),
                    interest_amount = table.Column<float>(nullable: false),
                    category = table.Column<string>(nullable: true),
                    interest_rate = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AWSLoans", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AWSLoans");
        }
    }
}
