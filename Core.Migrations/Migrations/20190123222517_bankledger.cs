﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Data.Migrations
{
    public partial class bankledger : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BankLedger",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<string>(nullable: true),
                    Debit = table.Column<string>(nullable: true),
                    Credit = table.Column<string>(nullable: true),
                    Bank = table.Column<string>(nullable: true),
                    BankAccount = table.Column<string>(nullable: true),
                    BankRef = table.Column<string>(nullable: true),
                    InvoiceSubtotal = table.Column<string>(nullable: true),
                    InvoiceVAT = table.Column<string>(nullable: true),
                    InvoiceUrl = table.Column<string>(nullable: true),
                    BankUrl = table.Column<string>(nullable: true),
                    loan_request_id = table.Column<string>(nullable: true),
                    user_id = table.Column<string>(nullable: true),
                    invoice_cae = table.Column<string>(nullable: true),
                    loan_id = table.Column<string>(nullable: true),
                    bank_transaction_id = table.Column<string>(nullable: true),
                    payment_id = table.Column<string>(nullable: true),
                    helper_1 = table.Column<string>(nullable: true),
                    helper_2 = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankLedger", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BankLedger");
        }
    }
}
