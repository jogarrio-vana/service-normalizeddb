﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Data.Migrations
{
    public partial class AddDeposits_rename_firebase_ids : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "User_ID",
                table: "Deposits",
                newName: "Firebase_User_Id");

            migrationBuilder.RenameColumn(
                name: "Loan_ID",
                table: "Deposits",
                newName: "Firebase_Loan_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Firebase_User_Id",
                table: "Deposits",
                newName: "User_ID");

            migrationBuilder.RenameColumn(
                name: "Firebase_Loan_Id",
                table: "Deposits",
                newName: "Loan_ID");
        }
    }
}
