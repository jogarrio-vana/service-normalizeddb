﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.Data.Migrations
{
    public partial class AddDeposits : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Balances_BalanceId",
                table: "Users");

            migrationBuilder.DropTable(
                name: "Transactions");

            migrationBuilder.DropTable(
                name: "Balances");

            migrationBuilder.DropIndex(
                name: "IX_Users_BalanceId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "BalanceId",
                table: "Users");

            migrationBuilder.CreateTable(
                name: "Deposits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Airtable_Id = table.Column<string>(nullable: true),
                    Airtable_CreatedTime = table.Column<DateTime>(nullable: false),
                    DepositID = table.Column<string>(nullable: true),
                    Loan_ID = table.Column<string>(nullable: true),
                    User_ID = table.Column<string>(nullable: true),
                    First_Name = table.Column<string>(nullable: true),
                    Last_Name = table.Column<string>(nullable: true),
                    Phone_Number = table.Column<string>(nullable: true),
                    Deposit_Amount = table.Column<float>(nullable: false),
                    DPI = table.Column<string>(nullable: true),
                    Nit = table.Column<string>(nullable: true),
                    Bank = table.Column<string>(nullable: true),
                    Loan_Counter = table.Column<int>(nullable: false),
                    Lote = table.Column<string>(nullable: true),
                    Money_Source = table.Column<string>(nullable: true),
                    Check_Id = table.Column<string>(nullable: true),
                    _doc = table.Column<string>(nullable: true),
                    Deposit_Image = table.Column<string>(nullable: true),
                    Deposit_Status = table.Column<string>(nullable: true),
                    Deposit_Date = table.Column<DateTime>(nullable: false),
                    Due_Date = table.Column<DateTime>(nullable: false),
                    Paid_Amount = table.Column<float>(nullable: false),
                    Payment_Date = table.Column<DateTime>(nullable: false),
                    Extension_Pay_Date = table.Column<DateTime>(nullable: false),
                    Payment_Confirmation_Image = table.Column<string>(nullable: true),
                    Pay_Early_Check = table.Column<string>(nullable: true),
                    Notas = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Deposits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Deposits_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Deposits_UserId",
                table: "Deposits",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Deposits");

            migrationBuilder.AddColumn<int>(
                name: "BalanceId",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Balances",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    total = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Balances", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Transactions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BalanceId = table.Column<int>(nullable: false),
                    bank_transaction_id = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(nullable: true),
                    credit = table.Column<float>(nullable: false),
                    debit = table.Column<float>(nullable: false),
                    image_url = table.Column<string>(nullable: true),
                    ledger_id = table.Column<string>(nullable: true),
                    loan_id = table.Column<string>(nullable: true),
                    loan_request_id = table.Column<string>(nullable: true),
                    payment_id = table.Column<string>(nullable: true),
                    reference = table.Column<string>(nullable: true),
                    status = table.Column<string>(nullable: true),
                    updated_at = table.Column<string>(nullable: true),
                    user_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transactions_Balances_BalanceId",
                        column: x => x.BalanceId,
                        principalTable: "Balances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_BalanceId",
                table: "Users",
                column: "BalanceId",
                unique: true,
                filter: "[BalanceId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_BalanceId",
                table: "Transactions",
                column: "BalanceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Balances_BalanceId",
                table: "Users",
                column: "BalanceId",
                principalTable: "Balances",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
