﻿using Amazon.Lambda.Core;
using Core.Common.Models.Normalized.V1;
using Core.Common.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Core.Data
{
  public class StreamsDbContext : DbContext
  {
    private ILambdaContext context { get; set; }
    public static IConfigurationRoot configuration = null;
    public static string connectionString = null;

    public StreamsDbContext()
      : base()
    {
    }

    public StreamsDbContext(ILambdaContext context)
      : base()
    {
      this.context = context;
    }

    public StreamsDbContext(DbContextOptions<StreamsDbContext> options)
      : base(options)
    {}

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      AddConnectionString(optionsBuilder).Wait();
    }

    private async Task AddConnectionString(DbContextOptionsBuilder optionsBuilder)
    {
      var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

      if (StreamsDbContext.configuration == null)
      {
        context?.Logger.LogLine($"[{context.AwsRequestId}] Reading configurations");
        StreamsDbContext.configuration = new ConfigurationBuilder()
       .SetBasePath(Directory.GetCurrentDirectory())
       .AddJsonFile("appsettings.dev.json", optional: false, reloadOnChange: false)
       .AddJsonFile($"appsettings.{environment}.json", optional: true, reloadOnChange: false)
       .Build();
      }

      context?.Logger.LogLine($">>>> [DBG] {nameof(StreamsDbContext)}.OnConfiguring().ConfigurationBuilder()");

      // context?.Logger.LogLine(">>>> [DBG] - AppSettings:AWSRDS:Secret = " + (configuration["AppSettings:AWSRDS:Secret"] ?? "null"));
      // context?.Logger.LogLine(">>>> [DBG] - AppSettings:AWSRegion = " + (configuration["AppSettings:AWSRegion"] ?? "null"));

      if (string.IsNullOrEmpty(StreamsDbContext.connectionString))
      {
        context?.Logger.LogLine($"[{context.AwsRequestId}] Calling SecretManager");
        var awsclient = new AwsAuthhentiction();
        StreamsDbContext.connectionString = await awsclient
          .GetSecretAsync<string>(
            secretName: configuration["AppSettings:AWSRDS:Secret"],
            region: configuration["AppSettings:AWSRegion"],
            context: context);
      }
      
      context?.Logger.LogLine($">>>> [DBG] {nameof(StreamsDbContext)}.UseSqlServer(connectionString)");

      optionsBuilder
        .UseSqlServer(connectionString);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<User>()
          .HasMany(p => p.Loans)
          .WithOne(b => b.User)
          .HasForeignKey(b => b.UserId);

      modelBuilder.Entity<User>()
          .HasMany(p => p.Deposits)
          .WithOne(b => b.User)
          .HasForeignKey(b => b.UserId);

      //modelBuilder.Entity<Loan>()
      //    .HasOne(a => a.LoanRequest)
      //    .WithOne(b => b.Loan)
      //    .HasForeignKey(b => b.LoanId);
    }

    public DbSet<User> Users { get; set; }
    public DbSet<Loan> Loans { get; set; }
    public DbSet<AWSLoan> AWSLoans { get; set; }
    //public DbSet<Balance> Balances { get; set; }
    //public DbSet<Transaction> Transactions { get; set; }
    public DbSet<Deposit> Deposits { get; set; }
    public DbSet<BankLedger> BankLedger { get; set; }
    //public DbSet<Core.Common.Loan> Loans { get; set; }
    //public DbSet<Core.Common.PhoneSMS> SMS { get; set; }
    //public DbSet<Core.Common.PhoneContact> PhoneContact { get; set; }

    //public DbSet<Core.Common.User> Users { get; set; }
  }

}