using Core.Data;
using Core.Processing;
using Core.Tests;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


/// <summary>
/// This is to be run from a single machine in parralel threads
/// </summary>
[TestCaseOrderer("Core.Tests.PriorityOrderer", "Core.Tests")]
public class ScaffoldClasses
{

  [Fact, TestPriority(1)]
  public void ScaffoldClass()
  {
    var json = "{'eventType':'LoanRequest.StatusUpdate',\"data\":{\"amount_requested\":400,\"app_version\":\"1.7.4\",\"category\":\"personal\",\"estimated_release_date\":\"2018-12-21\",\"estimated_release_hour\":12,\"estimated_release_status\":\"processing\",\"estimated_release_text\":\"El tiempo estimado de tu dep�sito es el VIERNES a las 12:00 p.m.\",\"interest_amount\":80,\"interest_rate\":20,\"loan_purpose\":\"en combustible gasolina\",\"loan_term\":28,\"outstanding_loans\":true,\"status\":\"processing\",\"sub_category\":\"Transporte\",\"user_id\":\"YjHzcgmYpDOz1gbu1YX1bfxNYax2\",\"loan_request_id\":\"-LUCTZgzeHyFdU8wmmkS\",\"created_datetime\":\"2018-12-20T21:17:24.720Z\",\"due_datetime\":\"2019-01-17T21:17:24.720Z\",\"user\":{\"devices\":{\"-LQdadX3jOz4m-jzHv3h\":true},\"loans\":{\"-LQop_ab6Xyr2YRSN_Xy\":true,\"-LUCTZgzeHyFdU8wmmkS\":true},\"bank_account\":{\"bank\":\"BI\",\"name\":\"Sergio Danilo Ju�rez �ngel\",\"number\":\"0120334610\",\"type\":\"checking\"},\"fb_user_profile\":{\"email\":\"danilojuarez11@hotmail.com\",\"first_name\":\"Sergio\",\"last_name\":\"Juarez\",\"locale\":\"es_LA\",\"name\":\"Sergio Danilo Juarez\",\"timezone\":\"-6\",\"verified\":\"true\"},\"info\":{\"active_device_id\":\"-LQdadX3jOz4m-jzHv3h\",\"install_referrer\":\"utm_source=(not%20set)&utm_medium=(not%20set)\",\"signed_up\":true},\"phone\":\"+50230538759\",\"phone_profile\":{\"months\":48,\"new_phone\":false,\"own_phone\":true},\"phone_verification\":true,\"profile\":{\"address\":{\"city\":\"Sayaxch�\",\"country\":\"GT\",\"state\":\"PETEN\",\"street\":\"barrio el centro sayaxche Pet�n\"},\"birthdate\":\"11/05/1992\",\"civil_status\":\"single\",\"dpi\":\"2225033431710\",\"email\":\"sdanilojuarez27@gmail.com\",\"first_name\":\"Sergio Danilo\",\"gender\":\"male\",\"has_kids\":false,\"id_verification\":{\"back\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Fprofile%2Freverse_document%2F13806?alt=media&token=d5828a13-d686-484a-8841-43d24092c042\",\"front\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Fprofile%2Ffront_document%2F13804?alt=media&token=c89db54f-e913-425a-b983-3ba653dca138\",\"selfie\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Fprofile%2Fselfie_document%2F13808?alt=media&token=77bcdbdb-5970-4424-9443-7385c3477f25\"},\"last_name\":\"Juarez Angel\",\"referral_source\":\"Otro\",\"tax_id\":\"88853594\"},\"status\":{\"active_loan\":\"-LUCTZgzeHyFdU8wmmkS\",\"displayed_login\":true,\"displayed_phone_verification\":true,\"displayed_tutorial\":true,\"loan_amount_level\":1,\"loan_total_applications\":2,\"loan_total_approvals\":1,\"loan_total_payment_approvals\":1},\"work_profile\":{\"employment_status\":\"employed\",\"income_amount\":3500,\"income_source\":\"compra de gasolina\n\n\n\n\",\"next_payment_date\":\"15/11/2018\",\"payment_frequency\":\"monthly\",\"profession\":\"Otro\",\"work_address\":{\"city\":\"La Libertad\",\"country\":\"GT\",\"state\":\"PETEN\",\"street\":\"asesor de cr�ditos\"}}},\"device\":{\"IMEI\":\"355648083615259\",\"instance\":\"eju7vJKPHlg\",\"SSAID\":\"e3c3456581eb7ca2\",\"last_dumps\":{\"calls\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fcall-logs%2Fcall_log-201812201516.json.gz?alt=media&token=71820a37-8f5d-43b7-8cac-2f4993767657\",\"contacts\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fcontact-list%2FcontactList-201812201516.json.gz?alt=media&token=32e7f08e-effd-4140-9e9b-4dd452151ba2\",\"installed_apps\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fdata-snapshots%2Finstalled-apps-201812201516.json.gz?alt=media&token=a9f3291b-907f-4c48-95f2-68d32ead4080\",\"sms_inbox\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fsms-inbox-logs%2Fsms_inbox_log-201812201516.json.gz?alt=media&token=47f9967b-0b69-4daa-8e46-53496cb02e9f\",\"sms_sent\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fsms-sent-logs%2Fsms_sent_log-201812201516.json.gz?alt=media&token=de31f8c5-8a3b-4cc6-be18-6f13b2863e39\"},\"messaging_token\":\"eju7vJKPHlg:APA91bEs-f5QoK3JX_zCpYYe3KZboLQgxfcjKkObIiT4__ZhzC6a7yczGkbWwxGCCPwnJ8tDnuFFOExN-62ok8Sb5WSVKxU0SkJg1FiR9Tfa2GW5I4EVXKdIJsu2PFmrlirqitGuT_u8\",\"user_id\":\"YjHzcgmYpDOz1gbu1YX1bfxNYax2\"},\"files\":{\"calls\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fcall-logs%2Fcall_log-201812201516.json.gz?alt=media&token=71820a37-8f5d-43b7-8cac-2f4993767657\",\"contacts\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fcontact-list%2FcontactList-201812201516.json.gz?alt=media&token=32e7f08e-effd-4140-9e9b-4dd452151ba2\",\"installed_apps\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fdata-snapshots%2Finstalled-apps-201812201516.json.gz?alt=media&token=a9f3291b-907f-4c48-95f2-68d32ead4080\",\"sms_inbox\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fsms-inbox-logs%2Fsms_inbox_log-201812201516.json.gz?alt=media&token=47f9967b-0b69-4daa-8e46-53496cb02e9f\",\"sms_sent\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fsms-sent-logs%2Fsms_sent_log-201812201516.json.gz?alt=media&token=de31f8c5-8a3b-4cc6-be18-6f13b2863e39\"},\"created_at\":\"2018-12-20T21:17:24.972Z\",\"transaction_id\":\"ff6e8ae8-dd41-4271-936e-dbba4f8796a6\"}}";
    json = "{\"amount_requested\":350,\"app_version\":\"1.7.2\",\"category\":\"personal\",\"created_at\":\"2018-10-30T16:49:39.381Z\",\"created_datetime\":\"2018-10-30T16:49:04.627Z\",\"device\":{\"IMEI\":\"868354032710230\",\"instance\":\"e0rFJFHlokw\",\"last_dumps\":{\"calls\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FSqSrCDl97BRAYmI6ZOscm6ALa8y1%2Flogs%2Fcall-logs%2Fcall_log-201810301047.json.gz?alt=media&token=22d65431-e641-4729-b239-b5c91956e3df\",\"contacts\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FSqSrCDl97BRAYmI6ZOscm6ALa8y1%2Flogs%2Fcontact-list%2FcontactList-201810301047.json.gz?alt=media&token=a81d92ec-2c7e-4b90-96a7-b8e2059ddf2e\",\"installed_apps\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FSqSrCDl97BRAYmI6ZOscm6ALa8y1%2Flogs%2Fdata-snapshots%2Finstalled-apps-201810301047.json.gz?alt=media&token=f81a2a74-de09-486c-ba5b-572f288f38f8\",\"sms_inbox\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FSqSrCDl97BRAYmI6ZOscm6ALa8y1%2Flogs%2Fsms-inbox-logs%2Fsms_inbox_log-201810301047.json.gz?alt=media&token=9d923ec2-828a-4408-9155-521307bde210\",\"sms_sent\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FSqSrCDl97BRAYmI6ZOscm6ALa8y1%2Flogs%2Fsms-sent-logs%2Fsms_sent_log-201810301047.json.gz?alt=media&token=727847a3-3480-4be6-ab9f-655006480a97\"},\"messaging_token\":\"e0rFJFHlokw:APA91bGQUQXVG22_TUyMsEyqUjufkq6B4chri2IbAR0uzCmbXgnByNIgFaMFpZnIhz3bUOt_owCljAN8j9vw2tyc9_lwtDcov90FnmZAF9v6a4ldGClrQyS6Zy6jmXImbiktew6rTIFS\",\"SSAID\":\"0e8e0845f7d3df06\",\"user_id\":\"SqSrCDl97BRAYmI6ZOscm6ALa8y1\"},\"files\":{\"calls\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FSqSrCDl97BRAYmI6ZOscm6ALa8y1%2Flogs%2Fcall-logs%2Fcall_log-201810301047.json.gz?alt=media&token=22d65431-e641-4729-b239-b5c91956e3df\",\"contacts\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FSqSrCDl97BRAYmI6ZOscm6ALa8y1%2Flogs%2Fcontact-list%2FcontactList-201810301047.json.gz?alt=media&token=a81d92ec-2c7e-4b90-96a7-b8e2059ddf2e\",\"installed_apps\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FSqSrCDl97BRAYmI6ZOscm6ALa8y1%2Flogs%2Fdata-snapshots%2Finstalled-apps-201810301047.json.gz?alt=media&token=f81a2a74-de09-486c-ba5b-572f288f38f8\",\"sms_inbox\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FSqSrCDl97BRAYmI6ZOscm6ALa8y1%2Flogs%2Fsms-inbox-logs%2Fsms_inbox_log-201810301047.json.gz?alt=media&token=9d923ec2-828a-4408-9155-521307bde210\",\"sms_sent\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FSqSrCDl97BRAYmI6ZOscm6ALa8y1%2Flogs%2Fsms-sent-logs%2Fsms_sent_log-201810301047.json.gz?alt=media&token=727847a3-3480-4be6-ab9f-655006480a97\"},\"interest_rate\":20,\"loan_purpose\":\"para gasto de transporte p�blico \",\"loan_request_id\":\"-LQ4rx12CiJO4wEwyc7N\",\"loan_term\":28,\"outstanding_loans\":false,\"status\":\"processing\",\"sub_category\":\"Transporte\",\"user\":{\"bank_account\":{\"bank\":\"G&T\",\"name\":\"Flor de Mar�a Mor�n Loreto\",\"number\":\"01500089047\",\"type\":\"checking\"},\"devices\":{\"-LQ4i68NQ_fhnFGiU43l\":true},\"fb_user_profile\":{\"email\":\"moranloreto26@yahoo.com.mx\",\"first_name\":\"Florecita\",\"last_name\":\"Mor�n\",\"locale\":\"es_LA\",\"name\":\"Florecita Bella Mor�n\",\"timezone\":\"-6\",\"verified\":\"true\"},\"info\":{\"active_device_id\":\"-LQ4i68NQ_fhnFGiU43l\",\"install_referrer\":\"utm_source=(not%20set)&utm_medium=(not%20set)\",\"signed_up\":true},\"loans\":{\"-LQ4rx12CiJO4wEwyc7N\":true},\"phone\":\"+50259091692\",\"phone_profile\":{\"months\":12,\"new_phone\":false,\"own_phone\":true},\"phone_verification\":true,\"profile\":{\"address\":{\"city\":\"Guatemala (Capital)\",\"country\":\"GT\",\"state\":\"GUATEMALA\",\"street\":\"18 calle 34-70 zona 7 colonia Villa Linda II\"},\"birthdate\":\"26/09/1978\",\"civil_status\":\"single\",\"dpi\":\"1984908360101\",\"email\":\"florloreto1978@gmail.com\",\"first_name\":\" Flor de Mar�a\",\"gender\":\"female\",\"has_kids\":false,\"id_verification\":{\"back\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FSqSrCDl97BRAYmI6ZOscm6ALa8y1%2Fprofile%2Freverse_document%2F8017?alt=media&token=b7f7d805-7497-427a-ae04-da1ff3f839af\",\"front\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FSqSrCDl97BRAYmI6ZOscm6ALa8y1%2Fprofile%2Ffront_document%2F8015?alt=media&token=833ad839-c357-40a5-8753-9ff7cc526868\",\"selfie\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FSqSrCDl97BRAYmI6ZOscm6ALa8y1%2Fprofile%2Fselfie_document%2F8019?alt=media&token=7f92541d-1d49-4ae7-ba25-005d98f961b9\"},\"last_name\":\"Moran Loreto\",\"referral_source\":\"Facebook\",\"tax_id\":\"9596542\"},\"status\":{\"active_loan\":\"-LQ4rx12CiJO4wEwyc7N\",\"displayed_login\":true,\"displayed_phone_verification\":true,\"displayed_tutorial\":true},\"work_profile\":{\"employment_status\":\"employed\",\"income_amount\":3500,\"income_source\":\"M� principal fuente de ingreso es el de Maestra \",\"next_payment_date\":\"28/11/2018\",\"payment_frequency\":\"monthly\",\"profession\":\"Profesor/a\",\"work_address\":{\"city\":\"San Pedro Sacatepequ�z\",\"country\":\"GT\",\"state\":\"GUATEMALA\",\"street\":\"km. 21 Aldea Vista Hermosa San Pedro Sacatep�quez Guatemala\"}}},\"user_id\":\"SqSrCDl97BRAYmI6ZOscm6ALa8y1\"}";
    json = @"";
    var output = Core.Processing.ClassScaffolder.Generate(json);
  }

  [Fact, TestPriority(2)]
  public void JsonToClass()
  {
    var eventJson = "{'eventType':'LoanRequest.StatusUpdate',\"data\":{\"amount_requested\":400,\"app_version\":\"1.7.4\",\"category\":\"personal\",\"estimated_release_date\":\"2018-12-21\",\"estimated_release_hour\":12,\"estimated_release_status\":\"processing\",\"estimated_release_text\":\"El tiempo estimado de tu dep�sito es el VIERNES a las 12:00 p.m.\",\"interest_amount\":80,\"interest_rate\":20,\"loan_purpose\":\"en combustible gasolina\",\"loan_term\":28,\"outstanding_loans\":true,\"status\":\"processing\",\"sub_category\":\"Transporte\",\"user_id\":\"YjHzcgmYpDOz1gbu1YX1bfxNYax2\",\"loan_request_id\":\"-LUCTZgzeHyFdU8wmmkS\",\"created_datetime\":\"2018-12-20T21:17:24.720Z\",\"due_datetime\":\"2019-01-17T21:17:24.720Z\",\"user\":{\"devices\":{\"-LQdadX3jOz4m-jzHv3h\":true},\"loans\":{\"-LQop_ab6Xyr2YRSN_Xy\":true,\"-LUCTZgzeHyFdU8wmmkS\":true},\"bank_account\":{\"bank\":\"BI\",\"name\":\"Sergio Danilo Ju�rez �ngel\",\"number\":\"0120334610\",\"type\":\"checking\"},\"fb_user_profile\":{\"email\":\"danilojuarez11@hotmail.com\",\"first_name\":\"Sergio\",\"last_name\":\"Juarez\",\"locale\":\"es_LA\",\"name\":\"Sergio Danilo Juarez\",\"timezone\":\"-6\",\"verified\":\"true\"},\"info\":{\"active_device_id\":\"-LQdadX3jOz4m-jzHv3h\",\"install_referrer\":\"utm_source=(not%20set)&utm_medium=(not%20set)\",\"signed_up\":true},\"phone\":\"+50230538759\",\"phone_profile\":{\"months\":48,\"new_phone\":false,\"own_phone\":true},\"phone_verification\":true,\"profile\":{\"address\":{\"city\":\"Sayaxch�\",\"country\":\"GT\",\"state\":\"PETEN\",\"street\":\"barrio el centro sayaxche Pet�n\"},\"birthdate\":\"11/05/1992\",\"civil_status\":\"single\",\"dpi\":\"2225033431710\",\"email\":\"sdanilojuarez27@gmail.com\",\"first_name\":\"Sergio Danilo\",\"gender\":\"male\",\"has_kids\":false,\"id_verification\":{\"back\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Fprofile%2Freverse_document%2F13806?alt=media&token=d5828a13-d686-484a-8841-43d24092c042\",\"front\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Fprofile%2Ffront_document%2F13804?alt=media&token=c89db54f-e913-425a-b983-3ba653dca138\",\"selfie\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Fprofile%2Fselfie_document%2F13808?alt=media&token=77bcdbdb-5970-4424-9443-7385c3477f25\"},\"last_name\":\"Juarez Angel\",\"referral_source\":\"Otro\",\"tax_id\":\"88853594\"},\"status\":{\"active_loan\":\"-LUCTZgzeHyFdU8wmmkS\",\"displayed_login\":true,\"displayed_phone_verification\":true,\"displayed_tutorial\":true,\"loan_amount_level\":1,\"loan_total_applications\":2,\"loan_total_approvals\":1,\"loan_total_payment_approvals\":1},\"work_profile\":{\"employment_status\":\"employed\",\"income_amount\":3500,\"income_source\":\"compra de gasolina\n\n\n\n\",\"next_payment_date\":\"15/11/2018\",\"payment_frequency\":\"monthly\",\"profession\":\"Otro\",\"work_address\":{\"city\":\"La Libertad\",\"country\":\"GT\",\"state\":\"PETEN\",\"street\":\"asesor de cr�ditos\"}}},\"device\":{\"IMEI\":\"355648083615259\",\"instance\":\"eju7vJKPHlg\",\"SSAID\":\"e3c3456581eb7ca2\",\"last_dumps\":{\"calls\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fcall-logs%2Fcall_log-201812201516.json.gz?alt=media&token=71820a37-8f5d-43b7-8cac-2f4993767657\",\"contacts\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fcontact-list%2FcontactList-201812201516.json.gz?alt=media&token=32e7f08e-effd-4140-9e9b-4dd452151ba2\",\"installed_apps\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fdata-snapshots%2Finstalled-apps-201812201516.json.gz?alt=media&token=a9f3291b-907f-4c48-95f2-68d32ead4080\",\"sms_inbox\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fsms-inbox-logs%2Fsms_inbox_log-201812201516.json.gz?alt=media&token=47f9967b-0b69-4daa-8e46-53496cb02e9f\",\"sms_sent\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fsms-sent-logs%2Fsms_sent_log-201812201516.json.gz?alt=media&token=de31f8c5-8a3b-4cc6-be18-6f13b2863e39\"},\"messaging_token\":\"eju7vJKPHlg:APA91bEs-f5QoK3JX_zCpYYe3KZboLQgxfcjKkObIiT4__ZhzC6a7yczGkbWwxGCCPwnJ8tDnuFFOExN-62ok8Sb5WSVKxU0SkJg1FiR9Tfa2GW5I4EVXKdIJsu2PFmrlirqitGuT_u8\",\"user_id\":\"YjHzcgmYpDOz1gbu1YX1bfxNYax2\"},\"files\":{\"calls\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fcall-logs%2Fcall_log-201812201516.json.gz?alt=media&token=71820a37-8f5d-43b7-8cac-2f4993767657\",\"contacts\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fcontact-list%2FcontactList-201812201516.json.gz?alt=media&token=32e7f08e-effd-4140-9e9b-4dd452151ba2\",\"installed_apps\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fdata-snapshots%2Finstalled-apps-201812201516.json.gz?alt=media&token=a9f3291b-907f-4c48-95f2-68d32ead4080\",\"sms_inbox\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fsms-inbox-logs%2Fsms_inbox_log-201812201516.json.gz?alt=media&token=47f9967b-0b69-4daa-8e46-53496cb02e9f\",\"sms_sent\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fsms-sent-logs%2Fsms_sent_log-201812201516.json.gz?alt=media&token=de31f8c5-8a3b-4cc6-be18-6f13b2863e39\"},\"created_at\":\"2018-12-20T21:17:24.972Z\",\"transaction_id\":\"ff6e8ae8-dd41-4271-936e-dbba4f8796a6\"}}";
    
    var flat_properties =
      Newtonsoft.Json.Linq.JObject.Parse(eventJson)
      .ScopeInto("data")
      .FlattenProperties<Newtonsoft.Json.Linq.JProperty>(includeValues: true)
      .ToArray();

    var flat_item = new Newtonsoft.Json.Linq.JObject(flat_properties);
    var json = Newtonsoft.Json.JsonConvert.SerializeObject(flat_item);

    var output = new LoanRequest.UpdateStatus();
    Newtonsoft.Json.JsonConvert.PopulateObject(json, output);

  }


  [Fact, TestPriority(3)]
  public void JsonToNormalized()
  {
    var event_json = "{'eventType':'LoanRequest.StatusUpdate',\"data\":{\"amount_requested\":400,\"app_version\":\"1.7.4\",\"category\":\"personal\",\"estimated_release_date\":\"2018-12-21\",\"estimated_release_hour\":12,\"estimated_release_status\":\"processing\",\"estimated_release_text\":\"El tiempo estimado de tu dep�sito es el VIERNES a las 12:00 p.m.\",\"interest_amount\":80,\"interest_rate\":20,\"loan_purpose\":\"en combustible gasolina\",\"loan_term\":28,\"outstanding_loans\":true,\"status\":\"processing\",\"sub_category\":\"Transporte\",\"user_id\":\"YjHzcgmYpDOz1gbu1YX1bfxNYax2\",\"loan_request_id\":\"-LUCTZgzeHyFdU8wmmkS\",\"created_datetime\":\"2018-12-20T21:17:24.720Z\",\"due_datetime\":\"2019-01-17T21:17:24.720Z\",\"user\":{\"devices\":{\"-LQdadX3jOz4m-jzHv3h\":true},\"loans\":{\"-LQop_ab6Xyr2YRSN_Xy\":true,\"-LUCTZgzeHyFdU8wmmkS\":true},\"bank_account\":{\"bank\":\"BI\",\"name\":\"Sergio Danilo Ju�rez �ngel\",\"number\":\"0120334610\",\"type\":\"checking\"},\"fb_user_profile\":{\"email\":\"danilojuarez11@hotmail.com\",\"first_name\":\"Sergio\",\"last_name\":\"Juarez\",\"locale\":\"es_LA\",\"name\":\"Sergio Danilo Juarez\",\"timezone\":\"-6\",\"verified\":\"true\"},\"info\":{\"active_device_id\":\"-LQdadX3jOz4m-jzHv3h\",\"install_referrer\":\"utm_source=(not%20set)&utm_medium=(not%20set)\",\"signed_up\":true},\"phone\":\"+50230538759\",\"phone_profile\":{\"months\":48,\"new_phone\":false,\"own_phone\":true},\"phone_verification\":true,\"profile\":{\"address\":{\"city\":\"Sayaxch�\",\"country\":\"GT\",\"state\":\"PETEN\",\"street\":\"barrio el centro sayaxche Pet�n\"},\"birthdate\":\"11/05/1992\",\"civil_status\":\"single\",\"dpi\":\"2225033431710\",\"email\":\"sdanilojuarez27@gmail.com\",\"first_name\":\"Sergio Danilo\",\"gender\":\"male\",\"has_kids\":false,\"id_verification\":{\"back\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Fprofile%2Freverse_document%2F13806?alt=media&token=d5828a13-d686-484a-8841-43d24092c042\",\"front\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Fprofile%2Ffront_document%2F13804?alt=media&token=c89db54f-e913-425a-b983-3ba653dca138\",\"selfie\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Fprofile%2Fselfie_document%2F13808?alt=media&token=77bcdbdb-5970-4424-9443-7385c3477f25\"},\"last_name\":\"Juarez Angel\",\"referral_source\":\"Otro\",\"tax_id\":\"88853594\"},\"status\":{\"active_loan\":\"-LUCTZgzeHyFdU8wmmkS\",\"displayed_login\":true,\"displayed_phone_verification\":true,\"displayed_tutorial\":true,\"loan_amount_level\":1,\"loan_total_applications\":2,\"loan_total_approvals\":1,\"loan_total_payment_approvals\":1},\"work_profile\":{\"employment_status\":\"employed\",\"income_amount\":3500,\"income_source\":\"compra de gasolina\n\n\n\n\",\"next_payment_date\":\"15/11/2018\",\"payment_frequency\":\"monthly\",\"profession\":\"Otro\",\"work_address\":{\"city\":\"La Libertad\",\"country\":\"GT\",\"state\":\"PETEN\",\"street\":\"asesor de cr�ditos\"}}},\"device\":{\"IMEI\":\"355648083615259\",\"instance\":\"eju7vJKPHlg\",\"SSAID\":\"e3c3456581eb7ca2\",\"last_dumps\":{\"calls\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fcall-logs%2Fcall_log-201812201516.json.gz?alt=media&token=71820a37-8f5d-43b7-8cac-2f4993767657\",\"contacts\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fcontact-list%2FcontactList-201812201516.json.gz?alt=media&token=32e7f08e-effd-4140-9e9b-4dd452151ba2\",\"installed_apps\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fdata-snapshots%2Finstalled-apps-201812201516.json.gz?alt=media&token=a9f3291b-907f-4c48-95f2-68d32ead4080\",\"sms_inbox\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fsms-inbox-logs%2Fsms_inbox_log-201812201516.json.gz?alt=media&token=47f9967b-0b69-4daa-8e46-53496cb02e9f\",\"sms_sent\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fsms-sent-logs%2Fsms_sent_log-201812201516.json.gz?alt=media&token=de31f8c5-8a3b-4cc6-be18-6f13b2863e39\"},\"messaging_token\":\"eju7vJKPHlg:APA91bEs-f5QoK3JX_zCpYYe3KZboLQgxfcjKkObIiT4__ZhzC6a7yczGkbWwxGCCPwnJ8tDnuFFOExN-62ok8Sb5WSVKxU0SkJg1FiR9Tfa2GW5I4EVXKdIJsu2PFmrlirqitGuT_u8\",\"user_id\":\"YjHzcgmYpDOz1gbu1YX1bfxNYax2\"},\"files\":{\"calls\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fcall-logs%2Fcall_log-201812201516.json.gz?alt=media&token=71820a37-8f5d-43b7-8cac-2f4993767657\",\"contacts\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fcontact-list%2FcontactList-201812201516.json.gz?alt=media&token=32e7f08e-effd-4140-9e9b-4dd452151ba2\",\"installed_apps\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fdata-snapshots%2Finstalled-apps-201812201516.json.gz?alt=media&token=a9f3291b-907f-4c48-95f2-68d32ead4080\",\"sms_inbox\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fsms-inbox-logs%2Fsms_inbox_log-201812201516.json.gz?alt=media&token=47f9967b-0b69-4daa-8e46-53496cb02e9f\",\"sms_sent\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fsms-sent-logs%2Fsms_sent_log-201812201516.json.gz?alt=media&token=de31f8c5-8a3b-4cc6-be18-6f13b2863e39\"},\"created_at\":\"2018-12-20T21:17:24.972Z\",\"transaction_id\":\"ff6e8ae8-dd41-4271-936e-dbba4f8796a6\"}}";

    var event_object =
      JObject.Parse(event_json)
      .ScopeInto("data")
      .FlattenProperties<JProperty>(includeValues: true)
      .ToArray()
      .WrapInside<JObject>();
    
    var loan = event_object
      .InferObject(new Core.Common.Models.DataSource.Firebase.Loan(),
        explicitMappings: new Dictionary<string, string>()
        {
          { "loan_request_id", "id" }
        })
      .WrapInJObject()
      .FlattenProperties<JProperty>(includeValues: true)
      .ToArray()
      .WrapInside<JObject>()
      .ToLoan_V1();

    var user = event_object
      .InferObject(new Core.Common.Models.DataSource.Firebase.User(),
        removeParentNode: new string[] { "user" },
        explicitMappings: new Dictionary <string, string>()
        {
          { "user_id", "id" }
        })
      .WrapInJObject()
      .FlattenProperties<JProperty>(includeValues: true)
      .ToArray()
      .WrapInside<JObject>()
      .ToUser_V1();
  }

}


public static class LoanRequest
{

  public class UpdateStatus
  {
    public string eventType { get; set; }

    public string amount_requested { get; set; }
    public string app_version { get; set; }
    public string category { get; set; }
    public string estimated_release_date { get; set; }
    public string estimated_release_hour { get; set; }
    public string estimated_release_status { get; set; }
    public string estimated_release_text { get; set; }
    public string interest_amount { get; set; }
    public string interest_rate { get; set; }
    public string loan_purpose { get; set; }
    public string loan_term { get; set; }
    public string outstanding_loans { get; set; }
    public string status { get; set; }
    public string sub_category { get; set; }
    public string user_id { get; set; }
    public string loan_request_id { get; set; }
    public string created_datetime { get; set; }
    public string due_datetime { get; set; }
    public string created_at { get; set; }
    public string transaction_id { get; set; }

    public string device___IMEI { get; set; }
    public string device___instance { get; set; }
    public string device___SSAID { get; set; }
    public string device___messaging_token { get; set; }
    public string device___user_id { get; set; }

    public string files___calls { get; set; }
    public string files___contacts { get; set; }
    public string files___installed_apps { get; set; }
    public string files___sms_inbox { get; set; }
    public string files___sms_sent { get; set; }

    public string device___last_dumps___calls { get; set; }
    public string device___last_dumps___contacts { get; set; }
    public string device___last_dumps___installed_apps { get; set; }
    public string device___last_dumps___sms_inbox { get; set; }
    public string device___last_dumps___sms_sent { get; set; }

    public string user___devices { get; set; }
    public string user___loans { get; set; }
    public string user___phone { get; set; }
    public string user___phone_verification { get; set; }
    public string user___work_profile___profession { get; set; }
    public string user___bank_account___bank { get; set; }
    public string user___bank_account___name { get; set; }
    public string user___bank_account___number { get; set; }
    public string user___bank_account___type { get; set; }
    public string user___fb_user_profile___email { get; set; }
    public string user___fb_user_profile___first_name { get; set; }
    public string user___fb_user_profile___last_name { get; set; }
    public string user___fb_user_profile___locale { get; set; }
    public string user___fb_user_profile___name { get; set; }
    public string user___fb_user_profile___timezone { get; set; }
    public string user___fb_user_profile___verified { get; set; }
    public string user___info___active_device_id { get; set; }
    public string user___info___install_referrer { get; set; }
    public string user___info___signed_up { get; set; }
    public string user___phone_profile___months { get; set; }
    public string user___phone_profile___new_phone { get; set; }
    public string user___phone_profile___own_phone { get; set; }
    public string user___profile___birthdate { get; set; }
    public string user___profile___civil_status { get; set; }
    public string user___profile___dpi { get; set; }
    public string user___profile___email { get; set; }
    public string user___profile___first_name { get; set; }
    public string user___profile___gender { get; set; }
    public string user___profile___has_kids { get; set; }
    public string user___profile___last_name { get; set; }
    public string user___profile___referral_source { get; set; }
    public string user___profile___tax_id { get; set; }
    public string user___status___active_loan { get; set; }
    public string user___status___displayed_login { get; set; }
    public string user___status___displayed_phone_verification { get; set; }
    public string user___status___displayed_tutorial { get; set; }
    public string user___status___loan_amount_level { get; set; }
    public string user___status___loan_total_applications { get; set; }
    public string user___status___loan_total_approvals { get; set; }
    public string user___status___loan_total_payment_approvals { get; set; }
    public string user___work_profile___employment_status { get; set; }
    public string user___work_profile___income_amount { get; set; }
    public string user___work_profile___income_source { get; set; }
    public string user___work_profile___next_payment_date { get; set; }
    public string user___work_profile___payment_frequency { get; set; }
    public string user___profile___address___city { get; set; }
    public string user___profile___address___country { get; set; }
    public string user___profile___address___state { get; set; }
    public string user___profile___address___street { get; set; }
    public string user___profile___id_verification___back { get; set; }
    public string user___profile___id_verification___front { get; set; }
    public string user___profile___id_verification___selfie { get; set; }
    public string user___work_profile___work_address___city { get; set; }
    public string user___work_profile___work_address___country { get; set; }
    public string user___work_profile___work_address___state { get; set; }
    public string user___work_profile___work_address___street { get; set; }
  }

}