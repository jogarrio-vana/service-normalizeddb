using Core.Data;
using Core.Tests;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

/// <summary>
/// This is to be run from a single machine in parralel threads
/// </summary>
[TestCaseOrderer("Core.Tests.PriorityOrderer", "Core.Tests")]
public class BatchProcessing_V1
{
  private readonly ITestOutputHelper logger;

  public BatchProcessing_V1(ITestOutputHelper output)
  {
    this.logger = output;
  }

  public static bool BatchProcess_Firebase_Users_Called;
  public static bool BatchProcess_Firebase_Loans_Called;
  public static bool BatchProcess_Airtable_Deposits_Called;
  public static bool BatchProcess_S3_LoanRequests_Called;
  public static bool BatchProcess_Firebase_LoanRequests_Called;

  [Fact, TestPriority(1000)]
  public void AssertCompletion()
  {
    Assert.True(BatchProcess_Firebase_Users_Called);
    Assert.True(BatchProcess_Firebase_Loans_Called);
    Assert.True(BatchProcess_Airtable_Deposits_Called);
    Assert.True(BatchProcess_S3_LoanRequests_Called);
    Assert.True(BatchProcess_Firebase_LoanRequests_Called);
  }

  [Fact, TestPriority(1)]
  public void BatchProcess_Firebase_Users()
  {
    System.Diagnostics.Debug.WriteLine(nameof(BatchProcess_Firebase_Users));

    const int workerCount = 40;

    var processing = new Core.Processing.BatchProcess();
    var queue = new BlockingCollection<dynamic>();

    Action<dynamic> work =
      (item) =>
      {
        //CONSUMER
        try
        {
          processing.Consumer_Users(item);
        }
        catch (Exception ex)
        {
          System.Diagnostics.Debugger.Break();
          throw ex;
        }
      };

    Task.Run(() =>
    {
      //PRODUCER
      processing.Producer_Users(queue);
      queue.CompleteAdding();
    });

    queue
      .GetConsumingEnumerable()
      .AsParallel()
      .WithDegreeOfParallelism(workerCount)
      .WithMergeOptions(ParallelMergeOptions.NotBuffered)
      .ForAll(work);

    queue.Dispose();
    BatchProcess_Firebase_Users_Called = true;
  }


  [Fact, TestPriority(2)]
  public void BatchProcess_Firebase_Loans()
  {
    System.Diagnostics.Debug.WriteLine(nameof(BatchProcess_Firebase_Loans));

    const int workerCount = 40;

    var processing = new Core.Processing.BatchProcess();
    var queue = new BlockingCollection<dynamic>();

    Action<dynamic> work =
      (item) =>
      {
        //CONSUMER
        try
        {
          processing.Consumer_Loans(item);
        }
        catch (Exception ex)
        {
          System.Diagnostics.Debugger.Break();
          throw ex;
        }
      };

    Task.Run(() =>
    {
      //PRODUCER
      processing.Producer_Loans(queue);
      queue.CompleteAdding();
    });

    queue
      .GetConsumingEnumerable()
      .AsParallel()
      .WithDegreeOfParallelism(workerCount)
      .WithMergeOptions(ParallelMergeOptions.NotBuffered)
      .ForAll(work);

    queue.Dispose();
    BatchProcess_Firebase_Loans_Called = true;
  }

  [Fact, TestPriority(3)]
  public void BatchProcess_Airtable_Deposits()
  {
    System.Diagnostics.Debug.WriteLine(nameof(BatchProcess_Airtable_Deposits));

    const int workerCount = 40;

    var processing = new Core.Processing.BatchProcess();
    var queue = new BlockingCollection<dynamic>();

    Action<dynamic> work =
      (item) =>
      {
        //CONSUMER
        processing.Consumer_Deposits(item);
      };

    Task.Run(() =>
    {
      //PRODUCER
      processing.Producer_Deposits(queue);
      queue.CompleteAdding();
    });

    queue
      .GetConsumingEnumerable()
      .AsParallel()
      .WithDegreeOfParallelism(workerCount)
      .WithMergeOptions(ParallelMergeOptions.NotBuffered)
      .ForAll(work);

    queue.Dispose();
    BatchProcess_Airtable_Deposits_Called = true;
  }
  
  [Fact, TestPriority(4)]
  public void BatchProcess_S3_LoanRequests()
  {
    BatchProcess_S3_LoanRequests_Called = true;
    return;

    System.Diagnostics.Debug.WriteLine(nameof(BatchProcess_S3_LoanRequests));

    const int workerCount = 1;

    var processing = new Core.Processing.BatchProcess();
    var queue = new BlockingCollection<dynamic>();

    Action<dynamic> work =
      (item) =>
      {
        //CONSUMER
        //processing.Consumer_LoanRequests(item);
      };

    Task.Run(() =>
    {
      //PRODUCER
      processing.Producer_LoanRequests(queue);
      queue.CompleteAdding();
    });

    queue
      .GetConsumingEnumerable()
      .AsParallel()
      .WithDegreeOfParallelism(workerCount)
      .WithMergeOptions(ParallelMergeOptions.NotBuffered)
      .ForAll(work);

    queue.Dispose();
    BatchProcess_S3_LoanRequests_Called = true;
  }


  [Fact, TestPriority(5)]
  public void BatchProcess_Firebase_LoanRequests()
  {
    return;
    System.Diagnostics.Debug.WriteLine(nameof(BatchProcess_Firebase_LoanRequests));

    const int workerCount = 1;

    var processing = new Core.Processing.BatchProcess();
    var queue = new BlockingCollection<dynamic>();

    Action<dynamic> work =
      (item) =>
      {
        //CONSUMER
        //processing.Consumer_LoanRequests(item);
      };

    Task.Run(() =>
    {
      //PRODUCER
      processing.Producer_Firebase_LoanRequests(queue);
      queue.CompleteAdding();
    });

    queue
      .GetConsumingEnumerable()
      .AsParallel()
      .WithDegreeOfParallelism(workerCount)
      .WithMergeOptions(ParallelMergeOptions.NotBuffered)
      .ForAll(work);

    queue.Dispose();
    BatchProcess_Firebase_LoanRequests_Called = true;
  }


}