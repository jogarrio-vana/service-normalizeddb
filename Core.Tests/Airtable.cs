using Core.Data;
using Core.Processing;
using Core.Tests;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


/// <summary>
/// This is to be run from a single machine in parralel threads
/// </summary>
[TestCaseOrderer("Core.Tests.PriorityOrderer", "Core.Tests")]
public class Airtable
{
  
  [Fact, TestPriority(1)]
  public void JsonToClass()
  {
    var eventJson = "{'eventType':'LoanRequest.StatusUpdate',\"data\":{\"amount_requested\":400,\"app_version\":\"1.7.4\",\"category\":\"personal\",\"estimated_release_date\":\"2018-12-21\",\"estimated_release_hour\":12,\"estimated_release_status\":\"processing\",\"estimated_release_text\":\"El tiempo estimado de tu dep�sito es el VIERNES a las 12:00 p.m.\",\"interest_amount\":80,\"interest_rate\":20,\"loan_purpose\":\"en combustible gasolina\",\"loan_term\":28,\"outstanding_loans\":true,\"status\":\"processing\",\"sub_category\":\"Transporte\",\"user_id\":\"YjHzcgmYpDOz1gbu1YX1bfxNYax2\",\"loan_request_id\":\"-LUCTZgzeHyFdU8wmmkS\",\"created_datetime\":\"2018-12-20T21:17:24.720Z\",\"due_datetime\":\"2019-01-17T21:17:24.720Z\",\"user\":{\"devices\":{\"-LQdadX3jOz4m-jzHv3h\":true},\"loans\":{\"-LQop_ab6Xyr2YRSN_Xy\":true,\"-LUCTZgzeHyFdU8wmmkS\":true},\"bank_account\":{\"bank\":\"BI\",\"name\":\"Sergio Danilo Ju�rez �ngel\",\"number\":\"0120334610\",\"type\":\"checking\"},\"fb_user_profile\":{\"email\":\"danilojuarez11@hotmail.com\",\"first_name\":\"Sergio\",\"last_name\":\"Juarez\",\"locale\":\"es_LA\",\"name\":\"Sergio Danilo Juarez\",\"timezone\":\"-6\",\"verified\":\"true\"},\"info\":{\"active_device_id\":\"-LQdadX3jOz4m-jzHv3h\",\"install_referrer\":\"utm_source=(not%20set)&utm_medium=(not%20set)\",\"signed_up\":true},\"phone\":\"+50230538759\",\"phone_profile\":{\"months\":48,\"new_phone\":false,\"own_phone\":true},\"phone_verification\":true,\"profile\":{\"address\":{\"city\":\"Sayaxch�\",\"country\":\"GT\",\"state\":\"PETEN\",\"street\":\"barrio el centro sayaxche Pet�n\"},\"birthdate\":\"11/05/1992\",\"civil_status\":\"single\",\"dpi\":\"2225033431710\",\"email\":\"sdanilojuarez27@gmail.com\",\"first_name\":\"Sergio Danilo\",\"gender\":\"male\",\"has_kids\":false,\"id_verification\":{\"back\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Fprofile%2Freverse_document%2F13806?alt=media&token=d5828a13-d686-484a-8841-43d24092c042\",\"front\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Fprofile%2Ffront_document%2F13804?alt=media&token=c89db54f-e913-425a-b983-3ba653dca138\",\"selfie\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Fprofile%2Fselfie_document%2F13808?alt=media&token=77bcdbdb-5970-4424-9443-7385c3477f25\"},\"last_name\":\"Juarez Angel\",\"referral_source\":\"Otro\",\"tax_id\":\"88853594\"},\"status\":{\"active_loan\":\"-LUCTZgzeHyFdU8wmmkS\",\"displayed_login\":true,\"displayed_phone_verification\":true,\"displayed_tutorial\":true,\"loan_amount_level\":1,\"loan_total_applications\":2,\"loan_total_approvals\":1,\"loan_total_payment_approvals\":1},\"work_profile\":{\"employment_status\":\"employed\",\"income_amount\":3500,\"income_source\":\"compra de gasolina\n\n\n\n\",\"next_payment_date\":\"15/11/2018\",\"payment_frequency\":\"monthly\",\"profession\":\"Otro\",\"work_address\":{\"city\":\"La Libertad\",\"country\":\"GT\",\"state\":\"PETEN\",\"street\":\"asesor de cr�ditos\"}}},\"device\":{\"IMEI\":\"355648083615259\",\"instance\":\"eju7vJKPHlg\",\"SSAID\":\"e3c3456581eb7ca2\",\"last_dumps\":{\"calls\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fcall-logs%2Fcall_log-201812201516.json.gz?alt=media&token=71820a37-8f5d-43b7-8cac-2f4993767657\",\"contacts\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fcontact-list%2FcontactList-201812201516.json.gz?alt=media&token=32e7f08e-effd-4140-9e9b-4dd452151ba2\",\"installed_apps\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fdata-snapshots%2Finstalled-apps-201812201516.json.gz?alt=media&token=a9f3291b-907f-4c48-95f2-68d32ead4080\",\"sms_inbox\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fsms-inbox-logs%2Fsms_inbox_log-201812201516.json.gz?alt=media&token=47f9967b-0b69-4daa-8e46-53496cb02e9f\",\"sms_sent\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fsms-sent-logs%2Fsms_sent_log-201812201516.json.gz?alt=media&token=de31f8c5-8a3b-4cc6-be18-6f13b2863e39\"},\"messaging_token\":\"eju7vJKPHlg:APA91bEs-f5QoK3JX_zCpYYe3KZboLQgxfcjKkObIiT4__ZhzC6a7yczGkbWwxGCCPwnJ8tDnuFFOExN-62ok8Sb5WSVKxU0SkJg1FiR9Tfa2GW5I4EVXKdIJsu2PFmrlirqitGuT_u8\",\"user_id\":\"YjHzcgmYpDOz1gbu1YX1bfxNYax2\"},\"files\":{\"calls\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fcall-logs%2Fcall_log-201812201516.json.gz?alt=media&token=71820a37-8f5d-43b7-8cac-2f4993767657\",\"contacts\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fcontact-list%2FcontactList-201812201516.json.gz?alt=media&token=32e7f08e-effd-4140-9e9b-4dd452151ba2\",\"installed_apps\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fdata-snapshots%2Finstalled-apps-201812201516.json.gz?alt=media&token=a9f3291b-907f-4c48-95f2-68d32ead4080\",\"sms_inbox\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fsms-inbox-logs%2Fsms_inbox_log-201812201516.json.gz?alt=media&token=47f9967b-0b69-4daa-8e46-53496cb02e9f\",\"sms_sent\":\"https://firebasestorage.googleapis.com/v0/b/vanadb-5d00a.appspot.com/o/Users%2FYjHzcgmYpDOz1gbu1YX1bfxNYax2%2Flogs%2Fsms-sent-logs%2Fsms_sent_log-201812201516.json.gz?alt=media&token=de31f8c5-8a3b-4cc6-be18-6f13b2863e39\"},\"created_at\":\"2018-12-20T21:17:24.972Z\",\"transaction_id\":\"ff6e8ae8-dd41-4271-936e-dbba4f8796a6\"}}";
    
    var flat_properties =
      Newtonsoft.Json.Linq.JObject.Parse(eventJson)
      .ScopeInto("data")
      .FlattenProperties<Newtonsoft.Json.Linq.JProperty>(includeValues: true)
      .ToArray();

    var flat_item = new Newtonsoft.Json.Linq.JObject(flat_properties);
    var json = Newtonsoft.Json.JsonConvert.SerializeObject(flat_item);

    var output = new LoanRequest.UpdateStatus();
    Newtonsoft.Json.JsonConvert.PopulateObject(json, output);

  }

}
