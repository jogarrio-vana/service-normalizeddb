using System;
using System.IO;
using System.Text;

using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Amazon.Lambda.KinesisEvents;
using Amazon.Lambda.S3Events;
using Amazon.S3;
using Amazon.S3.Util;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Core.Data;
using Core.Common.Models.Normalized.V1;
using Newtonsoft.Json;

using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using System.Dynamic;
using Microsoft.Extensions.Configuration;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

public static class MainEntry
{
  public static void Main(string[] args)
  {

    var lambda = new Function();

    var S3_ACH_Bancoindustrial = "{\"Records\":[{\"eventVersion\":\"2.1\",\"eventSource\":\"aws:s3\",\"awsRegion\":\"us-east-1\",\"eventTime\":\"2019-01-25T19:02:01.241Z\",\"eventName\":\"ObjectCreated:Put\",\"userIdentity\":{\"principalId\":\"AWS:AIDAI34VOJMN4KWVKPSO6\"},\"requestParameters\":{\"sourceIPAddress\":\"190.148.69.26\"},\"responseElements\":{\"x-amz-request-id\":\"AF63C238EE13C9F3\",\"x-amz-id-2\":\"jxjAeuiYQT9sH8fsuO6MTaO43gZ0YdMNIeTXOv2eBq+xCDqQa7I6ZpON3UoNLqQNIryay5AMwN8=\"},\"s3\":{\"s3SchemaVersion\":\"1.0\",\"configurationId\":\"VSToolkitQuickCreate_service-normalizedDb-ingress\",\"bucket\":{\"name\":\"vana-bank-transactions\",\"ownerIdentity\":{\"principalId\":\"AYC17MFCJD2Z8\"},\"arn\":\"arn:aws:s3:::vana-bank-transactions\"},\"object\":{\"key\":\"ach-bancoindustrial/5_Procesado_Parcialmente_30012019_35818_PM.txt\",\"size\":190,\"eTag\":\"ce9db53b8a62f2e6b1425e162793611d\",\"sequencer\":\"005C4B5D2931E1EAED\"}}}]}";
    var S3_Cheques_Bancoindustrial = "";
    var S3_Cheques_Banrural = "{\"Records\":[{\"eventVersion\":\"2.1\",\"eventSource\":\"aws:s3\",\"awsRegion\":\"us-east-1\",\"eventTime\":\"2019-01-25T19:02:01.241Z\",\"eventName\":\"ObjectCreated:Put\",\"userIdentity\":{\"principalId\":\"AWS:AIDAI34VOJMN4KWVKPSO6\"},\"requestParameters\":{\"sourceIPAddress\":\"190.148.69.26\"},\"responseElements\":{\"x-amz-request-id\":\"AF63C238EE13C9F3\",\"x-amz-id-2\":\"jxjAeuiYQT9sH8fsuO6MTaO43gZ0YdMNIeTXOv2eBq+xCDqQa7I6ZpON3UoNLqQNIryay5AMwN8=\"},\"s3\":{\"s3SchemaVersion\":\"1.0\",\"configurationId\":\"VSToolkitQuickCreate_service-normalizedDb-ingress\",\"bucket\":{\"name\":\"vana-bank-transactions\",\"ownerIdentity\":{\"principalId\":\"AYC17MFCJD2Z8\"},\"arn\":\"arn:aws:s3:::vana-bank-transactions\"},\"object\":{\"key\":\"cheques-banrural/bank_transactions_banrural.csv\",\"size\":190,\"eTag\":\"ce9db53b8a62f2e6b1425e162793611d\",\"sequencer\":\"005C4B5D2931E1EAED\"}}}]}";
    var S3_Invoices_Infile = "{\"Records\":[{\"eventVersion\":\"2.1\",\"eventSource\":\"aws:s3\",\"awsRegion\":\"us-east-1\",\"eventTime\":\"2019-01-25T19:02:01.241Z\",\"eventName\":\"ObjectCreated:Put\",\"userIdentity\":{\"principalId\":\"AWS:AIDAI34VOJMN4KWVKPSO6\"},\"requestParameters\":{\"sourceIPAddress\":\"190.148.69.26\"},\"responseElements\":{\"x-amz-request-id\":\"AF63C238EE13C9F3\",\"x-amz-id-2\":\"jxjAeuiYQT9sH8fsuO6MTaO43gZ0YdMNIeTXOv2eBq+xCDqQa7I6ZpON3UoNLqQNIryay5AMwN8=\"},\"s3\":{\"s3SchemaVersion\":\"1.0\",\"configurationId\":\"VSToolkitQuickCreate_service-normalizedDb-ingress\",\"bucket\":{\"name\":\"vana-bank-transactions\",\"ownerIdentity\":{\"principalId\":\"AYC17MFCJD2Z8\"},\"arn\":\"arn:aws:s3:::vana-bank-transactions\"},\"object\":{\"key\":\"invoices-infile/Transacciones_1_2019.csv\",\"size\":190,\"eTag\":\"ce9db53b8a62f2e6b1425e162793611d\",\"sequencer\":\"005C4B5D2931E1EAED\"}}}]}";
    var S3_Transfers_Bancoindustrial = "{\"Records\":[{\"eventVersion\":\"2.1\",\"eventSource\":\"aws:s3\",\"awsRegion\":\"us-east-1\",\"eventTime\":\"2019-01-25T19:02:01.241Z\",\"eventName\":\"ObjectCreated:Put\",\"userIdentity\":{\"principalId\":\"AWS:AIDAI34VOJMN4KWVKPSO6\"},\"requestParameters\":{\"sourceIPAddress\":\"190.148.69.26\"},\"responseElements\":{\"x-amz-request-id\":\"AF63C238EE13C9F3\",\"x-amz-id-2\":\"jxjAeuiYQT9sH8fsuO6MTaO43gZ0YdMNIeTXOv2eBq+xCDqQa7I6ZpON3UoNLqQNIryay5AMwN8=\"},\"s3\":{\"s3SchemaVersion\":\"1.0\",\"configurationId\":\"VSToolkitQuickCreate_service-normalizedDb-ingress\",\"bucket\":{\"name\":\"vana-bank-transactions\",\"ownerIdentity\":{\"principalId\":\"AYC17MFCJD2Z8\"},\"arn\":\"arn:aws:s3:::vana-bank-transactions\"},\"object\":{\"key\":\"transfers-bancoindustrial/I152801L171.txt\",\"size\":190,\"eTag\":\"ce9db53b8a62f2e6b1425e162793611d\",\"sequencer\":\"005C4B5D2931E1EAED\"}}}]}";

    var BankTransaction_Created = "{\"Records\":[{\"kinesis\":{\"kinesisSchemaVersion\":\"1.0\",\"partitionKey\":\"960eb87b-5ad5-4973-a8a6-1b0fe5f3cce6\",\"sequenceNumber\":\"49589647337537105873604401508019018656500765921548173314\",\"data\":\"eyJldmVudFR5cGUiOiJCYW5rVHJhbnNhY3Rpb24uQ3JlYXRlZCIsImRhdGEiOnsicmVmZXJlbmNlIjoiMCIsImJhbmtfdHJhbnNhY3Rpb25faWQiOiJiYjgzOWY3Zi0wZjAzLTQzYzEtYWRkYi05Zjk0ODFkMTMzZDAiLCJ0eXBlIjoicGF5b3V0IiwibG9hbl9pZCI6ImNmZGJmZTQyLTEyNmMtNGZjZi04ODQ2LTlmZjgyN2Y0MTQ0ZSIsImxvYW5fcmVxdWVzdF9pZCI6Ii1MVkdXcE9UNmw3R2tMR0JnVjh3IiwicGF5bWVudF9pZCI6Ii1MWDluSno4LUU2dGJ5QXBNMUIwIiwidXNlcl9pZCI6IlpMbXVwV2pVTGZRWndkcWZPSHd6Wk5GUFlzdzEiLCJjcmVhdGVkX2F0IjoiMjAxOS0wMS0yNlQyMzo1MToxNi4yNjlaIiwiZGViaXQiOjAsImNyZWRpdCI6MTQ0MCwicGF5bWVudCI6eyJkYXRlIjoiMjAxOS0wMS0yNiIsInBheW1lbnRfaWQiOiItTFg5bkp6OC1FNnRieUFwTTFCMCIsInVzZXJfaWQiOiJaTG11cFdqVUxmUVp3ZHFmT0h3elpORlBZc3cxIiwiY3JlYXRlZF9hdCI6IjIwMTktMDEtMjZUMTU6MzU6NDAuNTUwWiIsImltYWdlIjp7InJlbW90ZV91cmwiOiJodHRwczovL2ZpcmViYXNlc3RvcmFnZS5nb29nbGVhcGlzLmNvbS92MC9iL3ZhbmFkYi01ZDAwYS5hcHBzcG90LmNvbS9vL1VzZXJzJTJGWkxtdXBXalVMZlFad2RxZk9Id3paTkZQWXN3MSUyRnBheW1lbnRzJTJGMjMyMDM/YWx0PW1lZGlhJnRva2VuPTY1NDcxYzg0LWM2NDctNGFiMi04ZmI5LTZiYjMxYzQ4NWUxMSIsInMzX2J1Y2tldCI6InZhbmEtcGF5bWVudHMiLCJzM19vYmplY3Rfa2V5IjoicGF5bWVudHMvWkxtdXBXalVMZlFad2RxZk9Id3paTkZQWXN3MS9jZmRiZmU0Mi0xMjZjLTRmY2YtODg0Ni05ZmY4MjdmNDE0NGUvMTU0ODUxNjk0MzQ1MS0yMzIwMyJ9LCJsb2FuX3JlcXVlc3RfaWQiOiItTFZHV3BPVDZsN0drTEdCZ1Y4dyIsInN0YXR1cyI6ImFkZGVkIiwiYW1vdW50IjoxNDQwLCJsb2FuX2lkIjoiY2ZkYmZlNDItMTI2Yy00ZmNmLTg4NDYtOWZmODI3ZjQxNDRlIiwidHlwZSI6InBheW91dCJ9LCJkYXRlIjoiMjAxOS0wMS0yNiIsImNhdGVnb3JpZXMiOnsiaW50ZXJlc3QiOjI0MCwicHJpbmNpcGFsIjoxMjAwfX19\",\"approximateArrivalTimestamp\":1.54644240652E9},\"eventSource\":\"aws:kinesis\",\"eventVersion\":\"1.0\",\"eventID\":\"shardId-000000000000:49589647337537105873604401508019018656500765921548173314\",\"eventName\":\"aws:kinesis:record\",\"invokeIdentityArn\":\"arn:aws:iam::384120103923:role/lambda_full_execution\",\"awsRegion\":\"us-east-1\",\"eventSourceARN\":\"arn:aws:kinesis:us-east-1:384120103923:stream/loan-stream\"}]}";
    var Payment_Created = "{\"Records\":[{\"kinesis\":{\"kinesisSchemaVersion\":\"1.0\",\"partitionKey\":\"960eb87b-5ad5-4973-a8a6-1b0fe5f3cce6\",\"sequenceNumber\":\"49589647337537105873604401508019018656500765921548173314\",\"data\":\"ewogICJldmVudFR5cGUiOiAiUGF5bWVudC5DcmVhdGVkIiwKICAiZGF0YSI6IHsKICAgICJyZWZlcmVuY2UiOiAiMzA2NSIsCiAgICAibG9hbl9pZCI6ICIxMzVjMzBiMC0zNGNmLTQyMzUtODU0MC00NjdhZWUwY2U0MWQiLAogICAgImFtb3VudCI6IDExNTAsCiAgICAiYmFuayI6ICJCSSIsCiAgICAiZGF0ZSI6ICIyMDE5LTAxLTIzIiwKICAgICJ1c2VyX2lkIjogIjBHRVJRYjdTUGZSSE5Cd1lDSmJZMDNKRXUzRDIiLAogICAgInR5cGUiOiAicGF5b3V0IiwKICAgICJwYXltZW50X2lkIjogIi1MV3h3Q2UwVnNyYmxRcjlzOXJlIiwKICAgICJsb2FuX3JlcXVlc3RfaWQiOiAiLUxXeHNFWUxKTXF1ZTJLVGE4SFIiLAogICAgInN0YXR1cyI6ICJ1cGxvYWRpbmciLAogICAgImltYWdlIjogewogICAgICAicmVtb3RlX3VybCI6ICJodHRwczovL2ZpcmViYXNlc3RvcmFnZS5nb29nbGVhcGlzLmNvbS92MC9iL3ZhbmF0ZXN0aW5nLmFwcHNwb3QuY29tL28vVXNlcnMlMkYwR0VSUWI3U1BmUkhOQndZQ0piWTAzSkV1M0QyJTJGcGF5bWVudHMlMkZpbWFnZSUzQTIwNDg/YWx0PW1lZGlhJnRva2VuPWNmNzI5M2Q0LTIxMzQtNGI2My05NDJiLWZmNWZiYjIxZTYxYiIKICAgIH0sCiAgICAiY3JlYXRlZF9hdCI6ICIyMDE5LTAxLTI0VDAzOjM5OjI1LjIzMFoiLAogICAgInVwZGF0ZWRfYXQiOiAiMjAxOS0wMS0yNFQwMzozOToyNS4yMzBaIgogIH0KfQo=\",\"approximateArrivalTimestamp\":1.54644240652E9},\"eventSource\":\"aws:kinesis\",\"eventVersion\":\"1.0\",\"eventID\":\"shardId-000000000000:49589647337537105873604401508019018656500765921548173314\",\"eventName\":\"aws:kinesis:record\",\"invokeIdentityArn\":\"arn:aws:iam::384120103923:role/lambda_full_execution\",\"awsRegion\":\"us-east-1\",\"eventSourceARN\":\"arn:aws:kinesis:us-east-1:384120103923:stream/loan-stream\"}]}";
    var Payment_ImageUploaded = "{\"Records\":[{\"kinesis\":{\"kinesisSchemaVersion\":\"1.0\",\"partitionKey\":\"960eb87b-5ad5-4973-a8a6-1b0fe5f3cce6\",\"sequenceNumber\":\"49589647337537105873604401508019018656500765921548173314\",\"data\":\"ewogICJldmVudFR5cGUiOiAiUGF5bWVudC5JbWFnZVVwbG9hZGVkIiwKICAiZGF0YSI6IHsKICAgICJkYXRlIjogIjIwMTktMDEtMjMiLAogICAgInBheW1lbnRfaWQiOiAiLUxXeHdDZTBWc3JibFFyOXM5cmUiLAogICAgInVzZXJfaWQiOiAiMEdFUlFiN1NQZlJITkJ3WUNKYlkwM0pFdTNEMiIsCiAgICAiY3JlYXRlZF9hdCI6ICIyMDE5LTAxLTI0VDAzOjM5OjI1LjIzMFoiLAogICAgImltYWdlIjogewogICAgICAicmVtb3RlX3VybCI6ICJodHRwczovL2ZpcmViYXNlc3RvcmFnZS5nb29nbGVhcGlzLmNvbS92MC9iL3ZhbmF0ZXN0aW5nLmFwcHNwb3QuY29tL28vVXNlcnMlMkYwR0VSUWI3U1BmUkhOQndZQ0piWTAzSkV1M0QyJTJGcGF5bWVudHMlMkZpbWFnZSUzQTIwNDg/YWx0PW1lZGlhJnRva2VuPWNmNzI5M2Q0LTIxMzQtNGI2My05NDJiLWZmNWZiYjIxZTYxYiIsCiAgICAgICJzM19idWNrZXQiOiAidmFuYS1wYXltZW50cy1kZXYiLAogICAgICAiczNfb2JqZWN0X2tleSI6ICJwYXltZW50cy8wR0VSUWI3U1BmUkhOQndZQ0piWTAzSkV1M0QyLzEzNWMzMGIwLTM0Y2YtNDIzNS04NTQwLTQ2N2FlZTBjZTQxZC8xNTQ4MzAxMTY3ODQ5LWltYWdlOjIwNDgiCiAgICB9LAogICAgImxvYW5fcmVxdWVzdF9pZCI6ICItTFd4c0VZTEpNcXVlMktUYThIUiIsCiAgICAic3RhdHVzIjogInByb2Nlc3NpbmciLAogICAgImFtb3VudCI6IDExNTAsCiAgICAibG9hbl9pZCI6ICIxMzVjMzBiMC0zNGNmLTQyMzUtODU0MC00NjdhZWUwY2U0MWQiLAogICAgInR5cGUiOiAicGF5b3V0IgogIH0KfQo=\",\"approximateArrivalTimestamp\":1.54644240652E9},\"eventSource\":\"aws:kinesis\",\"eventVersion\":\"1.0\",\"eventID\":\"shardId-000000000000:49589647337537105873604401508019018656500765921548173314\",\"eventName\":\"aws:kinesis:record\",\"invokeIdentityArn\":\"arn:aws:iam::384120103923:role/lambda_full_execution\",\"awsRegion\":\"us-east-1\",\"eventSourceARN\":\"arn:aws:kinesis:us-east-1:384120103923:stream/loan-stream\"}]}";
    var Payment_StatusUpdated = "{\"Records\":[{\"kinesis\":{\"kinesisSchemaVersion\":\"1.0\",\"partitionKey\":\"960eb87b-5ad5-4973-a8a6-1b0fe5f3cce6\",\"sequenceNumber\":\"49589647337537105873604401508019018656500765921548173314\",\"data\":\"ewogICJldmVudFR5cGUiOiAiUGF5bWVudC5TdGF0dXNVcGRhdGVkIiwKICAiZGF0YSI6IHsKICAgICJzdGF0dXMiOiAiYWRkZWQiLAogICAgInVzZXJfaWQiOiAiMEdFUlFiN1NQZlJITkJ3WUNKYlkwM0pFdTNEMiIsCiAgICAiYmFua190cmFuc2FjdGlvbiI6IHsKICAgICAgInJlZmVyZW5jZSI6ICIzMDY1IgogICAgfSwKICAgICJsb2FuX2lkIjogIjEzNWMzMGIwLTM0Y2YtNDIzNS04NTQwLTQ2N2FlZTBjZTQxZCIsCiAgICAibG9hbl9yZXF1ZXN0X2lkIjogIi1MV3hzRVlMSk1xdWUyS1RhOEhSIiwKICAgICJwYXltZW50X2lkIjogIi1MV3h3Q2UwVnNyYmxRcjlzOXJlIiwKICAgICJ0cmFuc2FjdGlvbl9pZCI6ICI3NjhhY2U0Ny0yMTVmLTQ0YTctYTlhMi0yYjE3MzY0ZjU4MzUiCiAgfQp9Cg==\",\"approximateArrivalTimestamp\":1.54644240652E9},\"eventSource\":\"aws:kinesis\",\"eventVersion\":\"1.0\",\"eventID\":\"shardId-000000000000:49589647337537105873604401508019018656500765921548173314\",\"eventName\":\"aws:kinesis:record\",\"invokeIdentityArn\":\"arn:aws:iam::384120103923:role/lambda_full_execution\",\"awsRegion\":\"us-east-1\",\"eventSourceARN\":\"arn:aws:kinesis:us-east-1:384120103923:stream/loan-stream\"}]}";

    lambda.Handler(JObject.Parse(S3_ACH_Bancoindustrial), null);
  }
}

public class Function
{
  public static IAmazonS3 S3Client { get; set; }
  public static IConfiguration configuration = null;

  /// <summary>
  /// Default constructor. This constructor is used by Lambda to construct the instance. When invoked in a Lambda environment
  /// the AWS credentials will come from the IAM role associated with the function and the AWS region will be set to the
  /// region the Lambda function is executed in.
  /// </summary>
  public Function()
  {
    Function.S3Client = new AmazonS3Client(Amazon.RegionEndpoint.USEast1);
  }

  public static void Initialize(ILambdaContext context = null)
  {
    if (Function.configuration != null) return;
    context?.Logger.LogLine(">>>> [DBG] Initializing function");

    var alias = context?.InvokedFunctionArn?.Substring(context.InvokedFunctionArn.LastIndexOf(":") + 1);
    var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

    Function.configuration = new Microsoft.Extensions.Configuration.ConfigurationBuilder()
     .SetBasePath(Directory.GetCurrentDirectory())
     .AddJsonFile("appsettings.dev.json", optional: false, reloadOnChange: false)
     .AddJsonFile($"appsettings.{alias}.json", optional: true, reloadOnChange: false)
     .AddJsonFile($"appsettings.{environment}.json", optional: true, reloadOnChange: false)
     .Build();

    context?.Logger.LogLine(">>>> [DBG] Environment = " + (environment ?? alias ?? "dev"));
  }

  /// <summary>
  /// Constructs an instance with a preconfigured S3 client. This can be used for testing the outside of the Lambda environment.
  /// </summary>
  /// <param name="s3Client"></param>
  public Function(IAmazonS3 s3Client)
  {
    Function.S3Client = s3Client;
  }

  /// <summary>
  /// This method is called for every Lambda invocation. This method takes in an S3 event object and can be used 
  /// to respond to S3 notifications.
  /// </summary>
  /// <param name="evnt"></param>
  /// <param name="context"></param>
  /// <returns></returns>
  public void Handler(JObject lambdaEvent, ILambdaContext context)
  {
    Initialize(context);

    var eventType = GetEventType(lambdaEvent) ?? "OTHER";
    var events = Parse_Events(lambdaEvent);
    context?.Logger.LogLine(">>>> [DBG] EventType: " + eventType);

    // https://forums.aws.amazon.com/thread.jspa?threadID=250965

    switch (eventType)
    {
      case "Scheduled":
        break;

      case "Payment.StatusUpdated":
      case "Payment.ImageUploaded":
      case "Payment.Created":
        break;

      case "cheques-bancoindustrial":
      case "cheques-banrural":
      case "transfers-bancoindustrial":
        context?.Logger.LogLine(lambdaEvent.ToString(Newtonsoft.Json.Formatting.Indented));
        foreach (var item in Parse_S3Events(lambdaEvent))
          context?.Logger.LogLine(">>>> [DBG] RECORD: " + item);
        break;

      case "ach-bancoindustrial":
        context?.Logger.LogLine(lambdaEvent.ToString(Newtonsoft.Json.Formatting.Indented));

        var rows = new List<IDictionary<string, Object>>();
        foreach (var item in To_S3Event(lambdaEvent).Records)
        {
          context?.Logger.LogLine(">>>> [DBG] RECORD: " + item.S3.Object.Key.ToString());
          var content = item.S3.Download(Function.S3Client).ToText();
          rows.AddRange(content.ParseCSV(delimeter: "|"));
        }
        
        rows
          .Where(q =>
            q.ContainsKey("ESTADO") &&
            q["ESTADO"].ToString().Trim('"').Equals("PROCESADO", StringComparison.InvariantCultureIgnoreCase) &&
            q.ContainsKey("FECHA_APLICACION"))
          .Select(item =>
          {
            item["Reference"] = $"{item["LOTE"]}-{item["REFERENCIA"]}.{item["N�MERO"]}";
            item["Debit"] = item["MONTO"];
            item["Credit"] = 0.00;
            item["Cuenta"] = item["NO CUENTA"];
            item["loan_request_id"] = item["ID"];
            item["Date"] = DateTime.ParseExact(item["FECHA_APLICACION"].ToString(), "dd/MM/yyyy h:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture).AddHours(+6);
            //item["user_id"] = loans
            //  .FirstOrDefault(q => item["loan_request_id"].ToString().ToUpper() == q["loan_request_id"].ToString().ToUpper())
            //  ?["user_id"].ToString();
            var entity = new BankLedger().CopyFrom(item);
            return entity;
          })
          .ToList()
          .ForEach(item =>
          {
            item.user_id = FindUserId(item, context);
            AddOrUpdate(item, context).Wait();
          });
        break;

      case "BankTransaction.Created":
        context?.Logger.LogLine("\n" + lambdaEvent.ToString(Newtonsoft.Json.Formatting.Indented));
        foreach (var item in Parse_KinesisEvents(lambdaEvent))
        {
          context?.Logger.LogLine(">>>> [DBG] RECORD: ");
          context?.Logger.LogLine(item.ToString(Newtonsoft.Json.Formatting.Indented));

          var itemData = item["data"] ?? item;
          if (itemData.ContainsKey("payment")) itemData["BankUrl"] = itemData["payment"]["image"]["remote_url"];
          if (itemData.ContainsKey("categories"))
          {
            itemData["interest"] = itemData["categories"]["interest"];
            itemData["principal"] = itemData["categories"]["principal"];
          }
          if (itemData.ContainsKey("date"))
            itemData["date"] = DateTime.ParseExact(itemData["date"].ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture).AddHours(+6);
          else
            itemData["date"] = DateTime.Parse(itemData["created_at"].ToString());

          var entity = new BankLedger().CopyFrom(itemData);
          AddOrUpdate(entity, context).Wait();
        }
        break;

      case "BankTransaction.Created2":
        context?.Logger.LogLine("\n" + lambdaEvent.ToString(Newtonsoft.Json.Formatting.Indented));
        foreach (var item in Parse_KinesisEvents(lambdaEvent))
        {
          context?.Logger.LogLine(">>>> [DBG] RECORD: ");
          context?.Logger.LogLine(item.ToString(Newtonsoft.Json.Formatting.Indented));

          var itemData = item["data"] ?? item;
          if (itemData.ContainsKey("payment")) itemData["BankUrl"] = itemData["payment"]["image"]["remote_url"];
          if (itemData.ContainsKey("categories"))
          {
            itemData["interest"] = itemData["categories"]["interest"];
            itemData["principal"] = itemData["categories"]["principal"];
          }
          if (itemData.ContainsKey("date"))
            itemData["date"] = DateTime.ParseExact(itemData["date"].ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture).AddHours(+6);
          else
            itemData["date"] = DateTime.Parse(itemData["created_at"].ToString());

          var entity = new BankLedger().CopyFrom(itemData);
          AddOrUpdate(entity, context).Wait();
        }
        break;





      default:
        break;
    }



    //var s3Event = evnt.Records?[0].S3;
    //if(s3Event == null)
    //{
    //    return null;
    //}

    //try
    //{
    //    var response = await this.S3Client.GetObjectMetadataAsync(s3Event.Bucket.Name, s3Event.Object.Key);
    //    return response.Headers.ContentType;
    //}
    //catch(Exception e)
    //{
    //    context.Logger.LogLine($"Error getting object {s3Event.Object.Key} from bucket {s3Event.Bucket.Name}. Make sure they exist and your bucket is in the same region as this function.");
    //    context.Logger.LogLine(e.Message);
    //    context.Logger.LogLine(e.StackTrace);
    //    throw;
    //}

  }

  public bool EntityComparer(BankLedger newEntity, BankLedger q)
  {
    if (newEntity.bank_transaction_id != null && q.bank_transaction_id == newEntity.bank_transaction_id) return true;
    if (newEntity.payment_id != null && q.payment_id == newEntity.payment_id) return true;
    if (q.bank_transaction_id == null && q.payment_id == null && q.BankRef == newEntity.BankRef && q.loan_request_id.Equals(newEntity.loan_request_id, StringComparison.InvariantCultureIgnoreCase)) return true;
    return false;
  }

  public async Task AddOrUpdate(BankLedger newEntity, ILambdaContext context)
  {

    //throw Exception("update db.BankLedger.Add(newEntity).Entity to only replace modified fields");

    using (var db = new StreamsDbContext(context))
    {
      context?.Logger.LogLine($">>>> [DBG] Function.AddOrUpdate.StreamsDbContext()");

      // check if entity already exists
      var oldEntity = db.BankLedger.SingleOrDefault(q => EntityComparer(newEntity, q));

      context?.Logger.LogLine($">>>> [DBG] Function.AddOrUpdate.StreamsDbContext().FirstOrDefault() = " + oldEntity?.TrxId);

      // if not exists, create it 
      if (oldEntity == null)
      {
        oldEntity = db.BankLedger.Add(newEntity).Entity;
        //db.SaveChangesAsync().Wait();
        Console.WriteLine($"BankLedger-{newEntity.TrxId}\tDBT {newEntity.Debit}\tCRD {newEntity.Credit}");
        context?.Logger.LogLine($">>>> [DBG] BankLedger-{newEntity.TrxId},\tDEBIT: {newEntity.Debit},\tCREDIT: {newEntity.Credit}");
      }

      // if exsits, just update it
      else
      {
        newEntity.TrxId = oldEntity.TrxId;
        db.Entry(oldEntity).CurrentValues.SetValues(newEntity);
        //db.SaveChangesAsync().Wait();
        Console.WriteLine($"BankLedger-{newEntity.TrxId}\tDBT {newEntity.Debit}\tCRD {newEntity.Credit} [Update]");
        context?.Logger.LogLine($">>>> [DBG] BankLedger-{newEntity.TrxId},\tDEBIT: {newEntity.Debit},\tCREDIT: {newEntity.Credit},\t [Update]");
      }

      await db.SaveChangesAsync();
    }

  }

  public bool Comparer_LoanRequest(Loan q, BankLedger newEntity)
  {
    return q.Firebase_Id.Equals(newEntity.loan_request_id);
  }

  public string FindUserId(BankLedger newEntity, ILambdaContext context)
  {
    using (var db = new StreamsDbContext())
    {
      return db.Loans.FirstOrDefault(q => Comparer_LoanRequest(q, newEntity))?.Firebase_UserId;
    }
  }




  #region JObject to KinesisEvent, S3Event
  public static KinesisEvent To_KinesisEvent(JObject lambdaEvent)
  {
    var stream = lambdaEvent.ToString().ToStream();
    var output = new Amazon.Lambda.Serialization.Json.JsonSerializer().Deserialize<KinesisEvent>(stream);
    stream.Dispose();
    return output;
  }

  public static S3Event To_S3Event(JObject lambdaEvent)
  {
    return lambdaEvent.ToObject<S3Event>();
  } 
  #endregion
  
  #region Kinesis to List
  public static IEnumerable<JObject> Parse_KinesisEvents(JObject lambdaEvent)
  {
    var eventData = To_KinesisEvent(lambdaEvent);
    foreach (var item in eventData.Records)
    {
      var content = GetKinesisData(item.Kinesis);
      yield return JObject.Parse(content);
    }
  }

  public static string GetKinesisData(KinesisEvent.Record streamRecord)
  {
    using (var reader = new StreamReader(streamRecord.Data, Encoding.UTF8))
    {
      return reader.ReadToEnd();
    }
  }
  #endregion

  #region S3 to list

  public static IEnumerable<JObject> Parse_S3Events(JObject lambdaEvent)
  {
    var eventData = To_S3Event(lambdaEvent);
    foreach (var item in eventData.Records)
      yield return new JObject(new { key = item.S3.Object.Key.ToString() });
  }

  //public static IEnumerable<List<System.Dynamic.ExpandoObject>> Parse_TransfersBancoindustrial(JObject lambdaEvent)
  //{
  //  var eventData = To_S3Event(lambdaEvent);
  //  foreach (var item in eventData.Records)
  //  {
  //    var content = item.S3.Download(Function.S3Client).ToText(Encoding.GetEncoding("ISO-8859-1"));
  //    var rows = content.ParseCSV(delimeter: "|");
  //    yield return rows;
  //  }
  //}

  //public static IEnumerable<List<System.Dynamic.ExpandoObject>> Parse_ChequesBanrural(JObject lambdaEvent)
  //{
  //  var eventData = To_S3Event(lambdaEvent);
  //  foreach (var item in eventData.Records)
  //  {
  //    var content = item.S3.Download(Function.S3Client).ToText(Encoding.UTF8);
  //    var rows = content.ParseCSV(delimeter: ",");
  //    yield return rows;
  //  }
  //}

  //public static IEnumerable<List<System.Dynamic.ExpandoObject>> Parse_AchBancoindustrial(JObject lambdaEvent)
  //{
  //  var eventData = To_S3Event(lambdaEvent);
  //  foreach (var item in eventData.Records)
  //  {
  //    var content = item.S3.Download(Function.S3Client).ToText(Encoding.GetEncoding("ISO-8859-1"));
  //    var rows = content.ParseCSV(delimeter: "|");
  //    yield return rows;
  //  }
  //} 
  #endregion

  #region GetEventType

  public static List<JObject> Parse_Events(JObject lambdaEvent)
  {
    if (Is_KinesisEvent(lambdaEvent)) return Parse_KinesisEvents(lambdaEvent).ToList();
    else if (Is_S3Event(lambdaEvent)) return Parse_S3Events(lambdaEvent).ToList();

    return new List<JObject>();
  }

  public static string GetEventType(JObject lambdaEvent)
  {
    var events = new string[] {
      "ach-bancoindustrial",
      "cheques-bancoindustrial",
      "cheques-banrural",
      "transfers-bancoindustrial",
      "Payment.StatusUpdated",
      "Payment.ImageUploaded",
      "Payment.Created",
      "BankTransaction.Created",
      "Scheduled"
    };

    if (Is_KinesisEvent(lambdaEvent))
      return Check_KinesisType(lambdaEvent, events);

    else if (Is_S3Event(lambdaEvent))
      return Check_S3Type(lambdaEvent, events);

    else if (Is_ScheduledEvent(lambdaEvent))
      return "Scheduled";

    return null;
  }

  private static string Check_KinesisType(JObject lambdaEvent, string[] types)
  {
    var source = lambdaEvent["Records"][0]["kinesis"]["data"].ToString();
    byte[] data = Convert.FromBase64String(source);
    string value = Encoding.UTF8.GetString(data);
    return types.FirstOrDefault(q => value.Contains(q));
  }
  private static string Check_S3Type(JObject lambdaEvent, string[] types)
  {
    var value = lambdaEvent["Records"][0]["s3"]["object"]["key"].ToString();
    return types.FirstOrDefault(q => value.StartsWith(q));
  } 

  public static bool Is_S3Event(JObject lambdaEvent)
  {
    if (!lambdaEvent.ContainsKey("Records") || !lambdaEvent["Records"][0]["eventSource"].ToString().Contains(":s3")) return false;
    return true;
  }

  public static bool Is_KinesisEvent(JObject lambdaEvent)
  {
    if (!lambdaEvent.ContainsKey("Records") || !lambdaEvent["Records"][0]["eventSource"].ToString().Contains(":kinesis")) return false;
    return true;
  }

  public static bool Is_ScheduledEvent(JObject lambdaEvent)
  {
    if (!lambdaEvent.ContainsKey("source") || !lambdaEvent["source"].ToString().Replace(".", ":").Contains(":events")) return false;
    return true;
  }
  #endregion

}

public static class Extension
{

  public static void AddOrUpdate(this List<BankLedger> collection, ILambdaContext context)
  {

    using (var db = new StreamsDbContext(context))
    {
      context?.Logger.LogLine($">>>> [DBG] Function.AddOrUpdate.StreamsDbContext()");

      var dbSnapshot = db.BankLedger.ToList();

      foreach (var newEntity in collection)
      {

        // check if entity already exists
        var oldEntity = dbSnapshot//db.BankLedger
          .SingleOrDefault(q => {
            if (newEntity.bank_transaction_id != null && q.bank_transaction_id == newEntity.bank_transaction_id) return true;
            if (newEntity.payment_id != null && q.payment_id == newEntity.payment_id) return true;
            if (q.bank_transaction_id == null && q.payment_id == null && q.BankRef == newEntity.BankRef && q.loan_request_id.Equals(newEntity.loan_request_id, StringComparison.InvariantCultureIgnoreCase)) return true;
            return false;
          });

        context?.Logger.LogLine($">>>> [DBG] Function.AddOrUpdate.StreamsDbContext().FirstOrDefault() = " + oldEntity?.TrxId);

        // if not exists, create it 
        if (oldEntity == null)
        {
          oldEntity = db.BankLedger.Add(newEntity).Entity;
          //db.SaveChangesAsync().Wait();
          Console.WriteLine($"BankLedger-{newEntity.TrxId}\tDBT {newEntity.Debit}\tCRD {newEntity.Credit}");
          context?.Logger.LogLine($">>>> [DBG] BankLedger-{newEntity.TrxId},\tDEBIT: {newEntity.Debit},\tCREDIT: {newEntity.Credit}");
        }

        // if exsits, just update it
        else
        {
          newEntity.TrxId = oldEntity.TrxId;
          var updatedEntity = oldEntity.CopyFrom(newEntity);
          db.Entry(oldEntity).CurrentValues.SetValues(updatedEntity);
          //db.Entry(oldEntity).CurrentValues.SetValues(newEntity);
          //db.SaveChangesAsync().Wait();
          Console.WriteLine($"BankLedger-{newEntity.TrxId}\tDBT {newEntity.Debit}\tCRD {newEntity.Credit} [Update]");
          context?.Logger.LogLine($">>>> [DBG] BankLedger-{newEntity.TrxId},\tDEBIT: {newEntity.Debit},\tCREDIT: {newEntity.Credit},\t [Update]");
        }
      }

      db.SaveChangesAsync().Wait();
    }
  }


  public static List<T> ToPopulatedList<T>(this IEnumerable<dynamic> source) where T : class, new()
  {
    return source.ToPopulatedEnumberable<T>().ToList<T>();
  }

  public static IEnumerable<T> ToPopulatedEnumberable<T>(this IEnumerable<object> source) where T : class, new()
  {
    foreach (var item in source)
    {
      var target = item.CopyTo<T>(new T());
      yield return target;
    }
  }

  public static T CopyFrom<T>(this T target, object source) where T : class
  {
    if (source is null) return target as T;

    //var target = default(T);
    string json;
    if (source is Document) json = (source as Document).ToJson();
    else json = Newtonsoft.Json.JsonConvert.SerializeObject(source);

    Newtonsoft.Json.JsonConvert.PopulateObject(json, target, SerializerSettings);

    return target as T;
  }

  public static T CopyTo<T>(this object source, object target) where T : class
  {
    if (source is null) return target as T;

    string json;
    if (source is Document) json = (source as Document).ToJson();
    else json = Newtonsoft.Json.JsonConvert.SerializeObject(source);

    Newtonsoft.Json.JsonConvert.PopulateObject(json, target, SerializerSettings);
    return target as T;
  }

  private static readonly JsonSerializerSettings SerializerSettings = new JsonSerializerSettings
  {
    // MissingMemberHandling = MissingMemberHandling.Ignore,
    NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
    DateFormatHandling = Newtonsoft.Json.DateFormatHandling.IsoDateFormat,
    ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
  };


  public static Stream ToStream(this string s)
  {
    var stream = new MemoryStream();
    var writer = new StreamWriter(stream);
    writer.Write(s);
    writer.Flush();
    stream.Position = 0;
    return stream;
  }

  public static List<ExpandoObject> ParseCSV(this string response, IDictionary<string, object> prepend = null, string delimeter = "|")
  {
    var output = new List<ExpandoObject>();

    const string lineBreak = "\r\n";

    // determine which and how many headers we actually have
    var headers = response.Substring(0, response.IndexOf(lineBreak)).Split(new string[] { delimeter }, StringSplitOptions.None);
    var current = response.IndexOf(lineBreak) + lineBreak.Length;
    var next = current;

    // prepend additional headers
    if (prepend != null)
      headers = prepend.Keys.Concat(headers).ToArray();

    var row = new System.Dynamic.ExpandoObject() as IDictionary<string, Object>;
    int i = 0;

    while (current < response.Length)
    {
      // add new row
      if (i == headers.Length)
      {
        output.Add((ExpandoObject)row);
        row = new System.Dynamic.ExpandoObject() as IDictionary<string, Object>;
        i = 0;
      }

      // prepend additional properties
      if (i == 0 && prepend != null)
      {
        foreach (var item in prepend) row.Add(item.Key, item.Value);
        i += prepend.Count;
      }

      // determine column edges and return pointers
      var index = response.IndexOf(delimeter, current);
      next = index > -1 ? index : response.Length;

      // check for line break
      if (i == headers.Length - 1)
      {
        var indexLineBreak = response.IndexOf(lineBreak, current);
        if (indexLineBreak > -1 && indexLineBreak < next)
          next = indexLineBreak;
      }

      // add value to row
      var dataLength = next - current;
      var data = response.Substring(current, dataLength).Replace("\n", string.Empty).Replace("\r", string.Empty).Trim('"');
      row.Add(headers[i], data);

      // update pointers
      current = next + delimeter.Length;
      i++;
    }

    if (row != null && row.Count == headers.Length)
      output.Add((ExpandoObject)row);

    return output;
  }

  public static string ToText(this Stream stream, System.Text.Encoding encoding = null)
  {
    try
    {
      using (var reader = new StreamReader(stream, encoding ?? Encoding.UTF8))
      {
        return reader.ReadToEnd();
      }
    }
    finally
    {
      stream.Dispose();
    }
  }

  public static Stream Download(this S3EventNotification.S3Entity source, IAmazonS3 client)
  {
    var response = client.GetObjectAsync(source.Bucket.Name, source.Object.Key).Result;
    return response.ResponseStream;
  }

  public static bool ContainsKey(this JToken source, string key)
  {
    return source.Children<JProperty>().Any(q => q.Path.Equals(key));
  }

  public static bool ContainsKey(this JObject source, string key)
  {
    return source.Children<JProperty>().Any(q => q.Path.Equals(key));
  }

}