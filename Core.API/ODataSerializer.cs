﻿using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Formatter.Serialization;
using Microsoft.OData.Edm;
using System;

namespace Core.API
{
  /// <summary>
  /// OData Entity Serilizer that omits null properties from the response
  /// </summary>
  public class IngoreNullEntityPropertiesSerializer : ODataResourceSerializer
  {
    public IngoreNullEntityPropertiesSerializer(ODataSerializerProvider provider)
        : base(provider) { }

    /// <summary>
    /// Only return properties that are not null
    /// </summary>
    /// <param name="structuralProperty">The EDM structural property being written.</param>
    /// <param name="resourceContext">The context for the entity instance being written.</param>
    /// <returns>The property be written by the serilizer, a null response will effectively skip this property.</returns>
    public override Microsoft.OData.ODataProperty CreateStructuralProperty(IEdmStructuralProperty structuralProperty, ResourceContext resourceContext)
    {
      var property = base.CreateStructuralProperty(structuralProperty, resourceContext);
      return property.Value != null ? property : null;
    }
  }

  /// <summary>
  /// Provider that selects the IngoreNullEntityPropertiesSerializer that omits null properties on resources from the response
  /// </summary>
  public class IngoreNullEntityPropertiesSerializerProvider : DefaultODataSerializerProvider
  {
    private readonly IngoreNullEntityPropertiesSerializer _entityTypeSerializer;

    public IngoreNullEntityPropertiesSerializerProvider(IServiceProvider rootContainer)
        : base(rootContainer)
    {
      _entityTypeSerializer = new IngoreNullEntityPropertiesSerializer(this);
    }

    public override ODataEdmTypeSerializer GetEdmTypeSerializer(Microsoft.OData.Edm.IEdmTypeReference edmType)
    {
      // Support for Entity types AND Complex types
      if (edmType.Definition.TypeKind == EdmTypeKind.Entity || edmType.Definition.TypeKind == EdmTypeKind.Complex)
        return _entityTypeSerializer;
      else
        return base.GetEdmTypeSerializer(edmType);
    }
  }
}
