﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNet.OData;
using Core.Common.Models.Normalized.V1;
using Core.Data;

namespace Core.API.Controllers
{

  public class LoansController : ODataController
  {
    private readonly StreamsDbContext _dbContext;
    private IHttpContextAccessor _httpContext;

    public LoansController(StreamsDbContext context, IHttpContextAccessor accessor)
    {
      _dbContext = context;
      _httpContext = accessor;
    }

    // GET: odata/Loans
    [EnableQuery]
    public IQueryable<Loan> GetLoans()
    {
      Logger.LogClientIP(Request, _httpContext);

      return _dbContext.Loans.AsQueryable();
    }

    //public FileResult GetCSV()
    //{
    //  var list = _context.Loans.OrderByDescending(q => q.dueDateTime);
    //  MemoryStream ms = new MemoryStream();
    //  StreamWriter sw = new StreamWriter(ms);
    //  var csv = new CsvWriter(sw);
    //  csv.WriteRecords(list);
    //  ms.Seek(0, SeekOrigin.Begin);
    //  return File(ms, "application/octet-stream", $"{nameof(Loan)}s.csv");
    //}

  }
}