﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Data;
using Microsoft.AspNetCore.Mvc;

namespace Core.API.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class StatusController : ControllerBase
  {
    // GET api/values
    [HttpGet]
    public ActionResult<object> Get()
    {
      string database_server = "db-lms-dev.cpcvcsz0gdaj.us-east-1.rds.amazonaws.com";
      bool database_isConnected = TryConnectDb();

      return new[] {

        new {
          database = new {
            server = database_server,
            isConnected = database_isConnected
          },
          sql = new {
            enabled = true,
            endpoint = "/sql/entities?q=[query]",
            cache = "3600"
          },
          paging = new {
            enabled = true,
            endpoint = new[] {"/odata/bankledger" },
            cache = "3600"
          },
          odata = new {
            enabled = true,
            endpoint = new[] {"/odata/users", "/odata/loans"},
            entities = "/odata/$metadata",
            commands = "$filter, $count, $expand, $orderby, $top, $skip, $select",
            pageSize = "100"
          },
          elasticSearch = new {
            enabled = false,
            endpoint = "/search?q=[query]",
            denormalizedEntities = "Users,Loans"
          }
        }
        
      };
    }

    private bool TryConnectDb()
    {
      try
      {
        new StreamsDbContext().Dispose();
        return true;
      }
      catch
      {
        return false;
      }
    }

  }
}
