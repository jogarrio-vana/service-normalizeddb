﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.Common;
using System.Data;
using Microsoft.Extensions.Caching.Memory;
using System.Dynamic;
using Core.Data;
using Core.API.Controllers;
using Microsoft.AspNet.OData;
using Core.Common.Models.Normalized.V1;

namespace Core.RestAPI.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class BankLedgerController : ControllerBase
  {
    private readonly StreamsDbContext _dbContext;
    private IMemoryCache _cache;
    private IHttpContextAccessor _httpContext;

    public BankLedgerController(StreamsDbContext context, IMemoryCache memoryCache, IHttpContextAccessor accessor)
    {
      _httpContext = accessor;
      _cache = memoryCache;
      _dbContext = context;
    }

    // GET: odata/Deposits
    [EnableQuery]
    [ResponseCache(VaryByQueryKeys = new string[] { "page" }, Duration = 3600)] // cache query result
    public async Task<ActionResult<BankLedger>> GetDeposits(int? page)
    {
      if (page == null)
        return Redirect("/api/bankledger?page=1");

      int pageSize = 50;

      Logger.LogClientIP(Request, _httpContext);
      var content = await PaginatedList<BankLedger>.CreateAsync(_dbContext.BankLedger.AsNoTracking().AsQueryable(), page ?? 1, pageSize);
      return Ok(new {
        page = page,
        totalPages = content.TotalPages,
        data = content
      });
    }


  }

  public class PaginatedList<T> : List<T>
  {
    public int PageIndex { get; private set; }
    public int TotalPages { get; private set; }

    public PaginatedList(List<T> items, int count, int pageIndex, int pageSize)
    {
      PageIndex = pageIndex;
      TotalPages = (int)Math.Ceiling(count / (double)pageSize);

      this.AddRange(items);
    }

    public bool HasPreviousPage
    {
      get
      {
        return (PageIndex > 1);
      }
    }

    public bool HasNextPage
    {
      get
      {
        return (PageIndex < TotalPages);
      }
    }

    public static async Task<PaginatedList<T>> CreateAsync(IQueryable<T> source, int pageIndex, int pageSize)
    {
      var count = await source.CountAsync();
      var items = await source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
      return new PaginatedList<T>(items, count, pageIndex, pageSize);
    }
  }
}