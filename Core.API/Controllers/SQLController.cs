﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.Common;
using System.Data;
using Microsoft.Extensions.Caching.Memory;
using System.Dynamic;
using Core.Data;
using Core.API.Controllers;

namespace Core.RestAPI.Controllers
{
  [Route("sql/[controller]")]
  [ApiController]
  public class EntitiesController : ControllerBase
  {
    private readonly StreamsDbContext _dbContext;
    private IMemoryCache _cache;
    private IHttpContextAccessor _httpContext;

    public EntitiesController(StreamsDbContext context, IMemoryCache memoryCache, IHttpContextAccessor accessor)
    {
      _httpContext = accessor;
      _cache = memoryCache;
      _dbContext = context;
    }

    // GET: api/SQL
    [HttpGet]
    [ResponseCache(VaryByQueryKeys = new string[] { "q" }, Duration = 3600)] // cache query result
    public IEnumerable<object> Get(string q)
    {
      Logger.LogClientIP(Request, _httpContext);

      // create and open connection to database
      using (var command = _dbContext.Database.GetDbConnection().CreateCommand())
      {
        _dbContext.Database.OpenConnection();

        // send query to database
        command.CommandText = q;
        using (var result = command.ExecuteReaderAsync().Result)
        {

          // map column names
          var size = result.FieldCount;
          var cols = new string[size];
          for (int i = 0; i < size; i++)
            cols[i] = result.GetName(i);

          // transform sql dataset result to json properties
          while (result.ReadAsync().Result)
          {
            var item = new ExpandoObject() as IDictionary<string, Object>;
            for (int i = 0; i < size; i++)
              item.Add(cols[i], result.GetValue(i));
            yield return item;
          }

        }
      }

    }
  }
}