﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNet.OData;
using Core.Common.Models.Normalized.V1;
using Core.Data;

namespace Core.API.Controllers
{

  public class TransactionsController : ODataController
  {
    private readonly StreamsDbContext _dbContext;
    private IHttpContextAccessor _httpContext;

    public TransactionsController(StreamsDbContext context, IHttpContextAccessor accessor)
    {
      _dbContext = context;
      _httpContext = accessor;
    }

    // GET: odata/Users
    [EnableQuery]
    public IQueryable<Transaction> GetTransactions()
    {
      Logger.LogClientIP(Request, _httpContext);
      return _dbContext.Transactions.AsQueryable();
    }
  }
}