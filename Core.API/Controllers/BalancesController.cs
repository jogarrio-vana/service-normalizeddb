﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNet.OData;
using Core.Common.Models.Normalized.V1;
using Core.Data;

namespace Core.API.Controllers
{

  public class BalancesController : ODataController
  {
    private readonly StreamsDbContext _dbContext;
    private IHttpContextAccessor _httpContext;

    public BalancesController(StreamsDbContext context, IHttpContextAccessor accessor)
    {
      _dbContext = context;
      _httpContext = accessor;
    }

    // GET: odata/Users
    [EnableQuery]
    public IQueryable<Balance> GetBalances()
    {
      Logger.LogClientIP(Request, _httpContext);
      return _dbContext.Users.AsQueryable();
    }
  }
}