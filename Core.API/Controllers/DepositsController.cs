﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNet.OData;
using Core.Common.Models.Normalized.V1;
using Core.Data;

namespace Core.API.Controllers
{

  public class DepositsController : ODataController
  {
    private readonly StreamsDbContext _dbContext;
    private IHttpContextAccessor _httpContext;

    public DepositsController(StreamsDbContext context, IHttpContextAccessor accessor)
    {
      _dbContext = context;
      _httpContext = accessor;
    }

    // GET: odata/Deposits
    [EnableQuery]
    public IQueryable<Deposit> GetDeposits()
    {
      Logger.LogClientIP(Request, _httpContext);
      return _dbContext.Deposits.AsQueryable();
    }
  }
}