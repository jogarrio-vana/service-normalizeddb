﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.API.Controllers
{
  public static class Logger
  {
    public static void LogClientIP(HttpRequest Request,  IHttpContextAccessor _httpContext)
    {
      var ip = _httpContext.HttpContext?.Connection?.RemoteIpAddress?.ToString();
      var query = $"{Request.Path}{Request.QueryString}";

      System.Diagnostics.Debug.WriteLine($"[{ip}] \"{query}\"");
    }
  }
}
