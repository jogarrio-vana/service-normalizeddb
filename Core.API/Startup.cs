﻿using System.Collections.Generic;
using System.Linq;
using AspNetCore.RouteAnalyzer;
using Core.Data;
using Core.Common.Models.Normalized.V1;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNet.OData.Formatter.Serialization;
using Microsoft.AspNet.OData.Routing.Conventions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OData.Edm;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Core.Common.Services;

namespace Core.API
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
      AwsClient = new AwsAuthhentiction();
    }

    public IConfiguration Configuration { get; }
    public AwsAuthhentiction AwsClient { get; internal set; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

      services.AddResponseCaching();
      services.AddRouteAnalyzer();
      services.AddOData();

      services.AddMvc()
        .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
        .AddJsonOptions(opt =>
        {
          opt.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
          opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
          opt.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
        });
      
      //var connectionString = AwsClient.GetSecretAsync<string>(secretName: Configuration["AppSettings:AWSRDS:Secret"], region: Configuration["AppSettings:AWSRegion"]).Result;
      //services.AddDbContext<StreamsDbContext>(options => options.UseSqlServer(connectionString));
      services.AddDbContext<StreamsDbContext>();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      
      app.UseResponseCaching();
      app.UseMvc(builder =>
      {
        builder.Select().Expand().Filter().OrderBy().MaxTop(100).Count();
        builder.EnableDependencyInjection();
        builder.MapODataServiceRoute("odata", "odata", (dependencies) => 
        {
          dependencies
            .AddService(Microsoft.OData.ServiceLifetime.Singleton, typeof(IEdmModel), sp => GetEdmModel())
            .AddService(Microsoft.OData.ServiceLifetime.Singleton, typeof(ODataSerializerProvider), sp => new IngoreNullEntityPropertiesSerializerProvider(sp))
            .AddService(Microsoft.OData.ServiceLifetime.Singleton, typeof(IEnumerable<IODataRoutingConvention>), sp => ODataRoutingConventions.CreateDefaultWithAttributeRouting("odata", builder));
        });
      });
    }

    private static IEdmModel GetEdmModel()
    {
      var builder = new ODataConventionModelBuilder();

      builder.EntitySet<User>("Users")
        .EntityType
        .Filter() // Allow for the $filter Command
        .Count() // Allow for the $count Command
        .Expand() // Allow for the $expand Command
        .OrderBy() // Allow for the $orderby Command
        .Page(null,100) // Allow for the $top and $skip Commands
        .Select();// Allow for the $select Command; 

      builder.EntitySet<Loan>("Loans")
        .EntityType
        .Filter() // Allow for the $filter Command
        .Count() // Allow for the $count Command
        .Expand() // Allow for the $expand Command
        .OrderBy() // Allow for the $orderby Command
        .Page(null, 100) // Allow for the $top and $skip Commands
        .Select();// Allow for the $select Command; 

      //builder.EntitySet<Balance>("Balances")
      //  .EntityType
      //  .Filter() // Allow for the $filter Command
      //  .Count() // Allow for the $count Command
      //  .Expand() // Allow for the $expand Command
      //  .OrderBy() // Allow for the $orderby Command
      //  .Page(null, 100) // Allow for the $top and $skip Commands
      //  .Select();// Allow for the $select Command; 

      //builder.EntitySet<Transaction>("Transactions")
      //  .EntityType
      //  .Filter() // Allow for the $filter Command
      //  .Count() // Allow for the $count Command
      //  .Expand() // Allow for the $expand Command
      //  .OrderBy() // Allow for the $orderby Command
      //  .Page(null, 100) // Allow for the $top and $skip Commands
      //  .Select();// Allow for the $select Command; 

      //builder.EntitySet<Deposit>("Deposits")
      //  .EntityType
      //  .Filter() // Allow for the $filter Command
      //  .Count() // Allow for the $count Command
      //  .Expand() // Allow for the $expand Command
      //  .OrderBy() // Allow for the $orderby Command
      //  .Page(null, 100) // Allow for the $top and $skip Commands
      //  .Select();// Allow for the $select Command; 


      builder.EntitySet<BankLedger>("BankLedgers")
        .EntityType
        .Filter() // Allow for the $filter Command
        .Count() // Allow for the $count Command
        .Expand() // Allow for the $expand Command
        .OrderBy() // Allow for the $orderby Command
        .Page(null, 100) // Allow for the $top and $skip Commands
        .Select();// Allow for the $select Command; 

      return builder.GetEdmModel();
    }

  }
}
