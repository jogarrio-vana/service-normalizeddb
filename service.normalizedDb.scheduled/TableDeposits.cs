﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Newtonsoft.Json;
using CsvHelper;
using Amazon.Lambda.Core;
using Core.Common.Models.Normalized.V1;
using Core.Data;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using Amazon.S3;
using Amazon.S3.Model;
using System.Dynamic;
using Core.Processing;

namespace service.normalizedDb.scheduled
{
  public static class TableDeposits
  {
    public static AmazonDynamoDBClient dynamoDb = new AmazonDynamoDBClient(RegionEndpoint.USEast1);
    public static AmazonS3Client s3Bucket = new AmazonS3Client(RegionEndpoint.USEast1);
    public static Firebase.Database firebase = null;

    public static IConfiguration configuration = null;

    /// <summary>
    /// Initialize class and load configuration plus services to shared global memory.
    /// </summary>
    /// <param name="context"></param>
    public static void Initialize(ILambdaContext context = null)
    {
      if (TableDeposits.configuration != null) return;

      var alias = context?.InvokedFunctionArn?.Substring(context.InvokedFunctionArn.LastIndexOf(":") + 1);
      var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

      TableDeposits.configuration = new ConfigurationBuilder()
       .SetBasePath(Directory.GetCurrentDirectory())
       .AddJsonFile("appsettings.dev.json", optional: false, reloadOnChange: false)
       .AddJsonFile($"appsettings.{alias}.json", optional: true, reloadOnChange: false)
       .AddJsonFile($"appsettings.{environment}.json", optional: true, reloadOnChange: false)
       .Build();


      TableDeposits.firebase = new Firebase.Database(context, configuration);

      System.Diagnostics.Trace.TraceWarning("Pending relocate Airtable credentials.");
    }

    public static void Main(string[] args)
    {
      Execute(null);
    }

    public static void Execute(ILambdaContext context)
    {
      //Initialize(context);

      //const string loan_request_hackishIndex = "-LWS5ALdBKghCXTtfRit";
      //const string bank_transaction_hackishIndex = "00036b4e-bcfd-4d5d-a0c0-969ef15a6338";
      //const string loan_ledger_hackishIndex = "-LV0diWz1EkyXQIcFLKI";
      //const string payment_hackishIndex = "0141a698-59f4-4dc1-8e39-9f2f3d5625d3";

      //var count = dynamoDb
      //  .GetTable("loan_request")
      //  .ToEnumerable(
      //      new ScanFilter()
      //      .AddFilter("loan_request_id", ScanOperator.GreaterThan, loan_request_hackishIndex))
      //  .ToList();

      //Console.WriteLine("BatchProcess_Firebase_Users");
      //Function.BatchProcess_Firebase_Users(context);

      //Console.WriteLine("BatchProcess_Firebase_Loans");
      //Function.BatchProcess_Firebase_Loans(context);

      //Batch_DYNAMO_BankTransaction(context);
      //Batch_CSV_AchBancoIndustrial(context);
      //Batch_DYNAMO_Payments(context);

      new BatchProcess(context).PatchLoans(context);

      Console.WriteLine("Done.");
      Console.WriteLine("Press any key to exit...");
      Console.ReadLine();
    }

    public static void Batch_CSV_AchBancoIndustrial(ILambdaContext context)
    {
      Console.WriteLine("Reading s3://vana-bank-transactions/ach-bancoindustrial/*");
      context?.Logger.LogLine("Reading s3://vana-bank-transactions/ach-bancoindustrial/*");

      var datasource = s3Bucket
        .ToEnumerable("vana-bank-transactions/ach-bancoindustrial")
        .Where(q => q.Size > 0)
        .OrderBy(q => q.LastModified);

      var loans = dynamoDb
        .GetTable("loan")
        .ToList()
        .ToJsonList();

      //var users = firebase
      //  .Get("/Users", context)
      //  .Result
      //  .ToIdJSON()
      //  .ToList();

      List<User> users;
      using (var db = new StreamsDbContext())
        users = db.Users.ToList();

      foreach (var item in datasource)
      CSV_AchBancoIndustrial(item, loans, context, users);
    }

    public static void CSV_AchBancoIndustrial(S3Object dataObject, List<JObject> loans, ILambdaContext context, List<User> users)
    {
      //Console.WriteLine($"Reading s3://{dataObject.BucketName}/{dataObject.Key}");
      context?.Logger.LogLine($"Reading s3://{dataObject.BucketName}/{dataObject.Key}");

      var datasource = dataObject
        .Download()
        .ToText()
        .ParseCSV<IDictionary<string, Object>>(delimeter: "|")
        .ToJsonList();

      if (loans == null)
      {
        loans = dynamoDb
           .GetTable("loan")
           .ToList()
           .ToJsonList();
      }

      datasource
        .Where(q =>
          q.ContainsKey("ESTADO") &&
          q["ESTADO"].ToString().Trim('"').Equals("PROCESADO", StringComparison.InvariantCultureIgnoreCase) &&
          q.ContainsKey("FECHA_APLICACION"))
        .Select(item =>
        {
          item["Reference"] = $"{item["LOTE"]}-{item["REFERENCIA"]}.{item["NÚMERO"]}";
          item["Debit"] = item["MONTO"];
          item["Credit"] = 0.00;
          item["Cuenta"] = item["NO CUENTA"];
          item["loan_request_id"] = item["ID"];
          item["Date"] = DateTime.ParseExact(item["FECHA_APLICACION"].ToString(), "dd/MM/yyyy h:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture).AddHours(+6);

          var matching_users = loans.Where(q =>
            (q["loan_request_id"].ToString().StartsWith(item["loan_request_id"].ToString()))
          );


          if (matching_users.Count() == 1 && matching_users.First()["loan_request_id"].ToString() == item["loan_request_id"].ToString())
          {
            item["user_id"] = matching_users.First()["user_id"].ToString();
            //Console.Write(".");//
            return new BankLedger().CopyFrom(item);
          }

          if (matching_users.Count() > 1)
          {
            Console.WriteLine($"\n[!] Reading s3://{dataObject.BucketName}/{dataObject.Key} >>>> "+ matching_users.Count());
            return new BankLedger().CopyFrom(item);
          }

          if (matching_users.Count() == 1)
          {
            var item2 = matching_users.First();
            Console.WriteLine($"\n[#] Reading s3://{dataObject.BucketName}/{dataObject.Key} >>>> " + matching_users.Count() + ", partial-match on loan_request_id");
            Console.WriteLine($"{item2["loan_request_id"]}");
            Console.WriteLine($"{item["loan_request_id"]} \t {item["LOTE"]} \t {item["NOMBRE"]} \t {item["NO CUENTA"]}");
            return new BankLedger().CopyFrom(item);
          }

          if (matching_users.Count() == 0)
          {
            var match2 = users.FirstOrDefault(q => q.bankAccount___number == item["NO CUENTA"].ToString());
            if (match2 != null)
            {
              Console.WriteLine($"\n[#] Reading s3://{dataObject.BucketName}/{dataObject.Key} >>>> " + matching_users.Count() + ", match on bank_Account");
              Console.WriteLine($"{item["loan_request_id"]} \t {item["LOTE"]} \t {item["NOMBRE"]} \t {item["NO CUENTA"]}");
              return new BankLedger().CopyFrom(item);
            }
          }

          if (matching_users.Count() == 0)
          {
            Console.WriteLine($"\n[!!!] Reading s3://{dataObject.BucketName}/{dataObject.Key} >>>> " + matching_users.Count() + ", WTFFFF");
            Console.WriteLine($"{item["loan_request_id"]} \t {item["LOTE"]} \t {item["NOMBRE"]} \t {item["NO CUENTA"]}");
            return new BankLedger().CopyFrom(item);
          }



          return new BankLedger().CopyFrom(item);
        })
        .ToList();
        //.AddOrUpdate(context);
    }



    public static void Batch_DYNAMO_BankTransaction(ILambdaContext context)
    {
      Console.WriteLine("Reading DynamoDb: " + @"bank_transaction");
      var bank_transactions = dynamoDb
        .GetTable("bank_transaction")
        .ToList()
        .ToJsonList();

      bank_transactions
        .Select(item => {
          var itemData = item["data"] ?? item;
          if (itemData.ContainsKey("payment"))
            itemData["BankUrl"] = itemData["payment"]["image"]["remote_url"];
          if (itemData.ContainsKey("categories"))
          {
            itemData["interest"] = itemData["categories"]["interest"];
            itemData["principal"] = itemData["categories"]["principal"];
          }
          if (itemData.ContainsKey("date"))
            itemData["date"] = DateTime.ParseExact(itemData["date"].ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture).AddHours(+6);
          else
            itemData["date"] = DateTime.Parse(itemData["created_at"].ToString());

          var entity = new BankLedger().CopyFrom(itemData);
          return entity;
        })
        .ToList()
        .AddOrUpdate(context);
    }

    public static void Batch_DYNAMO_Payments(ILambdaContext context)
    {
      Console.WriteLine("Reading DynamoDb: " + @"payment");
      var datasource = dynamoDb
        .GetTable("payment")
        .ToList()
        .ToJsonList();

      datasource
        .Where(item => {
          var itemData = item["data"] ?? item;
          var match = itemData["status"].ToString() != "rejected";
          return match;
        })
        .Select(item => {
          var itemData = item["data"] ?? item;
          if (itemData["payment_id"].ToString() == "-LU3jzqDNtmqZikx7GST") System.Diagnostics.Debugger.Break();

          if (itemData.ContainsKey("image"))
          {
            var itemImage = itemData["image"];
            var key = new[] { "remote_url", "external_url" }.FirstOrDefault(q => (itemImage as JObject).ContainsKey(q));
            if (key != null) itemData["BankUrl"] = itemImage[key];
          }

          if (itemData.ContainsKey("date"))
            itemData["date"] = DateTime.ParseExact(itemData["date"].ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture).AddHours(+6);
          else
            itemData["date"] = DateTime.Parse(itemData["created_at"].ToString());
          
          var entity = new BankLedger().CopyFrom(itemData);
          return entity;
        })
        .ToList()
        .AddOrUpdate(context);
    }
    
    public static void Batch1(ILambdaContext context) {

      // LOAD DATA
      Console.WriteLine("Reading CSV: " + @"./ach_transactions.csv");
      context?.Logger.LogLine("Reading CSV: " + @"./ach_transactions.csv");
      var ach_transactions = new StreamReader(
        @"./ach_transactions.csv",
          Encoding.GetEncoding("ISO-8859-1"))
        .ReadCSV()
        .ToCSVList();

      Console.WriteLine("Reading CSV: " + @"./invoices.csv");
      context?.Logger.LogLine("Reading CSV: " + @"./invoices.csv");
      var invoices = new StreamReader(
        @"./invoices.csv")
        .ReadCSV()
        .ToCSVList();

      Console.WriteLine("Reading DynamoDb: " + @"bank_transaction");
      context?.Logger.LogLine("Reading DynamoDb: " + @"bank_transaction");


      var bank_transactions = dynamoDb
        .GetTable("bank_transaction")
        .ToList()
        .ToJsonList();

      //using (var writer = new StreamWriter(@"C:\Users\Javier\OneDrive\Desktop\vanadb\bank_transactions.csv"))
      //using (var csv = new CsvWriter(writer))
      //{
      //  var list = bank_transactions.Select(q => JObject.Parse(q.ToJson()).ToObject<System.Dynamic.ExpandoObject> ()).ToList();
      //  foreach (var item in list)
      //  {
      //    var item2 = (IDictionary<string, object>)item;
      //    if (string.IsNullOrEmpty(item2["payment_id"].ToString())) System.Diagnostics.Debugger.Break();
      //    csv.WriteRecord(item);
      //    csv.NextRecord();
      //  }
      //}

      Console.WriteLine("Reading DynamoDb: " + @"loan");
      context?.Logger.LogLine("Reading DynamoDb: " + @"loan");
      var loans = dynamoDb
        .GetTable("loan")
        .ToList();

      Console.WriteLine("Reading DynamoDb: " + @"loan_ledger");
      context?.Logger.LogLine("Reading DynamoDb: " + @"loan_ledger");
      var loan_ledgers = dynamoDb
        .GetTable("loan_ledger")
        .ToList();

      Console.WriteLine("Reading DynamoDb: " + @"payment");
      context?.Logger.LogLine("Reading DynamoDb: " + @"payment");
      var payments = dynamoDb
        .GetTable("payment")
        .ToList();


      // MAPPINGS & CALCULATED FIELDS
      Console.WriteLine("[LOG] MAPPINGS & CALCULATED FIELDS...");
      context?.Logger.LogLine("[LOG] MAPPINGS & CALCULATED FIELDS...");

      loan_ledgers
        .ForEach(item => {
          item.Remove("credit");

          if (!item.ContainsKey("bank_transaction_id"))
            item["bank_transaction_id"] = null;
        });


      loans
        .ForEach(item => {
          item.Remove("date");
        });

      ach_transactions
        .ForEach(e => {
          e["Reference"] = $"{e["DOC"]}-{e["NUMERO"]}.{e["OPERACION"]}";
          e["Debit"] = e["MONTO"];
          e["Credit"] = 0.00;
          e["Cuenta"] = e["NO CUENTA"];
          e["loan_request_id"] = e["ID"];

          e["date"] = DateTime.ParseExact(e["FECHA_DURACIÓN"].ToString(), "dd/MM/yyyy H:mm", System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
        });

      payments
        .ForEach(item => {

          if (item.ContainsKey("image"))
          {
            var image = item["image"].AsDocument();

            var key = new[] { "remote_url", "external_url" }.FirstOrDefault(q => image.ContainsKey(q));
            if (key != null) item["BankUrl"] = image[key].AsString();
          }

          if (item.ContainsKey("bank"))
            item["Banco"] = item["bank"];
        });

      invoices
        .ForEach(item => {
          item["InvoiceUrl"] = string.Format("https://www.ingface.net/Ingfacereport/dtefactura.jsp?cae={0}", item["CAE"]);
        });

      bank_transactions
        .ForEach(item => {
          item["data"]["BankUrl"] = item["data"]["payment"]["image"]["remote_url"];
          item["data"]["interest"] = item["data"]["categories"]["interest"];
          item["data"]["principal"] = item["data"]["categories"]["principal"];
          item["data"]["date"] = DateTime.ParseExact(item["data"]["date"].ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);//, System.Globalization.DateTimeStyles.AdjustToUniversal);
        });


      // JOINS
      Console.WriteLine("[LOG] JOINS...");
      context?.Logger.LogLine("[LOG] JOINS...");

      var bank_operations = bank_transactions

        .ToPopulatedEnumberable<BankLedger>()               // TRANSFORM: 'bank_transactions' to class 'BankLedger'

        .Join(payments,                                     // JOIN: 'payments'
          left => left.payment_id,                          // the source table of the inner join
          right => right["payment_id"].ToString(),          // Select the primary key (the first part of the "on" clause in an sql "join" statement)
          (left, right) => left.CopyFrom(right))            // On match, selection

        .GroupJoin(invoices,                                // JOIN: 'invoices'
          left => left.payment_id,
          right => right["PERSONALIZADO_03"].ToString(),
          (left, right) => left.CopyFrom(right.FirstOrDefault()))

        .ToList()                                           // From iterator to list

        .Append(ach_transactions                            // APPEND: 'ach_transactions' where 'ESTADO=PROCESADO'
            .Where(q => q["ESTADO"].Equals("PROCESADO"))
            .ToPopulatedList<BankLedger>()

        .GroupJoin(loans,                                   // JOIN: 'loans' to get missing 'user_id' in 'ach_transactions'
          left => left.loan_request_id.ToString().ToUpper(),        // HOTFIX: added '.ToUpper()' to fix 'ach_transactions' that incorrecty UpperCased 'loan_request_id'
          right => right["loan_request_id"].ToString().ToUpper(),   // HOTFIX: added '.ToUpper()' to fix 'ach_transactions' that incorrecty UpperCased 'loan_request_id'
          (left, right) => left.CopyFrom(right.SingleOrDefault()))
          );


      // CORRECTIONS
      Console.WriteLine("[LOG] CORRECTIONS...");
      context?.Logger.LogLine("[LOG] CORRECTIONS...");

      bank_operations                                       // for missing 'Date' in 'bank_transactions', pull from event's 'created_at' (AKA. 'helper_1')
        .ForEach(item => {

          if (string.IsNullOrEmpty(item.source_date))
            item.Date = DateTime.Parse(item.source_created_at); //.ToString("dd/MM/yyyy");


          var formats = new[] { "dd/MM/yyyy H:mm", "yyyy-MM-dd", "dd/MM/yyyy" };
          if (!DateTime.TryParseExact(item.source_date, formats, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime helper_2))
            throw new InvalidDataException(item.source_date);
          item.helper_2 = helper_2;
          item.source_date_string = item.helper_2.ToString("dd/MM/yyyy");

          // item.helper_3 = item.Date + item.BankRef + item.user_id;
        });

      var output = bank_operations
        .OrderBy(q => q.helper_2);


      using (var db = new StreamsDbContext())
      {
        foreach (var newEntity in output)
        {

          // check first exists: loan, user
          var oldEntity = db.BankLedger
            .FirstOrDefault(q =>
              q.BankRef.Equals(newEntity.BankRef) &&
              q.Date.Equals(newEntity.Date) &&
              q.loan_request_id.Equals(newEntity.loan_request_id, StringComparison.InvariantCultureIgnoreCase));

          // if it doesnt exist in db, create it 
          if (oldEntity == null)
          {
            oldEntity = db.BankLedger.Add(newEntity).Entity;
            //db.SaveChangesAsync().Wait();
            Console.WriteLine($"BankLedger-{newEntity.TrxId}\tDBT {newEntity.Debit}\tCRD {newEntity.Credit}");
            context?.Logger.LogLine($"BankLedger-{newEntity.TrxId}\tDBT {newEntity.Debit}\tCRD {newEntity.Credit}");
          }
          // if it exsits in db, just update it
          else
          {
            newEntity.TrxId = oldEntity.TrxId;
            db.Entry(oldEntity).CurrentValues.SetValues(newEntity);
            //db.SaveChangesAsync().Wait();
            Console.WriteLine($"BankLedger-{newEntity.TrxId}\tDBT {newEntity.Debit}\tCRD {newEntity.Credit} [Update]");
            context?.Logger.LogLine($"BankLedger-{newEntity.TrxId}\tDBT {newEntity.Debit}\tCRD {newEntity.Credit} [Update]");
          }

        }

        db.SaveChangesAsync().Wait();
      }

      //var filepath = @"C:\Users\Javier\OneDrive\Desktop\vanadb\final.csv";
      //File.Delete(filepath);
      //using (var writer = File.AppendText(filepath))
      //using (var csv = new CsvWriter(writer))
      //{
      //  csv.Configuration.ShouldQuote = (field, ctx) => ctx.HasHeaderBeenWritten;
      //  csv.WriteRecords(output);
      //  writer.Flush();
      //}
    }

  }

  public static class Extensions
  {

    public static void AddOrUpdate(this List<BankLedger> collection, ILambdaContext context)
    {

      using (var db = new StreamsDbContext(context))
      {
        context?.Logger.LogLine($">>>> [DBG] Function.AddOrUpdate.StreamsDbContext()");

        var dbSnapshot = db.BankLedger.ToList();

        foreach (var newEntity in collection)
        {

          // check if entity already exists
          var oldEntity = dbSnapshot//db.BankLedger
            .SingleOrDefault(q => {
              if (newEntity.bank_transaction_id != null && q.bank_transaction_id == newEntity.bank_transaction_id) return true;
              if (newEntity.payment_id != null && q.payment_id == newEntity.payment_id) return true;
              if (q.bank_transaction_id == null && q.payment_id == null && q.BankRef == newEntity.BankRef && q.loan_request_id.Equals(newEntity.loan_request_id, StringComparison.InvariantCultureIgnoreCase)) return true;
              return false;
            });

          context?.Logger.LogLine($">>>> [DBG] Function.AddOrUpdate.StreamsDbContext().FirstOrDefault() = " + oldEntity?.TrxId);

          // if not exists, create it 
          if (oldEntity == null)
          {
            oldEntity = db.BankLedger.Add(newEntity).Entity;
            //db.SaveChangesAsync().Wait();
            Console.WriteLine($"BankLedger-{newEntity.TrxId}\tDBT {newEntity.Debit}\tCRD {newEntity.Credit}");
            context?.Logger.LogLine($">>>> [DBG] BankLedger-{newEntity.TrxId},\tDEBIT: {newEntity.Debit},\tCREDIT: {newEntity.Credit}");
          }

          // if exsits, just update it
          else
          {
            newEntity.TrxId = oldEntity.TrxId;
            var updatedEntity = oldEntity.CopyFrom(newEntity);
            db.Entry(oldEntity).CurrentValues.SetValues(updatedEntity);
            //db.Entry(oldEntity).CurrentValues.SetValues(newEntity);
            //db.SaveChangesAsync().Wait();
            Console.WriteLine($"BankLedger-{newEntity.TrxId}\tDBT {newEntity.Debit}\tCRD {newEntity.Credit} [Update]");
            context?.Logger.LogLine($">>>> [DBG] BankLedger-{newEntity.TrxId},\tDEBIT: {newEntity.Debit},\tCREDIT: {newEntity.Credit},\t [Update]");
          }
        }

        db.SaveChangesAsync().Wait();
      }
    }


    public static List<T> Append<T>(this List<T> target, IEnumerable<T> collection)
    {
      target.AddRange(collection);
      return target;
    }

    public static CsvReader ReadCSV(this StreamReader reader)
    {
      var csvReaderConfig = new CsvHelper.Configuration.Configuration()
      {
        HasHeaderRecord = true,
        HeaderValidated = null,
        MissingFieldFound = null
      };
      return new CsvReader(reader, csvReaderConfig);
    }

    public static List<IDictionary<string, object>> ToCSVList(this CsvReader csv)
    {
      var output = new List<IDictionary<string, object>>();

      csv.Read();
      csv.ReadHeader();

      while (csv.Read())
      {
        var record = csv.GetRecord<dynamic>();
        output.Add(record);
      }

      csv.Context.Dispose();
      csv.Dispose();

      return output;
    }

    public static Table GetTable(this AmazonDynamoDBClient client, string tableName)
    {
      return Table.LoadTable(client, tableName);
    }

    public static ScanFilter AddFilter(this ScanFilter filter, string attributeName, ScanOperator op, params DynamoDBEntry[] values)
    {
      filter.AddCondition(attributeName, op, values);
      return filter;
    }

    public static IEnumerable<Document> ToEnumerable(this Table table, ScanFilter filter = null)
    {
      filter = filter == null ? new ScanFilter() : filter;
      Search search = table.Scan(filter);
      do
      {
        var documentList = search.GetNextSetAsync().Result;

        foreach (var document in documentList)
          yield return document;

      } while (!search.IsDone);
    }

    public static List<Document> ToList(this Table table, ScanFilter filter = null)
    {
      return table.ToEnumerable().ToList();
    }

    public static List<JObject> ToJsonList(this List<Document> source)
    {
      var output = new List<JObject>();
      foreach (var item in source)
        output.Add(JObject.Parse(item.ToJson()));
      return output;
    }


    public static List<JObject> ToJsonList(this List<IDictionary<string, object>> source)
    {
      var output = new List<JObject>();
      foreach (var item in source)
        output.Add(JObject.FromObject(item));
      return output;
    }

    //public static IEnumerable<T> ToPopulatedEnumberable<T>(this IEnumerable<Document> source) where T : class, new()
    //{
    //  foreach (var item in source)
    //  {
    //    var target = item.CopyTo<T>(new T());
    //    yield return target;
    //  }
    //}

    public static IEnumerable<T> ToPopulatedEnumberable<T>(this IEnumerable<object> source) where T : class, new()
    {
      foreach (var item in source)
      {
        var target = item.CopyTo<T>(new T());
        yield return target;
      }
    }

    public static T CopyTo<T>(this object source, object target) where T : class
    {
      if (source is null) return target as T;

      string json;
      if (source is Document) json = (source as Document).ToJson();
      else json = Newtonsoft.Json.JsonConvert.SerializeObject(source);

      Newtonsoft.Json.JsonConvert.PopulateObject(json, target, SerializerSettings);
      return target as T;
    }

    public static T CopyFrom<T>(this T target, dynamic source) where T : class
    {
      if (source is null) return target as T;

      //var target = default(T);
      string json;
      if (source is Document) json = source.ToJson();
      else json = Newtonsoft.Json.JsonConvert.SerializeObject(source);

      Newtonsoft.Json.JsonConvert.PopulateObject(json, target, SerializerSettings);

      return target as T;
    }

    public static List<T> ToPopulatedList<T>(this IEnumerable<Document> source) where T : class, new()
    {
      return source.ToPopulatedEnumberable<T>().ToList<T>();
    }

    public static List<T> ToPopulatedList<T>(this IEnumerable<dynamic> source) where T : class, new()
    {
      return source.ToPopulatedEnumberable<T>().ToList<T>();
    }

    private static readonly JsonSerializerSettings SerializerSettings = new JsonSerializerSettings
    {
      // MissingMemberHandling = MissingMemberHandling.Ignore,
      NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
      DateFormatHandling = Newtonsoft.Json.DateFormatHandling.IsoDateFormat,
      ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
    };

    public static bool ContainsKey(this JToken source, string key)
    {
      return source.Children<JProperty>().Any(q => q.Path.Equals(key));
    }


    public static IEnumerable<S3Object> ToEnumerable(this AmazonS3Client client, string URI)
    {
      string _bucketName = URI.Substring(0, URI.IndexOf('/'));
      string _prefix = URI.Substring(URI.IndexOf('/', 5) + 1);

      ListObjectsRequest request = new ListObjectsRequest();
      ListObjectsResponse response = null;

      request.BucketName = _bucketName;
      request.Prefix = _prefix;

      int count = 1;
      do
      {
        response = client.ListObjectsAsync(request).Result;

        foreach (var item in response.S3Objects)
        {
          //System.Diagnostics.Debug.WriteLine($"{count++ + ".",-7} {item.Key}");
          yield return item;
        }

        request.Marker = response.NextMarker;
      } while (response.IsTruncated);
    }

    public static Stream Download(this S3Object item)
    {
      using (var client = new AmazonS3Client(new AmazonS3Config() { RegionEndpoint = RegionEndpoint.USEast1 }))
      {
        var request = new GetObjectRequest()
        {
          BucketName = item.BucketName,
          Key = item.Key
        };

        GetObjectResponse response = client.GetObjectAsync(request).Result;
        return response.ResponseStream;
      }
    }

    public static StreamReader ToReader(this Stream item, Encoding encoding)
    {
      return new StreamReader(item, encoding);

    }

    public static string ToText(this Stream stream, System.Text.Encoding encoding = null)
    {
      try
      {
        using (var reader = new StreamReader(stream, encoding ?? Encoding.UTF8))
        {
          return reader.ReadToEnd();
        }
      }
      finally
      {
        stream.Dispose();
      }
    }

    public static List<T> ParseCSV<T>(this string response, IDictionary<string, object> prepend = null, string delimeter = "|") where T : class
    {
      var output = new List<ExpandoObject>();

      const string lineBreak = "\r\n";

      // determine which and how many headers we actually have
      var headers = response.Substring(0, response.IndexOf(lineBreak)).Split(new string[] { delimeter }, StringSplitOptions.None);
      var current = response.IndexOf(lineBreak) + lineBreak.Length;
      var next = current;

      // prepend additional headers
      if (prepend != null)
        headers = prepend.Keys.Concat(headers).ToArray();

      var row = new System.Dynamic.ExpandoObject() as IDictionary<string, Object>;
      int i = 0;

      while (current < response.Length)
      {
        // add new row
        if (i == headers.Length)
        {
          output.Add((ExpandoObject)row);
          row = new System.Dynamic.ExpandoObject() as IDictionary<string, Object>;
          i = 0;
        }

        // prepend additional properties
        if (i == 0 && prepend != null)
        {
          foreach (var item in prepend) row.Add(item.Key, item.Value);
          i += prepend.Count;
        }

        // determine column edges and return pointers
        var index = response.IndexOf(delimeter, current);
        next = index > -1 ? index : response.Length;

        // check for line break
        if (i == headers.Length - 1)
        {
          var indexLineBreak = response.IndexOf(lineBreak, current);
          if (indexLineBreak > -1 && indexLineBreak < next)
            next = indexLineBreak;
        }

        // add value to row
        var dataLength = next - current;
        var data = response.Substring(current, dataLength).Replace("\n", string.Empty).Replace("\r", string.Empty).Trim('"');
        row.Add(headers[i], data);

        // update pointers
        current = next + delimeter.Length;
        i++;
      }

      if (row != null && row.Count == headers.Length)
        output.Add((ExpandoObject)row);

      return output.Cast<T>().ToList<T>();
    }


    public static IEnumerable<object> ToIdJSON(this Stream stream)
    {
      try
      {
        using (StreamReader reader = new StreamReader(stream))
        using (JsonReader json = new JsonTextReader(reader))
        {
          var serializer = new JsonSerializer();
          var _key = string.Empty; // keep track of item id
          json.Read(); // skip initial object token

          while (json.ReadAsync().Result)
          {
            if (json.TokenType == JsonToken.PropertyName)
            {
              _key = json.Path;
            }
            else if (json.TokenType == JsonToken.StartObject)
            {
              var _value = serializer.Deserialize<dynamic>(json);
              _value.id = _key;
              yield return _value;
            }
          }
        }

      }
      finally
      {
        stream.Dispose();
      }
    }



  }


}
