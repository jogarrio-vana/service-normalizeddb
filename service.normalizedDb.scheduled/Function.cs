using System;
using System.IO;
using System.Text;

using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Amazon.Lambda.KinesisEvents;
using Amazon.Lambda.S3Events;
using Amazon.S3;
using Amazon.S3.Util;
using Core.Processing;
using Newtonsoft.Json.Linq;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

public class Function
{

  public void Handler(JObject lambdaEvent, ILambdaContext context)
  {


    try
    {
      new BatchProcess(context).PatchLoans(context);

      BatchProcess_Firebase_Users(context);
      BatchProcess_Firebase_Loans(context);
      //service.normalizedDb.scheduled.TableDeposits.Execute(context);
      //BatchProcess_Airtable_Deposits(context);
    }
    catch (AggregateException ex)
    {
      context?.Logger.LogLine(ex.Flatten().InnerException.ToString());
      throw ex;
    }

  }
  

  public static void BatchProcess_Firebase_Users(ILambdaContext context)
  {
    /// MONITORING
    context?.Logger.LogLine($"[{context.AwsRequestId}] {nameof(BatchProcess_Firebase_Users)}");
    var stopWatch = System.Diagnostics.Stopwatch.StartNew();
    stopWatch.Start();

    const int workerCount = 12;

    var processing = new Core.Processing.BatchProcess(context);
    var queue = new BlockingCollection<dynamic>();

    Action<dynamic> work =
      (item) =>
      {
          //CONSUMER
          processing.Consumer_Users(item, context);
      };

    Task.Run(() =>
    {
        //PRODUCER
        processing.Producer_Users(queue, context);
        queue.CompleteAdding();
    });

    queue
      .GetConsumingEnumerable()
      .AsParallel()
      .WithDegreeOfParallelism(workerCount)
      .WithMergeOptions(ParallelMergeOptions.NotBuffered)
      .ForAll(work);

    /// MONITORING
    stopWatch.Start();
    System.Diagnostics.Trace.TraceInformation($"[{context.AwsRequestId}] {stopWatch.Elapsed.Milliseconds:n0}ms");

    queue.Dispose();
  }

  public static void BatchProcess_Firebase_Loans(ILambdaContext context)
  {
    /// MONITORING
    context?.Logger.LogLine($"[{context.AwsRequestId}] {nameof(BatchProcess_Firebase_Loans)}");
    var stopWatch = System.Diagnostics.Stopwatch.StartNew();
    stopWatch.Start();

    const int workerCount = 12;

    var processing = new Core.Processing.BatchProcess();
    var queue = new BlockingCollection<dynamic>();

    Action<dynamic> work =
      (item) =>
      {
          //CONSUMER
          processing.Consumer_Loans(item, context);
      };

    Task.Run(() =>
    {
        //PRODUCER
        processing.Producer_Loans(queue, context);
        queue.CompleteAdding();
    });

    queue
      .GetConsumingEnumerable()
      .AsParallel()
      .WithDegreeOfParallelism(workerCount)
      .WithMergeOptions(ParallelMergeOptions.NotBuffered)
      .ForAll(work);

    /// MONITORING
    stopWatch.Start();
    System.Diagnostics.Trace.TraceInformation($"[{context?.AwsRequestId}] {stopWatch.Elapsed.Milliseconds:n0}ms");

    queue.Dispose();
  }

  public void BatchProcess_Airtable_Deposits(ILambdaContext context)
  {
    /// MONITORING
    context?.Logger.LogLine($"[{context.AwsRequestId}] {nameof(BatchProcess_Airtable_Deposits)}");
    var stopWatch = System.Diagnostics.Stopwatch.StartNew();
    stopWatch.Start();

    const int workerCount = 12;

    var processing = new Core.Processing.BatchProcess();
    var queue = new BlockingCollection<dynamic>();

    Action<dynamic> work =
      (item) =>
      {
        //CONSUMER
        processing.Consumer_Deposits(item, context);
      };

    Task.Run(() =>
      {
        //PRODUCER
        processing.Producer_Deposits(queue);
        queue.CompleteAdding();
      });

    queue
      .GetConsumingEnumerable()
      .AsParallel()
      .WithDegreeOfParallelism(workerCount)
      .WithMergeOptions(ParallelMergeOptions.NotBuffered)
      .ForAll(work);

    /// MONITORING
    stopWatch.Start();
    System.Diagnostics.Trace.TraceInformation($"[{context.AwsRequestId}] {stopWatch.Elapsed.Milliseconds:n0}ms");

    queue.Dispose();
  }


}
