﻿using System;
using System.Collections.Generic;
using System.Text;
using Amazon.CDK;

namespace Core.StreamReplay
{
  class Program
  { 
    static void Main(string[] args)
    {
      var app = new App();

      // A CDK app can contain multiple stacks. You can view a list of all the stacks in your
      // app by typing `cdk list`.
      new HelloStack(app, "hello-cdk-1", new StackProps());
      new HelloStack(app, "hello-cdk-2", new StackProps());

      app.Run();
    }
  }
}
