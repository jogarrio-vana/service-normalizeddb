﻿--http://www.zentut.com/sql-tutorial/sql-aggregate-functions/
--https://msdn.microsoft.com/en-us/magazine/dn519921.aspx?f=255&MSPPError=-2147217396
--https://stackoverflow.com/questions/13593845/how-to-create-a-view-using-ef-code-first-poco

SELECT 
	loan_request_id,
	user_id,
	COUNT(loan_request_id) as 'test' 
FROM 
	dbo.BankLedger
GROUP BY 
	loan_request_id,
	user_id