﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Processing
{
  public static class ClassScaffolder
  {
    public static string Generate(string json)
    {
      var flat_properties = 
        JObject.Parse(json)
        .FlattenProperties<JProperty>(includeValues: true)
        .ToArray();

      var builder = new StringBuilder();
      builder.AppendLine("public class JsonObject");
      builder.AppendLine("{");

      foreach (var item in flat_properties)
      {
        builder.AppendLine($"   public string {item.Name} {{ get; set; }}");
      }

      builder.AppendLine("}");

      return builder.ToString();
    }

  }
}
