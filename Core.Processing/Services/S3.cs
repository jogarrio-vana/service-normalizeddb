﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Processing.Services
{
  public class S3_Producer
  {
    public AmazonS3Client client = null;

    public S3_Producer()
    {
      client = new AmazonS3Client(new AmazonS3Config() { RegionEndpoint = RegionEndpoint.USEast1 });
      //Amazon.AWSConfigs.RegionEndpoint = RegionEndpoint.USEast1;
    }

    public IEnumerable<object> List(string URI)
    {
      string _bucketName = URI.Substring(5, URI.IndexOf('/', 5) - 5);
      string _prefix = URI.Substring(URI.IndexOf('/', 5) + 1);

      ListObjectsRequest request = new ListObjectsRequest();
      ListObjectsResponse response = null;
      
      request.BucketName = _bucketName;
      request.Prefix = _prefix;

      int count = 1;
      do
      {
        response = client.ListObjectsAsync(request).Result;

        foreach (var item in response.S3Objects)
        {
          System.Diagnostics.Debug.WriteLine($"{count++ + ".", -7} {item.Key}");
          yield return item;
        }

        request.Marker = response.NextMarker;
      } while (response.IsTruncated);
    }


  }
}
