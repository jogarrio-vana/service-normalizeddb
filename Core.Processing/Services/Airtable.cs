﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AirtableApiClient;



namespace Core.Processing.Services
{
  public class Airtable
  {
    private static string baseId;
    private static string appKey;

    public Airtable(string baseId, string appKey)
    {
      Airtable.baseId = baseId;
      Airtable.appKey = appKey;
    }

    public IEnumerable<AirtableRecord> List(string TABLE_NAME)
    {

      string offset = null;
      string errorMessage = null;
      var records = new List<AirtableRecord>();

      using (AirtableBase airtableBase = new AirtableBase(appKey, baseId))
      {
        //
        // Use 'offset' and 'pageSize' to specify the records that you want
        // to retrieve.
        // Only use a 'do while' loop if you want to get multiple pages
        // of records.
        //

        do
        {
          Task<AirtableListRecordsResponse> task = airtableBase.ListRecords(
            TABLE_NAME,
            offset,
            fields: null,
            filterByFormula: null,
            maxRecords: null,
            pageSize: null,
            sort: null,
            view: null);

          AirtableListRecordsResponse response = task.Result;

          if (response.Success)
          {
            //records.AddRange(response.Records.ToList());
            foreach (var item in response.Records) yield return item;
            offset = response.Offset;
          }
          else if (response.AirtableApiError is AirtableApiException)
          {
            errorMessage = response.AirtableApiError.ErrorMessage;
            break;
          }
          else
          {
            errorMessage = "Unknown error";
            break;
          }
        } while (offset != null);
      }

      if (!string.IsNullOrEmpty(errorMessage))
      {
        // Error reporting
      }
      else
      {
        // Do something with the retrieved 'records' and the 'offset'
        // for the next page of the record list.
      }

    }

  }
}
