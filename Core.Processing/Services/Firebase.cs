﻿using Amazon.Lambda.Core;
using Core.Common.Services;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

public static class Firebase
{

  public class Database
  {
    private readonly HttpClient client = new HttpClient();
    private AwsAuthhentiction aws_client;
    private GcpAuthentication gcp_client;

    private int RetryCounter = 0;
    private bool firsttime = true;
    
    private static string Firebase_Endpoint;
    private static string SecretName;
    private static string Region;
    public static string auth_token = null;

    private IConfiguration configuration;

    public Database(ILambdaContext context, IConfiguration configuration)
    {
      this.Initialize(context, new AwsAuthhentiction(), new GcpAuthentication(), configuration);
    }

    private void Initialize(ILambdaContext context, AwsAuthhentiction aws, GcpAuthentication gcp, IConfiguration configuration)
    {
      if (!string.IsNullOrEmpty(Database.auth_token)) return;

      Database.Firebase_Endpoint = configuration["AppSettings:Firebase:Endpoint"];
      Database.SecretName = configuration["AppSettings:Firebase:Secret"];
      Database.Region = configuration["AppSettings:AWSRegion"];

      this.aws_client = aws;
      this.gcp_client = gcp;

      context?.Logger.LogLine($"[{context.AwsRequestId}] Calling SecretManager");
      var secret_value = aws_client.GetSecretAsync<string>(secretName: SecretName, region: Region, context: context).Result;
      Database.auth_token = gcp_client.GetAuthTokenAsync(secret_value).Result;

      client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Database.auth_token);
    }

    public async Task<Stream> Get(string path, ILambdaContext context = null)
    {      
      var result = await client.GetStreamAsync($"https://{Firebase_Endpoint}/{path}.json");
      var memoryStream = new MemoryStream();
      await result.CopyToAsync(memoryStream);
      result.Dispose();
      memoryStream.Seek(0, SeekOrigin.Begin);
      return memoryStream;
    }

    public async Task refreshToken(ILambdaContext context)
    {
      context?.Logger.LogLine($"[{context.AwsRequestId}] Refreshing token");

      var secret_value = new AwsAuthhentiction().GetSecretAsync<String>(secretName: SecretName, region: Region, context: context).Result;
      Database.auth_token = new GcpAuthentication().GetAuthTokenAsync(secret_value).Result;
      client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Database.auth_token);
      firsttime = false;
    }


    public async Task<string> Patch(ILambdaContext context, string path, object values)
    {
    retry_auth:
      if (firsttime && RetryCounter < 2)
      {
        await refreshToken(context);
      }

      var json = JsonConvert.SerializeObject(values);
      var payload = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

      var response = await client.PatchAsync(
        $"https://{Firebase_Endpoint}/{path}.json",
        payload
      );


      string result = await response.Content.ReadAsStringAsync();

      if (!response.IsSuccessStatusCode)
      {
        if ((int)response.StatusCode == 401)
        {
          if (RetryCounter == 0)
          {
            RetryCounter++;
            firsttime = true;
            goto retry_auth;
          }
        }
        throw new Exception(response.ReasonPhrase);
      }
      return result;
    }
  }

}
