﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Core.Processing
{
  public static class Loaders
  {
    private static readonly string delimeter = "___";
    private static readonly string delimeterPlaceholder = "|";
    private static readonly JsonSerializerSettings SerializerSettings = new JsonSerializerSettings
    {
      NullValueHandling = NullValueHandling.Ignore,
      DateFormatHandling = DateFormatHandling.IsoDateFormat
    };


    public static Stream DownloadObject(this object item)
    {
      var s3_entry = item as S3Object;

      using (var client = new AmazonS3Client(new AmazonS3Config() { RegionEndpoint = RegionEndpoint.USEast1 }))
      {
        var request = new GetObjectRequest()
        {
          BucketName = s3_entry.BucketName,
          Key = s3_entry.Key
        };

        GetObjectResponse response = client.GetObjectAsync(request).Result;
        return response.ResponseStream;
      }
    }


    #region PopulateObject
    /// <summary>
    /// Populates an object properties with by explicit property name match.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="target"></param>
    /// <returns></returns>
    public static object PopulateObject(this JObject source, object target)
    {
      return source.PopulateObject<object>(target);
    }

    public static T PopulateObject<T>(this JObject source) where T : class, new()
    {
      var target = new T();
      return source.PopulateObject<T>(target);
    }

    public static T PopulateObject<T>(this JObject source, object target) where T : class, new()
    {
      //var target = default(T);
      var json = JsonConvert.SerializeObject(source);
      JsonConvert.PopulateObject(json, target, SerializerSettings);
      return target as T;
    }

    #endregion
    
    #region WrapInside
    /// <summary>
    /// Creates a new parent object to wrap provided object's content.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="array"></param>
    /// <returns></returns>
    public static T WrapInside<T>(this Array array) where T : class
    {
      return (T)Activator.CreateInstance(typeof(T), array);
    }

    public static T WrapInside<T>(this object source) where T : class
    {
      return (T)Activator.CreateInstance(typeof(T), source);
    }

    public static JObject WrapInJObject(this object source)
    {
      return JObject.FromObject(source);
    }

    public static JObject WrapInJObject(this JProperty[] array)
    {
      return new JObject(array);
    }
    #endregion

    #region Hashify
    /// <summary>
    /// Normalizes objects property names to allow matching similar but not-exact property names.
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    private static string Hashify(string input)
    {
      return input
          .Replace(delimeter, delimeterPlaceholder)
          .Replace("_", string.Empty)
          .Replace(" ", string.Empty)
          .ToLower();
    }

    public static JProperty[] HashifyProperties(this JProperty[] array)
    {
      var output = new List<JProperty>();

      for (int i = 0; i < array.Length; i++)
      {
        var item = array[i];
        var name = Hashify(item.Name);
        output.Add(new JProperty(name, item.Value));
      }

      return output.ToArray();
    } 
    #endregion

    public static object InferObject(this JObject source, object target, IDictionary<string,string> explicitMappings = null, string[] removeParentNode = null)
    {
      var source_properties = source
        .FlattenProperties<JProperty>(includeValues: true)
        .ToArray()
        .HashifyProperties()
        .ToList();

      // removeParent
      for (int i = source_properties.Count - 1; i >= 0; i--)
      {
        var item = source_properties[i];
        var name = item.Name;
        if (removeParentNode != null)
        {
          var prefix = removeParentNode.FirstOrDefault(q => name.StartsWith(Hashify(q) + delimeterPlaceholder));
          if (prefix != null)
            name = name.Substring(prefix.Length + 1);

          source_properties.RemoveAt(i);
          source_properties.Add(new JProperty(name, item.Value));
        }
      }

      // explicitMappings
      if (explicitMappings != null)
      {
        foreach (var map in explicitMappings)
        {
          var name = Hashify(map.Key);
          var item = source_properties.FirstOrDefault(q => q.Name == name);
          if (item == null) continue;

          source_properties.Remove(item);
          source_properties.Add(new JProperty(Hashify(map.Value), item.Value));
        }
      }
            
      for (int i = source_properties.Count - 1; i >= 0; i--)
      {
        var item = source_properties[i];
        var name = item.Name;
        if (removeParentNode != null)
        {
          var prefix = removeParentNode.FirstOrDefault(q => name.StartsWith(q + delimeterPlaceholder));
          if (prefix != null)
            name = name.Substring(prefix.Length + 1);

          source_properties.RemoveAt(i);
          source_properties.Add(new JProperty(name, item.Value));
        }
      }

      var target_properties =
        target.GetType()
        .GetProperties()
        .Where(p => p.CanWrite)
        .ToDictionary(p => Hashify(p.Name), p => p);
      
      foreach (var item in source_properties)
      {
        if (!target_properties.ContainsKey(item.Name)) continue;
        var property = target_properties[item.Name];
        try
        {
          property.SetValue(target, item.Value.ToObject(property.PropertyType));
        }
        catch (Exception ex)
        {
          System.Diagnostics.Debug.WriteLine($"Loaders.InferredPopulateObject()\t Skipping {item.Name}");
          System.Diagnostics.Debug.WriteLine(ex.Message);
        }
      }

      return target;
    }

    public static JObject ScopeInto(this JObject item, string propertyName) {
      return item[propertyName] as JObject;
    }

    public static dynamic Value(this JObject item, string value)
    {

      return item.SelectToken(value);
      //try
      //{
      //  var segments = value.Split('.');
      //  if (segments.Length > 1)
      //  {
      //    string property = segments.First();
      //    string path = value.Substring(property.Length+1);
      //    return Value(item.GetType().GetProperty(property), path);
      //  }
      //  return item.GetType().GetProperty(value).GetValue(item, null);
      //}
      //catch
      //{
      //  return null;
      //}
    }

    public static JProperty[] AppendValues(this JProperty[] target, object source, IDictionary<string, string> mappings)
    {
      var source_jobject = source is JObject ? source as JObject : JObject.FromObject(source);
      var output = new List<JProperty>(target);
      foreach (var item in mappings)
      {
        var value = source_jobject[item.Value];
        var index = output.FindIndex(q => q.Name.Equals(item.Key));

        if (index == -1) output.Add(new JProperty(item.Key, value));
        else output[index].Value = value;
      }
      return output.ToArray();
    }

    public static JProperty[] RemoveProperties(this JProperty[] target, IList<string> mappings)
    {
      var output = new List<JProperty>();
      foreach (var item in target)
      {
        if (mappings.Any(q => item.Name.StartsWith(q)))
          continue;
        output.Add(item);
      }
      return output.ToArray();
    }

    public static IEnumerable<T> FlattenProperties<T>(this object input, bool includeValues = true) where T : class
    {
      var ignore = new[] { "javier" };// "Devices", "Tokens", "Loans", "LastLocations", "files", "Signatures" };
      var ignoreLength = 1024;

      var queue = new Queue<Tuple<JToken, string>>();
      queue.Enqueue(new Tuple<JToken, string>(input as JToken, string.Empty));

      while (queue.Count > 0)
      {
        var node = queue.Dequeue();
        string prefix = node.Item2;

        if (node.Item1 is JProperty)
        {
          var property = node.Item1 as JProperty;
          var values = property.Values();
          //prefix = string.Join(delimeter, prefix, property.Name);

          // validation rules:
          if (property.Name.Length > ignoreLength) continue;
          if (!ignore.Any(i => prefix.EndsWith(i))) prefix = string.Join(delimeter, prefix, property.Name);

          if (values.Count() == 1 && property.Value is JValue)
          {
            // validation rules:
            string _value = property.Value.ToString();
            if (_value.Length > ignoreLength) _value = _value.Substring(0, ignoreLength) + "[...]";

            if (typeof(T) == typeof(JProperty) && includeValues) yield return new JProperty(prefix.Substring(delimeter.Length), _value) as T;
            else if (includeValues) yield return (new { key = prefix.Substring(delimeter.Length), value = _value }) as T;
            else yield return prefix.Substring(delimeter.Length) as T;
            continue;
          }
        }

        foreach (var child in (node.Item1 as JToken).Children())
          queue.Enqueue(new Tuple<JToken, string>(child, prefix));
      }
    }

    public static void PrintProperties(this object input, string prefix = "")
    {
      if (input is JProperty)
      {
        var property = input as JProperty;
        var values = property.Values();
        if (values.Count() == 1 && property.Value is JValue)
        {
          Console.WriteLine($"{prefix}{property.Name}");// = {property.Value}");
          return;
        }
        prefix += property.Name + ".";
      }

      var node = input as JToken;
      foreach (var child in node.Children())
        PrintProperties(child, prefix);
    }

    public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)
    {
      foreach (T item in enumeration) action(item);
    }

    public static IEnumerable<TOut> Map<T, TOut>(this IEnumerable<T> enumeration, Func<T, TOut> action)
    {
      foreach (T item in enumeration) yield return action(item);
    }

    public static IEnumerable<object> ToIdJSON(this Stream stream)
    {
      try
      {
        using (StreamReader reader = new StreamReader(stream))
        using (JsonReader json = new JsonTextReader(reader))
        {
          var serializer = new JsonSerializer();
          var _key = string.Empty; // keep track of item id
          json.Read(); // skip initial object token

          while (json.ReadAsync().Result)
          {
            if (json.TokenType == JsonToken.PropertyName)
            {
              _key = json.Path;
            }
            else if (json.TokenType == JsonToken.StartObject)
            {
              var _value = serializer.Deserialize<dynamic>(json);
              _value.id = _key;
              yield return _value;
            }
          }
        }

      }
      finally
      {
        stream.Dispose();
      }
    }

    public static IEnumerable<object> ToKeyValueJSON(this Stream stream)
    {
      using (StreamReader reader = new StreamReader(stream))
      using (JsonReader json = new JsonTextReader(reader))
      {
        var serializer = new JsonSerializer();
        var _key = string.Empty; // keep track of item id
        json.Read(); // skip initial object token

        while (json.ReadAsync().Result)
        {
          if (json.TokenType == JsonToken.PropertyName)
          {
            _key = json.Path;
          }
          else if (json.TokenType == JsonToken.StartObject)
          {
            yield return new
            {
              key = _key,
              value = serializer.Deserialize<dynamic>(json)
            };
          }
        }
      }

      stream.Dispose();
    }
    
    public static IEnumerable<object> ToDynamic(this Stream stream)
    {
      try
      {
        using (var reader = new StreamReader(stream))
        using (var json = new JsonTextReader(reader))
        {
          return new JsonSerializer().Deserialize<dynamic[]>(json);
        }
      }
      finally
      {
        stream.Dispose();
      }
    }

    public static IEnumerable<object> ToJObject(this Stream stream)
    {
      try
      {
        using (var reader = new StreamReader(stream))
        using (var json = new JsonTextReader(reader))
        {
          return new JsonSerializer().Deserialize<dynamic[]>(json);
        }
      }
      finally
      {
        stream.Dispose();
      }
    }

    public static string ToText(this Stream stream)
    {
      try
      {
        using (var reader = new StreamReader(stream, Encoding.UTF8))
        {
          return reader.ReadToEnd();
        }
      }
      finally
      {
        stream.Dispose();
      }
    }

    public static Stream TryDecompress_GZip(this Stream input)
    {
      ushort GZIP_LEAD_BYTES = 0x8b1f;
      input.Seek(0, SeekOrigin.Begin);

      byte[] bytes = new byte[4];
      input.Read(bytes, 0, 4);
      input.Seek(0, SeekOrigin.Begin);

      if (BitConverter.ToUInt16(bytes, 0) != GZIP_LEAD_BYTES) return input;

      try
      {
        return Decompress_GZip(input);
      }
      finally
      {
        input.Dispose();
      }
    }

    public static Stream Decompress_GZip(this Stream input)
    {
      var decompressionStream = new System.IO.Compression.GZipStream(input, System.IO.Compression.CompressionMode.Decompress);


      var stream = new MemoryStream();
      decompressionStream.CopyTo(stream);
      stream.Position = 0;
      input.Dispose();
      return stream;
    }

  }

}
