using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using Amazon.Lambda.KinesisEvents;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Scheduled
{

  private ILambdaContext Context = null;

    public void Handler(KinesisEvent kinesisEvent, ILambdaContext context)
    {
    this.Context = context;
    //context.Logger.LogLine($"Beginning to process {kinesisEvent.Records.Count} records...");

    //foreach (var record in kinesisEvent.Records)
    //{
    //  var data = GetRecordContents(record.Kinesis);

    //  if (data["eventType"].ToString() == "LoanLedger.AddEntry") return;
    //  if (data["eventType"].ToString() == "LoanRequest.StatusUpdate") return;
    //  if (data["eventType"].ToString() == "Loan.StatusUpdate") return;

    //}

    //context.Logger.LogLine("Stream processing complete.");

    BatchProcess_Firebase_Users(context);
    BatchProcess_Firebase_Loans(context);
    BatchProcess_Airtable_Deposits();
  }



  public void BatchProcess_Firebase_Users(ILambdaContext context)
  {
    Context.Logger.LogLine(nameof(BatchProcess_Firebase_Users));

    const int workerCount = 40;

    var processing = new Core.Processing.BatchProcess();
    var queue = new BlockingCollection<dynamic>();

    Action<dynamic> work =
      (item) =>
      {
        //CONSUMER
        processing.Consumer_Users(item, this.Context);
      };

    Task.Run(() =>
    {
      //PRODUCER
      processing.Producer_Users(queue, context);
      queue.CompleteAdding();
    });

    queue
      .GetConsumingEnumerable()
      .AsParallel()
      .WithDegreeOfParallelism(workerCount)
      .WithMergeOptions(ParallelMergeOptions.NotBuffered)
      .ForAll(work);

    queue.Dispose();
  }

  public void BatchProcess_Firebase_Loans(ILambdaContext context)
  {
    Context.Logger.LogLine(nameof(BatchProcess_Firebase_Loans));

    const int workerCount = 40;

    var processing = new Core.Processing.BatchProcess();
    var queue = new BlockingCollection<dynamic>();

    Action<dynamic> work =
      (item) =>
      {
        //CONSUMER
        processing.Consumer_Loans(item, this.Context);
      };

    Task.Run(() =>
    {
      //PRODUCER
      processing.Producer_Loans(queue, context);
      queue.CompleteAdding();
    });

    queue
      .GetConsumingEnumerable()
      .AsParallel()
      .WithDegreeOfParallelism(workerCount)
      .WithMergeOptions(ParallelMergeOptions.NotBuffered)
      .ForAll(work);

    queue.Dispose();
  }

  public void BatchProcess_Airtable_Deposits()
  {
    Context.Logger.LogLine(nameof(BatchProcess_Airtable_Deposits));

    const int workerCount = 40;

    var processing = new Core.Processing.BatchProcess();
    var queue = new BlockingCollection<dynamic>();

    Action<dynamic> work =
      (item) =>
      {
        //CONSUMER
        processing.Consumer_Deposits(item, this.Context);
      };

    Task.Run(() =>
    {
      //PRODUCER
      processing.Producer_Deposits(queue);
      queue.CompleteAdding();
    });

    queue
      .GetConsumingEnumerable()
      .AsParallel()
      .WithDegreeOfParallelism(workerCount)
      .WithMergeOptions(ParallelMergeOptions.NotBuffered)
      .ForAll(work);

    queue.Dispose();
  }




}
