using System;
using System.IO;
using System.Text;

using Amazon.Lambda.Core;
using Amazon.Lambda.KinesisEvents;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


public class Ingress
{

    public void Handler(KinesisEvent kinesisEvent, ILambdaContext context)
    {
    //context.Logger.LogLine($"Beginning to process {kinesisEvent.Records.Count} records...");

    foreach (var record in kinesisEvent.Records)
    {
      var data = GetRecordContents(record.Kinesis);

      if (data["eventType"].ToString() == "LoanLedger.AddEntry") return;
      if (data["eventType"].ToString() == "LoanRequest.StatusUpdate") return;
      if (data["eventType"].ToString() == "Loan.StatusUpdate") return;

    }

    //context.Logger.LogLine("Stream processing complete.");
  }

    private JToken GetRecordContents(KinesisEvent.Record streamRecord)
    {
        using (var reader = new StreamReader(streamRecord.Data, Encoding.UTF8))
        {
            return JObject.Parse(reader.ReadToEnd());
        }
    }
}
