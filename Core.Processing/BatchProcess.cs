using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Configuration;
using Core.Data;
using Core.Processing.Services;
using System.Collections.Generic;
using Amazon.Lambda.Core;
using Core.Common.Models.Normalized.V1;
using CsvHelper;

namespace Core.Processing
{
  public class BatchProcess
  {
    public static Firebase.Database firebase = null;
    public static IConfiguration configuration = null;
    public static Airtable airtable = null;
    public static S3_Producer s3_producer = null;

    /// <summary>
    /// Initialize class and load configuration plus services to shared global memory.
    /// </summary>
    /// <param name="context"></param>
    public BatchProcess(ILambdaContext context = null)
    {
      if (BatchProcess.configuration != null) return;

      var alias = context?.InvokedFunctionArn?.Substring(context.InvokedFunctionArn.LastIndexOf(":") + 1);
      var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

      context?.Logger.LogLine($"[{context.AwsRequestId}] Reading configurations");
      BatchProcess.configuration = new ConfigurationBuilder()
       .SetBasePath(Directory.GetCurrentDirectory())
       .AddJsonFile("appsettings.dev.json", optional: false, reloadOnChange: false)
       .AddJsonFile($"appsettings.{alias}.json", optional: true, reloadOnChange: false)
       .AddJsonFile($"appsettings.{environment}.json", optional: true, reloadOnChange: false)
       .Build();

      BatchProcess.firebase = new Firebase.Database(context, configuration);
      //BatchProcess.airtable = new Airtable("appqdovUhqUdZYZuD", "keyUm4fJkHVdQeW2L");
      //BatchProcess.s3_producer = new S3_Producer();


      System.Diagnostics.Trace.TraceWarning("Pending relocate Airtable credentials.");
    }

    private float FindPaidAmount(StreamsDbContext db, Loan q)
    {
      var results = db.BankLedger
        .Where(p => q.Firebase_Id.StartsWith(p.loan_id));

      return results.Sum(p => float.Parse(p.Credit));
    }

    private DateTime? FindPaidDate(StreamsDbContext db, Loan q)
    {
      var results = db.BankLedger
        .Where(p => q.Firebase_Id.StartsWith(p.loan_id));

      return results.FirstOrDefault()?.Date;
    }

    
    public void PatchLoans(ILambdaContext context)
    {
      using (var db = new StreamsDbContext())
      {
        var output = new List<dynamic>();

        var bankLedger = db.BankLedger.ToList();
        var awsLoans = db.AWSLoans.ToList();

        foreach (var user in db.Users)
        {
          var loans = awsLoans
            .Where(q=>q.user_id == user.Firebase_Id && q.status != "rejected" && q.status != "wrong")
            .OrderBy(q => q.created_datetime)
            .ToList()
            .Select((q,i) => {

              var matches = bankLedger.Where(p => q.loan_request_id.StartsWith(p.loan_request_id));
              DateTime? extDate = default(DateTime?);
              if (matches.Count() > 1)
                extDate = matches.Select(x => x.Date?.AddHours(-6)).Min();

              var obj = new
              {
                LoanId = q.loan_request_id,
                UserId = q.user_id,
                DepositAmount = q.amount_requested,
                LoanNumber = i + 1,
                DepositDate = q.released_datetime?.AddHours(-6),
                DueDate = q.due_datetime.AddHours(-6),
                PaidAmount = matches.ToList().Sum(p => !string.IsNullOrEmpty(p.Credit) ? float.Parse(p.Credit) : 0),
                PaymentDate = matches.Select(x => x.Date?.AddHours(-6)).Max(),
                ExtensionPaymentDate = extDate
              };

              return obj;

            })
            .ToList();

          if (loans.Count == 0) continue;

          output.AddRange(loans);
          Console.WriteLine($"{output.Count} \t - User_Id {user.Firebase_Id} has {loans.Count} loans");
        }


        var dbDataset = db.AWSLoans.ToList();
        foreach (var item in output)
        {
          var match = dbDataset.Single(q=>q.loan_request_id == item.LoanId);
          match.loan_number = item.LoanNumber;
          match.extension_datetime = item.ExtensionPaymentDate;
          match.payment_datetime = item.PaymentDate;
          Console.WriteLine($"number:{match.loan_number}, ext:{match.extension_datetime}, pay:{match.payment_datetime}");
          db.Update(match);
        }
        db.SaveChangesAsync().Wait();


        using (var writer = new StringWriter())
        using (var csv = new CsvWriter(writer))
        {
          csv.WriteRecords(output);

          var content = writer.ToString();
          File.WriteAllText(@"C:\Users\Javier\OneDrive\Desktop\LoansStatus.csv",content);
        }
        
      }
    }

    public void Producer_Users(BlockingCollection<object> queue, ILambdaContext context = null)
    {
      // yield all entities to producer and queue
      firebase
        .Get("/Users", context)
        
        .Result
        .ToIdJSON()
        .ForEach(item =>
        {
          bool isQueued = queue.TryAdd(item);
        });
    }

    public void Producer_Loans(BlockingCollection<object> queue, ILambdaContext context = null)
    {
      // yield all entities to producer and queue
      firebase
        .Get("/Loans", context)
        .Result
        .ToIdJSON()
        .ForEach(item =>
        {
          bool isQueued = queue.TryAdd(item);
        });
    }

    public void Producer_Deposits(BlockingCollection<object> queue)
    {
      // yield all entities to producer and queue
      airtable
        .List("Table 1")
        .ForEach(item =>
        {
          bool isQueued = queue.TryAdd(item);
        });
    }

    public void Producer_LoanRequests(BlockingCollection<object> queue)
    {
      // C:\Users\Javier\Repos\AwsDataMigration\src\ETL - Copy\Program.cs
      // yield all entities to producer and queue
      s3_producer
        .List("s3://vana-stream-loan-request/events/")
        .ForEach(item =>
        {
          bool isQueued = queue.TryAdd(item);
        });
    }

    public void Producer_Firebase_LoanRequests(BlockingCollection<object> queue)
    {
      // yield all entities to producer and queue
      s3_producer
        .List("s3://vana-stream-loan-request/events/")
        .ForEach(item =>
        {
          bool isQueued = queue.TryAdd(item);
        });
    }

    public void Consumer_Users(object item, ILambdaContext context = null)
    {
      // flatten document object
      var json = item as JObject;
      var flat_properties = json.FlattenProperties<JProperty>(includeValues: true).ToArray();
      var flat_item = new JObject(flat_properties);

      // parse to POCO class
      var newEntity = flat_item.ToUser_V1();

      // send data to rdbms 
      using (var db = new StreamsDbContext())
      {
        // check first exists: user
        var oldEntity = db.Users.FirstOrDefault(q => q.Firebase_Id == newEntity.Firebase_Id);

        // if it doesnt exist in db, create it 
        if (oldEntity == null)
        {
          oldEntity = db.Users.Add(newEntity).Entity;
          db.SaveChangesAsync().Wait();
          System.Diagnostics.Debug.WriteLine($"User-{newEntity.Id}");
          Console.WriteLine($"User-{newEntity.Id}");
          context?.Logger.LogLine($"User-{newEntity.Id}");
        }
        // if it exsits in db, just update it
        else
        {
          newEntity.Id = oldEntity.Id;
          db.Entry(oldEntity).CurrentValues.SetValues(newEntity);
          db.SaveChangesAsync().Wait();
          System.Diagnostics.Debug.WriteLine($"User-{oldEntity.Id} [update]");
          Console.WriteLine($"User-{oldEntity.Id} [update]");
          context?.Logger.LogLine($"User-{oldEntity.Id} [update]");
        }
      }

    }


    public void Consumer_Loans(object item, ILambdaContext context = null)
    {
      // flatten document object
      var json = item as JObject;
      var flat_properties = json.FlattenProperties<JProperty>(includeValues: true).ToArray();
      var flat_item = new JObject(flat_properties);

      // parse to POCO class
      var newEntity = flat_item.ToLoan_V1();

      // send data to rdbms 
      using (var db = new StreamsDbContext())
      {
        // check first exists: loan, user
        var oldEntity = db.Loans.FirstOrDefault(q => q.Firebase_Id == newEntity.Firebase_Id);
        var user = db.Users.FirstOrDefault(q => q.Firebase_Id == newEntity.Firebase_UserId);

        if (user == null) return; // loan depends on user, if no user then drop this loan
        newEntity.UserId = user.Id;

        // if it doesnt exist in db, create it 
        if (oldEntity == null)
        {
          oldEntity = db.Loans.Add(newEntity).Entity;
          db.SaveChangesAsync().Wait();
          System.Diagnostics.Debug.WriteLine($"Loan-{newEntity.Id} User-{user.Id}");
          Console.WriteLine($"Loan-{newEntity.Id} User-{user.Id}");
          context?.Logger.LogLine($"Loan-{newEntity.Id} User-{user.Id}");
        }
        // if it exsits in db, just update it
        else
        {
          newEntity.Id = oldEntity.Id;
          db.Entry(oldEntity).CurrentValues.SetValues(newEntity);
          db.SaveChangesAsync().Wait();
          System.Diagnostics.Debug.WriteLine($"Loan-{oldEntity.Id} User-{user.Id} [update]");
          Console.WriteLine($"Loan-{oldEntity.Id} User-{user.Id} [update]");
          context?.Logger.LogLine($"Loan-{oldEntity.Id} User-{user.Id} [update]");
        }
      }
    }

    public void Consumer_Deposits(object item, ILambdaContext context = null)
    {

      var event_object =
        JObject.FromObject(item)
        .ScopeInto("fields")
        .FlattenProperties<JProperty>(includeValues: true)
        .ToArray()
        .AppendValues(
          source: item,
          mappings: new Dictionary<string, string>()
          {
          { "Airtable_Id", "id" },
          { "Airtable_CreatedTime", "createdTime" },
          })
        .RemoveProperties(new[] {
          "Payment Confirmation Image",
          "Deposit Image"
        })
        .WrapInside<JObject>();

      var entityType = new Core.Common.Models.Normalized.V1.Deposit();

      var newEntity = event_object
      .InferObject(entityType,
        explicitMappings: new Dictionary<string, string>()
        {
        { "loan #", "Loan_Counter" },
        { "check #", "Check_Id" },
        { "# doc", "_doc" },
        { "user_ID", "Firebase_User_Id" },
        { "loan_ID", "Firebase_Loan_Id" },
        })
      .WrapInJObject()
      .FlattenProperties<JProperty>(includeValues: true)
      .ToArray()
      .WrapInside<JObject>()
      .ToDeposit_V1();

      // send data to rdbms 
      using (var db = new StreamsDbContext())
      {
        // check first exists: Deposits, user
        var oldEntity = db.Deposits.FirstOrDefault(q => q.Airtable_Id == newEntity.Airtable_Id);
        var user = db.Users.FirstOrDefault(q => q.Firebase_Id == newEntity.Firebase_User_Id);

        if (user == null) return; // Deposits depends on user, if no user then drop this Deposits
        newEntity.UserId = user.Id;

        // if it doesnt exist in db, create it 
        if (oldEntity == null)
        {
          oldEntity = db.Deposits.Add(newEntity).Entity;
          db.SaveChangesAsync().Wait();
          Console.WriteLine($"Deposits-{newEntity.Id} User-{user.Id}");
          context?.Logger.LogLine($"Deposits-{newEntity.Id} User-{user.Id}");
        }
        // if it exsits in db, just update it
        else
        {
          newEntity.Id = oldEntity.Id;
          db.Entry(oldEntity).CurrentValues.SetValues(newEntity);
          db.SaveChangesAsync().Wait();
          Console.WriteLine($"Deposits-{oldEntity.Id} User-{user.Id} [update]");
          context?.Logger.LogLine($"Deposits-{oldEntity.Id} User-{user.Id} [update]");
        }
      }

    }

    public void Consumer_LoanRequests(object item)
    {
      var data = item
        .DownloadObject()
        .Decompress_GZip()
        .ToText();
    }
    
  }
}