﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using Core.Common.Models.Normalized.V1;

namespace Core.Processing
{
  public static class Transformers
  {

    public static long ToPhone(this object input)
    {
      if (string.IsNullOrEmpty(input as string)) return 0;

      var phone = input.ToString();
      if (!phone.StartsWith('+')) phone = "502" + phone;
      else phone = phone.Substring(1);
      return long.Parse(System.Text.RegularExpressions.Regex.Match(phone, @"\d+").Value);
    }

    public static User ToUser_Raw(this JObject input)
    {

      return input
        .PopulateObject<User>();
    }

    public static User ToUser_V1(this JObject input)
    {
      var item = new User();

      input.Add("Firebase_id", input["id"]); input.Remove("id");
      item.phone = input["phone"].ToPhone();

      return input
        .PopulateObject<User>();
    }

    public static Loan ToLoan_V1(this JObject input)
    {
      input["Firebase_createdDateTime"] = (input["created_at"] ?? input["createdDateTime"] ?? input["created"]);// ?? DateTime.MinValue.ToString("s")).ToString();
      input["Firebase_dueDateTime"] = (input["dueDateTime"] ?? input["dueDate"]);// ?? DateTime.MinValue.ToString("s")).ToString();
      //input["repaymentDateTime"] = input["repaymentDateTime"].ToString().ToDateTimeOrDefault().ToString("s");
      input["amountRequested"] = (input["amountRequested"] ?? input["amountAsked"]);
      input["userId"] = (input["userId"] ?? input["user"]);
      
      input["createdDateTime"] = input["Firebase_createdDateTime"].ToDateTimeOrDefault();
      input["dueDateTime"] = input["Firebase_dueDateTime"].ToDateTimeOrDefault();
      
      input.Add("Firebase_id", input["id"]);
      input.Add("Firebase_UserId", input["userId"]);

      input.Remove("amountAsked");
      input.Remove("user");
      input.Remove("id");
      input.Remove("userId");

      return input
        .PopulateObject<Loan>(new Loan());
    }

    public static Deposit ToDeposit_V1(this JObject input)
    {
      var item = new Deposit();
      
      return input
        .PopulateObject<Deposit>();
    }

    public static LoanResult ToLoanResult_Raw(this JObject input)
    {
      input.Add("Firebase_id", input["id"]);
      input.Remove("id");

      return input
        .PopulateObject<LoanResult>();
    }

    public static Loan ToLoan_Raw(this JObject input)
    {
      input.Add("Firebase_id", input["id"]);
      input.Remove("id");

      return input
        .PopulateObject<Loan>();
    }


    public static DateTime ToDateTimeOrDefault(this object input)
    {
      try
      {
        return input.ToString().ToDateTime();
      }
      catch 
      {
        return DateTime.MinValue;
      }
    }

    public static DateTime ToDateTime(this string input)
    {
      var culture = System.Globalization.CultureInfo.InvariantCulture;
      var style = input.IndexOfAny(new char[] { '+', '-' }) > -1 ? System.Globalization.DateTimeStyles.AdjustToUniversal : System.Globalization.DateTimeStyles.None;
      var formats = new string[] {
        "yyyyMMddHHmm",
        "M/d/yy h:mm:ss tt",
        "M/d/yyyy h:mm:ss tt",
        "yyyyMMdd HH:mm",
        "yyyy-MM-ddTHH:mm:ss.SSSZ",
        "yyyy-MM-ddTHH:mm:ss+|-HH:mm",
        "yyyy-MM-ddTHH:mm:ss+HH:mm",
        "yyyy-MM-ddTHH:mm:ss",
        "yyyy-MM-dd HH:mm:ss"
      };

      // explicit formats from above
      if (DateTime.TryParseExact(input, formats, culture, style, out DateTime output_datetime))
        return output_datetime;

      // unix time
      else if (long.TryParse(input, out long output_long))
        return new DateTime(1970, 1, 1, 0, 0, 0).AddMilliseconds(Convert.ToDouble(output_long));

      // final try in spanish text
      else 
        return DateTime.Parse(input, new System.Globalization.CultureInfo("es-ES"));

      // exception
      throw new Exception($"DateTime could not parse '{input}'");
    }

  }
}